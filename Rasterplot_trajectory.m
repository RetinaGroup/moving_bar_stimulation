%% Bar Trajectpry with raster plot 01
%load the sorted merge file first
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G043')

T1=200; %sec %choose time range for plotting
T2=250;

% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',3); hold on;
set(gca,'Ydir','reverse')
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');


xlim([0,frame2-frame1])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])

%Raster plot of selected channels
yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end


%for RL direction stimulation: 1st row is for channel number:7,15,23,31,39,47/
%2nd row is for number: 1,8,16,24,32,40,48,55 etc
rasterch=cell(1,36); %36 is to plot 36 rows
for ch=1:length(rasterch)
    rasterch{ch}=zeros(1,length(rasterch));
end
selectch=[23 40 56 0 51 5 53 0];%choose specific channels % remember to enter 0 if that row has no selected channel
cou=1;
for ch=[1 6 11 16 21 26 31 36] %the numbers are the location, NOT the selected channel number / dont need to change/ correspond to 36 rows setting
    if selectch(cou)>0
         rasterch{ch}=yk_spikes2{selectch(cou)};
    end
    cou=cou+1;
end

figure; %plot raster plot for selected channels
set(gca,'Position',[0.17 0.2  0.75 0.7])
plotSpikeRaster(rasterch,'PlotType','vertline');
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

% create axis plot 
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Position (mm)')
xlabel('Time (sec)')
xlim([0,50])
% set(gca,'ytick',[])
set(gca, 'YTick', [0 0.5 1])
set(gca,'YTickLabel', {'0','0.7','1.4'} )
set(gca, 'XTick', [0 25 50])
set(gca,'XTickLabel', {'200','225','250'} )
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

%then overlap these 3 images together in ppt
