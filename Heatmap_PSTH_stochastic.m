%%  HMM and OU Heatmap and PSTH
 clear all
% close all
cc=hsv(6);
cd('D:\Yiko\Experiment\08102017\merge\HMM_RL') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;  

for z = 1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

%% Binning
BinningInterval = 1/200;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 
%   BinningSpike(:,1) = 0;BinningSpike(:,end) = 0;

%% Heat Map  
figure;  imagesc(BinningTime,[1:60],BinningSpike);  
set(gca,'fontsize',12)  
title(['Heatmap  bin=5ms  ',name]);
colorbar
xlabel('time  (s)');   ylabel('channel ID');
saveas(gcf, ['HeatMap_',name,'_Bin=5ms'],'fig');

%specific channels only
% sp_BinningSpike=[];
% ff=1;
% for ch=[36 45 46 51 52  54]
%     sp_BinningSpike(ff,:)=BinningSpike(ch,:);
%     ff=ff+1;
% end
% figure;  imagesc(BinningTime,[1:6],sp_BinningSpike);   
% % figure;  imagesc(BinningTime,[1:60],BinningSpike);   


%% PSTH 
s=0;
for channel=1:60
    s= s + BinningSpike(channel,:);
end
s=s./59; %average 59 channels, since 1 channel is for ground
figure; plot(BinningTime,s./BinningInterval);
set(gca,'fontsize',12)  
title(['PSTH /total channel/ bin=5ms   ',name]);
xlabel('time  (s)');   ylabel('Firing rate(Hz)');
saveas(gcf, ['PSTH_',name,'_Bin=5ms'],'fig');
mean(s)/BinningInterval  %mean firing rate of 1 channel (Hz)


end
