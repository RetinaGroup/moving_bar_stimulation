%these are from file "Space_calibration_Oct2017.m"
%%
Screen('Preference', 'SkipSyncTests', 1)
global rect w stop_flag
[w, rect] = Screen('OpenWindow',2, [0 0 0]); %[0 0 0]: black background %"2"(second) is the monitor number, another choice is "1"(first)

%% test focus plane: see the edge of the black square is clear or not
baseRect = [0 0 550 550];
xCenter=670; %x coordination of the rectangle center
yCenter=460;
centeredRect = CenterRectOnPointd(baseRect, xCenter, yCenter);
Screen('FillRect', w,  160,centeredRect); %"160" is luminance, you can arrange this yoursulf
baseRect = [0 0 150 150];
xCenter=495;  
yCenter=450; 
centeredRect = CenterRectOnPointd(baseRect, xCenter, yCenter);
Screen('FillRect', w, 0, centeredRect); %"0" is luminance, you can arrange this yoursulf
Screen('Flip', w); %show the image

%% decide mea region on LED screen
%full light screen
baseRect = [0 0 1250 1250]; %size of the rectangle shown
xCenter=700; %x coordination of the rectangle center
yCenter=400;
centeredRect = CenterRectOnPointd(baseRect, xCenter, yCenter);
Screen('FillRect', w, 10,centeredRect);  %be cardeful that all values have to be integer!!!

mea_size=503; %use odd number!!! %pixel %just try and error 
baseRect = [0 0 mea_size mea_size];  %use odd number!!!
meaCenter_x=630; 
meaCenter_y=573;  
centeredRect = CenterRectOnPointd(baseRect, meaCenter_x, meaCenter_y);
Screen('FillRect', w, 170,centeredRect);  %"170" is luminance, you can arrange this yoursulf
Screen('Flip', w);
