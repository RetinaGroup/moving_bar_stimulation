clear all
load('D:\Yiko\Files for Thesis\04262018\pos2 merge sort\merge sort1 _0426pos2 HMM 4322 G043')

%auto mutual information for stimulus
T=7*60; %second
TheStimuli = bin_pos; 
Neurons =  TheStimuli; 
% figure; autocorr(TheStimuli, 1000); %check!

%% Binning
fps=1/60;
multiple_value=1;
DataTime=T;
BinningInterval = DataTime/length(TheStimuli); %s
bin=BinningInterval*10^3; %ms

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
% figure; hist(isi2,StimuSN);
% title(name);


%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
dat=[];informationp=[];temp=backward+2;
%         norm = BinningInterval; %bits/second
norm =1;

    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim

        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];

        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
 
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);

    end  

    dat=[];informationf=[];temp=0;dat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
%         norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
%         norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
%         norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
    
t=[-backward*bin:bin:forward*bin];

figure;
set(gca,'fontsize',14); hold on
legend('-DynamicLegend');
plot(t,information-information_shuffle,'LineWidth',1.5); hold on
% title(['HMM RL MI all G  channel ',num2str(channelnumber)], 'Fontsize',20) 
xlabel('delta t (ms)');ylabel('MI (bits/second)(minus shuffled)');

