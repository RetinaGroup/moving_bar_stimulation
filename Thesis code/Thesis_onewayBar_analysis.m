%select only soma channels(Tina #!)
 Spikes2=cell(1,60);
for i=[5     7     9    11    14    15    19    22    24    27    30    35    38    43    44    50    51    58    59    60]
 Spikes2{i}= Spikes{i};
end
Spikes=cell(1,60);
Spikes=Spikes2;

%check timestamp
figure;plot(a_data(2,:)); hold on
% figure;plot(a_data(1,:)); hold on
plot(TimeStamps*20000,36000,'*');
TimeStamps(2)-TimeStamps(1)


% Binning : 10ms
cut_spikes=cell(1,60);
cut_spikes = seperate_trials(Spikes,TimeStamps); 
DataTime=TimeStamps(2)-TimeStamps(1);

BinningInterval = 1/100;  %s
BinningTime = [ 0 : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(cut_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 


%% Change rows for RL(LCD) direction stimulation
%LCD(RL) <=> MEA(UD)
mn(1,:)=[0 7 15 23 31 39 47 0];   mn(8,:)=[0 14 22 30 38 46 54 0];
for i=1:6
    if i==1
            mn(i+1,:)=[1 8 16 24 32 40 48 55];
    else        mn(i+1,:)=[1 8 16 24 32 40 48 55]+i-1;   end
end
BinningSpike2=[]; %the rearranged matrix
%test orientation:
Newori=[];
count=7; %start from #7 
for j=2:7
        for kk=1:8
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
                 count=55;
end
% Newori
count=7; %start from #7 
select_ch=54;
for j=2:7
        for kk=1:8
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                          if mn(j,kk)==select_ch
                    end
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                    if mn(j,kk)==select_ch
                    end
                    count=count+1;
        end
                 count=55;
end

%% Plot Heatmap RL
figure;  imagesc(BinningTime,[1:60],BinningSpike2);    
colorbar
title(['1123  Rearrange Oneway nsort RL T07/  Bin=10ms'])
xlabel('time(s)');   ylabel('channel ID');

    
%% Plot Heatmap UD
T=0.8;
Tbin=T/BinningInterval;
figure;
imagesc(BinningTime(1,1:Tbin),[1:60],BinningSpike(:,1:Tbin)); 
colorbar
xlabel('time(s)');   ylabel('channel ID');
title(['1213 Oneway UD T07 /  Bin=10ms'])

%% Plot Lines
hold on;
lineT=[0.1:0.1:0.7];
for linepos=lineT
    x1=linepos; 
    line([5 5],[10 20] ,get(gca, 'ylim'),'Color','g','LineStyle','--');  
end

hold on;
lineP=[55 47 39 31 23 15 7];
for i=lineP
    hline([10 15],'g--')
end

%% Plot square dashed box
lineT=[0:0.1:0.8];
lineP=[60 55 47 39 31 23 15 7 1];
for k=1:8 %8 boxes
      line([lineT(k) lineT(k)],[lineP(k) lineP(k+1)] ,get(gca, 'ylim'),'Color','r','LineStyle','--');  
      line([lineT(k+1) lineT(k+1)],[lineP(k) lineP(k+1)] ,get(gca, 'ylim'),'Color','r','LineStyle','--');  
      line([lineT(k) lineT(k+1)] ,[lineP(k) lineP(k)],'Color','r','LineStyle','--');  
      line([lineT(k) lineT(k+1)],[lineP(k+1) lineP(k+1)] ,'Color','r','LineStyle','--');  
end










