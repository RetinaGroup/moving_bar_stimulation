%remember to reload the files if you want to run different sections, since
%the parameter names are the same.

%% PSTH for singel G 
%load 0423 sort1 HMM RL G043 (04302018) channel 44
clear all
load('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs\03 merge sort1 unit1 _HMM RL G043')
T1=0; %sec %choose time range for plotting
T2=420;

yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end
frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
%use the trajectory plotted above
%Binning
        % BinningInterval = 1/60;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
        % BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningTime =diode_BT(frame1:frame2);
BinningTime=BinningTime-T1;
BinningSpike = zeros(60,length(BinningTime));
s=zeros(1,length(BinningSpike));
for i = [44 ]  %select neurons
    [n,~] = hist(yk_spikes2{i},BinningTime) ;
    BinningSpike(i,:) = n ;
    max(BinningSpike(i,:) )
    s=s+BinningSpike(i,:);
end 
% [5 44 45 48 50 55 ]  
 max(s)
 
figure;
% plot(BinningTime,s,'black');
plot(BinningTime,s/BinningInterval,'black'); 
max(s/BinningInterval)
set(gca,'Position',[0.17 0.2  0.75 0.7])
set(gca, 'Fontname', 'Times New Roman');
ylabel('Firing rate(Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15); 
xlim([0,420])
set(gca, 'XTick', [0 100 200 300 400])
set(gca,'XTickLabel', {'0','100','200','300','400'} )
set(gca, 'YTick', [ 50 100 150 200 250])
uistack(gca,'top');


% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7/2])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',1); hold on;
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');


% plot(newYarray)
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'ytick',[])
ylabel('Bar position')


%% Raster plot: lots channels
T1=60; %sec %choose time range for plotting
T2=120;
% choose corresponding spikes in T range
yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel
ss = Spikes{j};
        ss(ss<T1)=[]; %since yk_spikes already retuned to start time =0
        ss(ss>T2)=[]; %since yk_spikes already retuned to start time =0
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end

%make cell array without empty elements first
rasterch=cell(1,60);
for ch=1:60
    rasterch{ch}=zeros(1,60);
end
%choose specific channels
for gh=[2     9    16    18    19    25    28    32    38    39    42    43    48    49    50    54    55    58 ]; %use Tina's #
   rasterch{gh}=cell2mat(yk_spikes2(1,gh));
end

figure; plotSpikeRaster(rasterch,'PlotType','vertline');
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( 'Time(sec)');ylabel('Channel number');
set(gca,'fontsize',20); 
set(gca, 'TickDir','in')
set(gca, 'XTick', [10 30 50])
set(gca, 'YTick', [10 30 50])
set(gca,'XTickLabel', {'70','90','110'} )
box on

%% average firing rate
%choose specific channels
select_ykspikes=cell(1,60);
for gh=[2     9    12    14    16    18    19    21    26    28    31    32    38    40    42    45    47    49    50   51    52    54    55    56]; %use Tina's #
   select_ykspikes{gh}=cell2mat(yk_spikes(1,gh));
end
sum_spikes=zeros(1,60);
spikes_sec=zeros(1,60);
for kj=1:60
sum_spikes(1,kj)=length(select_ykspikes{kj}); %since yk_spikes is the timing of spikes
end
spikes_sec=sum_spikes./420;
max(spikes_sec)
test=spikes_sec;
test(test == 0) = inf;
min(test,[],2)  %min despite 0
%average firing rate of all selected channels
sech=[2     9    12    14    16    18    19    21    26    28    31    32    38    40    42    45    47    49    50   51    52    54    55    56];
sum(sum_spikes)/420/length(sech)  %average firing rate for 1 channel



%% Bar Trajectpries with 3 tau overlapping
%load the sorted merge file first
clear all
load('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs\03 merge sort1 unit1 _HMM RL G043')
%HMM
T=60; %sec %choose time range for plotting
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
frame=T/(1/60);

figure;
plot(bin_pos(1,1:frame),'linewidth',2.5); 
xlim([1 frame ])
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

clear all
load('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs\03 merge sort1 unit1 _HMM RL G09')
figure;
plot(newXarray(1,1:frame),'linewidth',1.5); hold on;

xlabel( 'Time(sec)');ylabel('Position (\mu m)');
set(gca,'fontsize',20);
% plot(new_y(1,1:frame),'linewidth',2);
% plot(newYarray)
xlim([0,frame])
ylim([min(newYarray), 800])
% ylim([min(new_y), max(new_y)])
set(gca, 'XTick', [1200 2400])
set(gca, 'YTick', [300 500 700])
set(gca,'XTickLabel', {'20','40'} )
set(gca,'YTickLabel', {'-563','0','563'} ) %1 pixel=2.816 um
lgd = legend('\tau =1.12 sec', '\tau =0.48 sec','\tau =0.15 sec');
lgd.FontSize = 10;
legend boxoff  

%OU
T=20; %sec %choose time range for plotting
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
frame=T/(1/60);
plot(new_y(1,1:frame),'linewidth',2.5); hold on;
plot(new_y(1,1:frame),'linewidth',1); hold on;
plot(new_y(1,1:frame),'linewidth',0.1); hold on;

xlim([0,frame])
ylim([min(new_y), 800])
% ylim([min(new_y), max(new_y)])
set(gca, 'XTick', [300 900])
set(gca, 'YTick', [300 500 700])
set(gca,'XTickLabel', {'5','15'} )
set(gca,'YTickLabel', {'-563','0','563'} ) %1 pixel=2.816 um
lgd = legend('\tau =1.12 sec', '\tau =0.48 sec','\tau =0.15 sec');
lgd.FontSize = 10;
legend boxoff  
xlabel( 'Time(sec)');ylabel('Position (\mu m)');
set(gca,'fontsize',20);


%% Bar Trajectpries with 3 tau "without" overlapping
%load the sorted merge file first
%0524 HMM G=3,5,20
%HMM
cd('D:\Yiko\Images for thesis\for bar tra 0524 merge\merge_HMM_unsort') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 
for z = 1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

T=60; %sec %choose time range for plotting
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
frame=T/(1/60);

plot(newYarray(1,1:frame),'linewidth',1.5); hold on;

xlabel( 'Time(sec)');ylabel('Position (\mu m)');
set(gca,'fontsize',20);
% plot(new_y(1,1:frame),'linewidth',2);
% plot(newYarray)
xlim([0,frame])
ylim([min(newYarray), 800])
% ylim([min(new_y), max(new_y)])
set(gca, 'XTick', [1200 2400])
set(gca, 'YTick', [300 500 700])
set(gca,'XTickLabel', {'20','40'} )
set(gca,'YTickLabel', {'-563','0','563'} ) %1 pixel=2.816 um
if z==1
lgd = legend('\tau =1.2 sec','Position',[315 270 0.1 0.2]);
end
if z==2
    lgd = legend('\tau =0.7 sec','Position',[315 270 0.1 0.2]);
end
if z==3
    lgd = legend('\tau =0.16 sec','Position',[315 270 0.1 0.2]);
end
lgd.FontSize = 18;
legend boxoff  
end

%OU
cd('D:\Yiko\Images for thesis\for bar tra 0524 merge\merge_OU_unsort') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 
for z = 1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

T=60; %sec %choose time range for plotting
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
frame=T/(1/60);

plot(new_y(1,1:frame),'linewidth',1.5); hold on;

xlabel( 'Time(sec)');ylabel('Position (\mu m)');
set(gca,'fontsize',20);
% plot(new_y(1,1:frame),'linewidth',2);
% plot(newYarray)
xlim([0,frame])
ylim([min(newYarray), 800])
% ylim([min(new_y), max(new_y)])
set(gca, 'XTick', [1200 2400])
set(gca, 'YTick', [300 500 700])
set(gca,'XTickLabel', {'20','40'} )
set(gca,'YTickLabel', {'-563','0','563'} ) %1 pixel=2.816 um
if z==1
lgd = legend('\tau =1.2 sec','Position',[315 270 0.1 0.2]);
end
if z==2
    lgd = legend('\tau =0.7 sec','Position',[315 270 0.1 0.2]);
end
if z==3
    lgd = legend('\tau =0.16 sec','Position',[315 270 0.1 0.2]);
end
lgd.FontSize = 18;
legend boxoff  
end



%% Bar Trajectpry with raster plot
%load the sorted merge file first
T1=60; %sec %choose time range for plotting
T2=120;

%% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/(1/60);
frame2=T2/(1/60);
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',3); hold on;
% plot(newYarray(1,frame1:frame2)+11,'b--','linewidth',0.5); hold on;
% plot(newYarray(1,frame1:frame2)-11,'b--','linewidth',0.5);
% plot(new_y(1,frame1:frame2)+11,'b--','linewidth',0.5); hold on;
% plot(new_y(1,frame1:frame2)-11,'b--','linewidth',0.5);

% plot(newYarray)
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])


%% Raster plot
% choose corresponding spikes in T range
yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end

%Raster plot: only plot chosen channels
% rasterch=cell(1,57);
rasterch=cell(1,40);
for ch=1:40
    rasterch{ch}=zeros(1,40);
end
selectch=[2 0 19 0 0 43 50 55];%choose specific channels
cou=1;
for ch=[1 6 11 16 21 26 31 36] %the numbers are the location, NOT the selected channel number
    if selectch(cou)>0
         rasterch{ch}=yk_spikes2{selectch(cou)};
    end
    cou=cou+1;
end

figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
plotSpikeRaster(rasterch,'PlotType','vertline');
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
box on
% ylabel('Channel Number')
% xlabel('Time (sec)')
% set(gca,'fontsize',13)


%% create axis plot 
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Position')
xlabel('Time (sec)')
xlim([0,60])
set(gca,'ytick',[])
set(gca, 'XTick', [0 30 60])
set(gca,'XTickLabel', {'60','90','120'} )
set(gca,'fontsize',20); 



%% Heat map
%Binning
BinningInterval = 1/200;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
BinningTime = [ 0 : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
%specific channels only
for sco= [2     8     9    14    16    18    19    21    22    26    28    31    32    38    40    42    45    47    49   50    51    52    54    55    56]
    [n,~] = hist(yk_spikes{sco},BinningTime) ;
    BinningSpike(sco,:) = n ;
end 
%check:
find(BinningSpike(2,:)>0)
% Heat Map  
figure;  imagesc(BinningTime,[1:60],BinningSpike); 
box on

%% PSTH
%Binning
BinningInterval = 1/60;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
BinningTime = [ 0 : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 
s=[];
s= BinningSpike(50,:);
 
figure;
plot(BinningTime,s);
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Spikes')
xlabel('Time (sec)')
box on
set(gca,'fontsize',20); 
xlim([0,420])
set(gca, 'XTick', [100 300])
set(gca, 'YTick', [1 3])
uistack(gca,'top');


%% Reversal V-Time delay
Tlist=[0.4 0.7 1.4 1.9]; %sec
mea_length=1.4; %mm
Vlist=mea_length./Tlist;
Tdelay=[290 260 300 290 ];
figure;
plot(Vlist,Tdelay,'.','MarkerSize',13);
xlim([0.4  4]);
ylim([100 400]);
box on
set(gca,'fontsize',20); 
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [150 250 350])
ylabel('Latency')
xlabel('Bar Speed (mm/s)')

%% Reversal Heatmap
%load mat file "1231 reversal UD T07 with timstamp and soma Spikes"
% Binning : 10ms
cut_spikes=cell(1,60);
cut_spikes = seperate_trials(Spikes,TimeStamps); 
DataTime=TimeStamps(2)-TimeStamps(1);

BinningInterval = 1/100;  %s
BinningTime = [ 0 : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(cut_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 

T=0.7;
Tbin=2*T/BinningInterval;
figure;
imagesc(BinningTime(1,1:Tbin),[1:60],BinningSpike(:,1:Tbin)); 
set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',20); 
set(gca, 'XTick', [0.35 0.7 1.05])
set(gca, 'YTick', [10 30 50])
ylabel('Channel Number')
xlabel('Time (sec)')
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[5 10]) %set colorbar tick
%plot T=0.7 line
x1=T; 
line([x1 x1], get(gca, 'ylim'),'Color','r','LineStyle','--');  

%% Reversal PSTH
%use data above
s=0;
for channel=1:60
    s= s + BinningSpike(channel,:);
end
T=0.7*2+1; %include rest1
Tbin=T/BinningInterval;
figure; plot(BinningTime(1,1:Tbin),s(1,1:Tbin)); 

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Spikes')
xlabel('Time (sec)')
box on
set(gca,'fontsize',20); 
xlim([0,2.4])
set(gca, 'XTick', [0.5 1.5])
set(gca, 'YTick', [10 30 50])
%plot T=0.7 line
x1=0.7; 
line([x1 x1], get(gca, 'ylim'),'Color','r','LineStyle','--');  


%% Oneway
%use 1213 retina 2 pos2 UD T07 data only soma
%select only soma channels(Tina #!)
 Spikes2=cell(1,60);
for i=[5     7     9    11    14    15    19    22    24    27    30    35    38    43    44    50    51    58    59    60]
 Spikes2{i}= Spikes{i};
end
Spikes=cell(1,60);
Spikes=Spikes2;

% Binning : 10ms
cut_spikes=cell(1,60);
cut_spikes = seperate_trials(Spikes,TimeStamps); 
DataTime=TimeStamps(2)-TimeStamps(1);

BinningInterval = 1/100;  %s
BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(cut_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 

%Plot Heatmap UD
T=0.8;
Tbin=T/BinningInterval;
figure;
imagesc(BinningTime(1,1:Tbin),[1:60],BinningSpike(:,1:Tbin)); 
set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',20); 
set(gca, 'XTick', [0.35 0.7 1.05])
set(gca, 'YTick', [10 30 50])
ylabel('Channel Number')
xlabel('Time (sec)')
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[5 10]) %set colorbar tick

%% Plot square dashed box
lineT=[1:0.1:1.7];
lineP=[60 55 47 39 31 23 15 7 1];
for k=1:8 %8 boxes
      line([lineT(k) lineT(k)],[lineP(k) lineP(k+1)] ,get(gca, 'ylim'),'Color','r','LineStyle','--');  
      line([lineT(k+1) lineT(k+1)],[lineP(k) lineP(k+1)] ,get(gca, 'ylim'),'Color','r','LineStyle','--');  
      line([lineT(k) lineT(k+1)] ,[lineP(k) lineP(k)],'Color','r','LineStyle','--');  
      line([lineT(k) lineT(k+1)],[lineP(k+1) lineP(k+1)] ,'Color','r','LineStyle','--');  
end
