%% reversal L > R (correct direction on the monitor)

Tlist=[4];
for gg=Tlist
clearvars -except gg Tlist
load('D:\Yiko\HMM video frame\workspace_for_video') 

init=channel_map{68}; %Left on the monitor
final=channel_map{69};  %right

trials=12;
trial_rest=5; %sec %rest btw trials
fps =60;        %freq of the screen flipping 
T=gg;
Tvalue=gg;
%second: time for one way
dt=1/fps;
T=dt:dt:T;

rest0=1; %sec  %rest time before stimulation
rest1=1; %sec %rest time after stimulation
restlum=0.05; %luminance value during resting time 

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

clean_LED=clean_LED./255; 
clean_LED(clean_LED>1)=1;

X0 = round(init(1)); 
Y0 = round(init(2));
dx =(final(1)-init(1))/length(T);
dy = (final(2)-init(2))/length(T);
Xs = zeros(1,2*length(T)-1); %multiply 2 because back and forth
Ys = zeros(1,2*length(T)-1);
Xs(1,1)=X0; %since the mean value of damped eq is zero
Ys(1,1)=Y0;
n=1;
% figure;
for i = 1:2*length(T)+1
        if i <= length(T)+1
            Xs(1,i) = round(X0 + (i-1) *dx);
            Ys(1,i) =round( Y0 + (i-1) *dy);
%             plot(i,Xs(i),'bo');hold on
        end       
        if i > length(T)+1
            Xs(1,i) = round(X0 + length(T) *dx - n *dx);
            Ys(1,i) = round(Y0 + length(T) *dy - n *dy);
            n=n+1;
%              plot(i,Xs(i),'r*');hold on
        end
end
%make sure start pts are the same and max pt location is correct!!!
% max(Xs)
% Xs(1)
% Xs(end)


%rearrange dotPositionMatrix format
dotPosition_x=[];   dotPosition_y=[];
dotPosition_x=dotPositionMatrix(1,1:8:end);
dotPosition_y=dotPositionMatrix(2,1:8);

number_repeat=repmat([1:length(Xs)],1,1);

%number file
cd('D:\Yiko\Timing stuff\numberFlip') 
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['Reversal LR T=',num2str(Tvalue)];
name
% ori_dir =['D:\Yiko\HMM video frame\reversalBar'];   

% %video setting
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\HMM video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);

%dark rest first
for mm=1:trial_rest*fps
img=0*ones(1024,1280);   
writeVideo(writerObj, img);
end


for trialn=1:trials
    
%start part: adaptation: bar shows
for mm=1:rest0*fps
a=zeros(1024,1280);
X=Xs(1);
Y=Ys(1);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end

img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end


for kk =1:length(Xs)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%HMM RL bar trajectory
X=Xs(kk);
Y=Ys(kk);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
        
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end


%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;


%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '/', num2str(kk,'%06i'),'.jpeg']);
%imwrite conversion would change data value and class/ Indexed images are converted to RGB before writing out JPEG files

img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part: adaptation: bar rest and then disappear
for mm=1:rest1*fps
a=zeros(1024,1280);
X=Xs(end);
Y=Ys(end);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end

img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end

%dark rest btw trials
for mm=1:trial_rest*fps
img=[];
img=zeros(1024,1280);
writeVideo(writerObj, img);
end

end
%show the video is ended
for mm=1:5
img=[];
img=zeros(1024,1280);
img(500-35:500+35,1230:1280)=0.2; %gray
writeVideo(writerObj, img);
end
close(writerObj);

%save parameters needed
ori_dir02 =['D:\Yiko\HMM video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end

%%
clear all
%reversal R > L (correct direction on the monitor)
Tlist=[4];
for gg=Tlist
clearvars -except gg Tlist
load('D:\Yiko\HMM video frame\workspace_for_video') 

init=channel_map{69}; %right on the monitor
final=channel_map{68};  %left

trials=12;
trial_rest=5; %sec
fps =60;        %freq of the screen flipping 
T=gg;
Tvalue=gg;
%second: time for one way
dt=1/fps;
T=dt:dt:T;

rest0=1; %sec
rest1=1; %sec
restlum=0.05;

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

clean_LED=clean_LED./255; 
clean_LED(clean_LED>1)=1;

X0 = round(init(1)); 
Y0 = round(init(2));
dx =(final(1)-init(1))/length(T);
dy = (final(2)-init(2))/length(T);
Xs = zeros(1,2*length(T)-1); %multiply 2 because back and forth
Ys = zeros(1,2*length(T)-1);
Xs(1,1)=X0; %since the mean value of damped eq is zero
Ys(1,1)=Y0;
n=1;
% figure;
for i = 1:2*length(T)+1
        if i <= length(T)+1
            Xs(1,i) = round(X0 + (i-1) *dx);
            Ys(1,i) =round( Y0 + (i-1) *dy);
%             plot(i,Xs(i),'bo');hold on
        end       
        if i > length(T)+1
            Xs(1,i) = round(X0 + length(T) *dx - n *dx);
            Ys(1,i) = round(Y0 + length(T) *dy - n *dy);
            n=n+1;
%              plot(i,Xs(i),'r*');hold on
        end
end
%make sure start pts are the same and max pt location is correct!!!
% max(Xs)
% Xs(1)
% Xs(end)

%rearrange dotPositionMatrix format
dotPosition_x=[];   dotPosition_y=[];
dotPosition_x=dotPositionMatrix(1,1:8:end);
dotPosition_y=dotPositionMatrix(2,1:8);

number_repeat=repmat([1:length(Xs)],1,1);

%number file
cd('D:\Yiko\Timing stuff\numberFlip') 
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['Reversal RL T=',num2str(Tvalue)];
name
% ori_dir =['D:\Yiko\HMM video frame\reversalBar'];   

% %video setting
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\HMM video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);

%dark rest first
for mm=1:trial_rest*fps
img=0*ones(1024,1280);   
writeVideo(writerObj, img);
end


for trialn=1:trials
    
%start part: adaptation: bar shows
for mm=1:rest0*fps
a=zeros(1024,1280);
X=Xs(1);
Y=Ys(1);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end

img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end


for kk =1:length(Xs)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%HMM RL bar trajectory
X=Xs(kk);
Y=Ys(kk);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
        
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end


%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;


%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '/', num2str(kk,'%06i'),'.jpeg']);
%imwrite conversion would change data value and class/ Indexed images are converted to RGB before writing out JPEG files

img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part: adaptation: bar rest and then disappear
for mm=1:rest1*fps
a=zeros(1024,1280);
X=Xs(end);
Y=Ys(end);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end

img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end

%dark rest btw trials
for mm=1:trial_rest*fps
img=[];
img=zeros(1024,1280);
writeVideo(writerObj, img);
end

end
%show the video is ended
for mm=1:5
img=[];
img=zeros(1024,1280);
img(500-35:500+35,1230:1280)=0.2; %gray
writeVideo(writerObj, img);
end
close(writerObj);

%save parameters needed
ori_dir02 =['D:\Yiko\HMM video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end


%%
clear all
%reversal U > D (correct direction on the monitor)
Tlist=[4];
for gg=Tlist
clearvars -except gg Tlist
load('D:\Yiko\HMM video frame\workspace_for_video') 

init=channel_map{66}; %U
final=channel_map{67}; %D

trials=12;
trial_rest=5; %sec
fps =60;        %freq of the screen flipping 
T=gg;
Tvalue=gg;
%second: time for one way
dt=1/fps;
T=dt:dt:T;

rest0=1; %sec
rest1=1; %sec
restlum=0.05;

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

clean_LED=clean_LED./255; 
clean_LED(clean_LED>1)=1;

X0 = round(init(1)); 
Y0 = round(init(2));
dx = (final(1)-init(1)) /length(T);
dy = (final(2)-init(2))/length(T);
Xs = zeros(1,2*length(T)-1); %multiply 2 because back and forth
Ys = zeros(1,2*length(T)-1);
Xs(1,1)=X0; %since the mean value of damped eq is zero
Ys(1,1)=Y0;
n=1;
% figure;
for i = 1:2*length(T)+1
        if i <= length(T)+1
            Xs(1,i) = round(X0 + (i-1) *dx);
            Ys(1,i) = round(Y0 + (i-1) *dy);
%               plot(i,Ys(i),'bo');hold on
        end       
        if i > length(T)+1
            Xs(1,i) = round(X0 + length(T)*dx - n *dx);
            Ys(1,i) =round( Y0 + length(T) *dy - n *dy);
            n=n+1;
%             plot(i,Ys(i),'r*');hold on
        end
end
% max(Ys)
% Ys(1)
% Ys(end)

%rearrange dotPositionMatrix format
dotPosition_x=[];   dotPosition_y=[];
dotPosition_x=dotPositionMatrix(1,1:8:end);
dotPosition_y=dotPositionMatrix(2,1:8);

number_repeat=repmat([1:length(Xs)],1,1);

%number file
cd('D:\Yiko\Timing stuff\numberFlip') 
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['Reversal UD T=',num2str(Tvalue)];
name
% ori_dir =['D:\Yiko\HMM video frame\reversalBar'];   

% %video setting
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\HMM video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);

%dark rest first
for mm=1:trial_rest*fps
img=0*ones(1024,1280);   
writeVideo(writerObj, img);
end


for trialn=1:trials
    
%start part: adaptation: bar shows
for mm=1:rest0*fps
a=zeros(1024,1280);
X=Xs(1);
Y=Ys(1);
if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end

img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end


for kk =1:length(Xs)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%HMM RL bar trajectory
X=Xs(kk);
Y=Ys(kk);
if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end



%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;


%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '/', num2str(kk,'%06i'),'.jpeg']);
%imwrite conversion would change data value and class/ Indexed images are converted to RGB before writing out JPEG files

img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part: adaptation: bar rest and then disappear
for mm=1:rest1*fps
a=zeros(1024,1280);
X=Xs(end);
Y=Ys(end);
if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end


img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end

%dark rest btw trials
for mm=1:trial_rest*fps
img=[];
img=zeros(1024,1280);
writeVideo(writerObj, img);
end

end

%show the video is ended
for mm=1:5
img=[];
img=zeros(1024,1280);
img(500-35:500+35,1230:1280)=0.2; %gray
writeVideo(writerObj, img);
end
close(writerObj);

%save parameters needed
ori_dir02 =['D:\Yiko\HMM video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end


%%
clear all
%reversal D > U (correct direction on the monitor)
Tlist=[4];
for gg=Tlist
clearvars -except gg Tlist
load('D:\Yiko\HMM video frame\workspace_for_video') 

final=channel_map{66}; %U
init=channel_map{67}; %D

trials=12;
trial_rest=5; %sec
fps =60;        %freq of the screen flipping 
T=gg;
Tvalue=gg;
%second: time for one way
dt=1/fps;
T=dt:dt:T;

rest0=1; %sec
rest1=1; %sec
restlum=0.05;

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

clean_LED=clean_LED./255; 
clean_LED(clean_LED>1)=1;

X0 = round(init(1)); 
Y0 = round(init(2));
dx = (final(1)-init(1)) /length(T);
dy = (final(2)-init(2))/length(T);
Xs = zeros(1,2*length(T)-1); %multiply 2 because back and forth
Ys = zeros(1,2*length(T)-1);
Xs(1,1)=X0; %since the mean value of damped eq is zero
Ys(1,1)=Y0;
n=1;
% figure;
for i = 1:2*length(T)+1
        if i <= length(T)+1
            Xs(1,i) = round(X0 + (i-1) *dx);
            Ys(1,i) = round(Y0 + (i-1) *dy);
%               plot(i,Ys(i),'bo');hold on
        end       
        if i > length(T)+1
            Xs(1,i) = round(X0 + length(T)*dx - n *dx);
            Ys(1,i) =round( Y0 + length(T) *dy - n *dy);
            n=n+1;
%             plot(i,Ys(i),'r*');hold on
        end
end
% max(Ys)
% Ys(1)
% Ys(end)


%rearrange dotPositionMatrix format
dotPosition_x=[];   dotPosition_y=[];
dotPosition_x=dotPositionMatrix(1,1:8:end);
dotPosition_y=dotPositionMatrix(2,1:8);

number_repeat=repmat([1:length(Xs)],1,1);

%number file
cd('D:\Yiko\Timing stuff\numberFlip') 
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['Reversal DU T=',num2str(Tvalue)];
name
% ori_dir =['D:\Yiko\HMM video frame\reversalBar'];   

% %video setting
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\HMM video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);

%dark rest first
for mm=1:trial_rest*fps
img=0*ones(1024,1280);   
writeVideo(writerObj, img);
end


for trialn=1:trials
    
%start part: adaptation: bar shows
for mm=1:rest0*fps
a=zeros(1024,1280);
X=Xs(1);
Y=Ys(1);
if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end

img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end


for kk =1:length(Xs)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%HMM RL bar trajectory
X=Xs(kk);
Y=Ys(kk);
if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end



%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;


%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '/', num2str(kk,'%06i'),'.jpeg']);
%imwrite conversion would change data value and class/ Indexed images are converted to RGB before writing out JPEG files

img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part: adaptation: bar rest and then disappear
for mm=1:rest1*fps
a=zeros(1024,1280);
X=Xs(end);
Y=Ys(end);
if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end


img=[];
a(500-35:500+35,1230:1280)=restlum; %gray
img=a;
writeVideo(writerObj,  img);
end

%dark rest btw trials
for mm=1:trial_rest*fps
img=[];
img=zeros(1024,1280);
writeVideo(writerObj, img);
end

end

%show the video is ended
for mm=1:5
img=[];
img=zeros(1024,1280);
img(500-35:500+35,1230:1280)=0.2; %gray
writeVideo(writerObj, img);
end
close(writerObj);

%save parameters needed
ori_dir02 =['D:\Yiko\HMM video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end

