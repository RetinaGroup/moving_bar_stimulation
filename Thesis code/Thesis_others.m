%%  create axis plot for MC rack slow frequancy noise image
figure;
set(gca,'Position',[0.11 0.2  0.85 0.7])
ylabel('Voltage(\mum)')
xlabel('Time (ms)')
xlim([657 720])
ylim([-80 80])
set(gca, 'YTick', [-60 -20 20 60])
% set(gca,'XTickLabel', {'160','210','260'} )
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');


%% plot diode a_data3 lagged or skipped signal
%normal signal
load('D:\Yiko\Experiment\04232018\04232018 mat\0423HMM_1946_G053')
figure; plot(a_data(3,946000:954000));
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Luminance (W/m^{2})')
xlabel('Time (sec)')
xlim([0 8000])
ylim([34200 35000])
set(gca, 'XTick', [1000 3000 5000 7000])
set(gca,'XTickLabel', {'0.05','0.15','0.25','0.35'} ) %due to samplingrate=20000
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
%translate to lum unit
mulvalue=5100/max(a_data(3,946000:954000)); %5100 from lum measurement: mW/m^2
set(gca, 'YTick', [34200 34400 34600 34800 35000])
[34200 34400 34600 34800 35000]*mulvalue
set(gca,'YTickLabel', {'4.99','5.03','5.06','5.09','5.11'} )

%delayed signal
load('D:\Yiko\Experiment\04232018\04232018 mat\0423HMM_1946_G053')
figure; plot(a_data(3,555000:561000));
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Luminance (W/m^{2})')
xlabel('Time (sec)')
xlim([0 6000])
ylim([34200 35000])
set(gca, 'XTick', [1000 3000 5000 ]) 
set(gca,'XTickLabel', {'0.05','0.15','0.25'} ) %due to samplingrate=20000
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
%translate to lum unit
mulvalue=5100/max(a_data(3,946000:954000)); %5100 from lum measurement: mW/m^2
set(gca, 'YTick', [34200 34400 34600 34800 35000])
[34200 34400 34600 34800 35000]*mulvalue
set(gca,'YTickLabel', {'4.99','5.03','5.06','5.09','5.11'} )


%delayed signal at early time range
load('D:\Yiko\Experiment\04232018\04232018 mat\0423HMM_1946_G053')
figure; plot(a_data(3,154000:166000));
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Luminance (W/m^{2})')
xlabel('Time (sec)')
xlim([0 12000])
ylim([34200 35000])
set(gca, 'XTick', [2000 6000 10000 ])
set(gca,'XTickLabel', {'0.1','0.3','0.5'} ) %due to samplingrate=20000
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
%translate to lum unit
mulvalue=5100/max(a_data(3,946000:954000)); %5100 from lum measurement: mW/m^2
set(gca, 'YTick', [34200 34400 34600 34800 35000])
[34200 34400 34600 34800 35000]*mulvalue
set(gca,'YTickLabel', {'4.99','5.03','5.06','5.09','5.11'} )

%whole stimualtion region
load('D:\Yiko\Experiment\04232018\04232018 mat\0423HMM_1946_G053')
figure; plot(a_data(3,10:8530000));
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Luminance (W/m^{2})')
xlabel('Time (sec)')
xlim([0 8.5*10^6])
ylim([34200 35050])
set(gca, 'XTick', [1 3 5 7 9]*10^6)
set(gca,'XTickLabel', {'50','150','250','350','450'} ) %due to samplingrate=20000
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
%translate to lum unit
mulvalue=5100/max(a_data(3,946000:954000));
set(gca, 'YTick', [34200 34400 34600 34800 35000])
[34200 34400 34600 34800 35000]*mulvalue
set(gca,'YTickLabel', {'4.99','5.03','5.06','5.09','5.11'} )

%% Plot firing rate of P cells and NP cells for 3 G
%get the code from code" Compare barhist eachsliceSpikenumber
clear all
cd('D:\Yiko\Files for Thesis\04042018\sort2 pos2 HMM UD MI figure') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

count=1;
List_sp=[];
for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name
%binning
BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
%select cells
pre_cell=[38 34 52 30 32 ];
npre_cell=[35 40 28  49 33  ];


for i =1:length(pre_cell)
    [n,~] = hist(yk_spikes{pre_cell(i)},BinningTime) ;
    List_sp(count,i)=sum(n);
end
for i =1:length(npre_cell)
    [n,~] = hist(yk_spikes{npre_cell(i)},BinningTime) ;
    List_sp(count+1,i)=sum(n);
end
count=count+2;

figure;
plot([1:length(List_sp(count-1,:))],List_sp(count-2,:)/(length(BinningTime)*BinningInterval)  ,'ro'); hold on;
plot([1:length(List_sp(count-1,:))],List_sp(count-1,:)/(length(BinningTime)*BinningInterval),'bo');


set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Average firing rate (Hz)')
xlabel('Cell number')
xlim([0 6])
ylim([0.5 6])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'YTick', [1 2 3 4 5 6])

lgd = legend('Predictive cell','Nonpredictive cell');
lgd.FontSize = 13;
lgd.FontName = 'Times New Roman';
% legend boxoff  


end


%% bar position hist and firing rate
%get the code from code" Compare barhist eachsliceSpikenumber
%UD stimulation
clear all
cd('D:\Yiko\Files for Thesis\04042018\sort2 pos2 HMM UD MI figure') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

%z=1
 z =1;
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
schannel=[2    10    12    22    28    28    30    32        34       38    40    41    49    52]; %0404 UD
for i =schannel  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
range=zeros(8,8);
range(1,2:7)=[1:6];
range(8,2:7)=[55:60];
for i=2:7
    range(i,1:8)=[14+8*(i-2)-7:14+8*(i-2)];
end
for kk=1:8
    chcount=0;
    for i=1:8
        if find(schannel==range(kk,i)) ~= 0
            chcount=chcount+1;
        end
    end
    
fg=range(kk,find(range(kk,:)~=0))
ansforslice=0;
for jk=1:length(fg)
        ansforslice=ansforslice+sum(BinningSpike(fg(jk),:));
        fg(jk)
end
sumSpikes(kk)=ansforslice/chcount;
end

%z=1
%plot bar position histogram
figure; hist(bin_pos,8); hold on;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Count')
xlabel('Bar position (\mum)')
xlim([300 900])
ylim([0 5000])
set(gca, 'XTick',[353:63*2:794+63*2])
set(gca,'XTickLabel',[1:202*2:1417+202*2] )
set(gca, 'YTick', [1000:1000:5000])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

%firing dots
figure;
plot([353:63:794],sumSpikes/(length(BinningTime)*BinningInterval),'ro')
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylim([1.1 2.1])
xlim([300 900])
set(gca,'xtick',[])
set(gca,'ytick',[])

%axis
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ax1 = gca;
xlim([300 900])
ylabel('Average firing rate (Hz)')
ylim([1.1 2.1])
set(gca,'xtick',[])
set(gca, 'YTick', [1.1:0.2:2.1])
set(gca,'YTickLabel', [1.1:0.2:2.1])
set(gca,'YAxisLocation','right')
ax1.YColor='r';
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

%check:
figure;
plot([353:63:794],sumSpikes/(length(BinningTime)*BinningInterval),'ro')
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylim([1.1 2.1])
set(gca, 'YTick', [1.1:0.2:2.1])
set(gca,'YTickLabel', [1.1:0.2:2.1])
xlim([300 900])


%z=2
 z =2;
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
schannel=[2    10    12    22    28    28    30    32        34       38    40    41    49    52]; %0404 UD
for i =schannel  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
range=zeros(8,8);
range(1,2:7)=[1:6];
range(8,2:7)=[55:60];
for i=2:7
    range(i,1:8)=[14+8*(i-2)-7:14+8*(i-2)];
end
for kk=1:8
    chcount=0;
    for i=1:8
        if find(schannel==range(kk,i)) ~= 0
            chcount=chcount+1;
        end
    end
    
fg=range(kk,find(range(kk,:)~=0))
ansforslice=0;
for jk=1:length(fg)
        ansforslice=ansforslice+sum(BinningSpike(fg(jk),:));
        fg(jk)
end
sumSpikes(kk)=ansforslice/chcount;
end

%z=2
%plot bar position histogram
figure; hist(bin_pos,8); hold on;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Count')
xlabel('Bar position (\mum)')
xlim([300 900])
ylim([0 7000])
set(gca, 'XTick',[353:63*2:794+63*2])
set(gca,'XTickLabel',[1:202*2:1417+202*2] )
set(gca, 'YTick', [1000:1000:7000])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

%check:
figure;
plot([353:63:794],sumSpikes/(length(BinningTime)*BinningInterval),'ro')
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylim([0.9 1.9])
set(gca, 'YTick', [0.9:0.2:1.9])
set(gca,'YTickLabel', [0.9:0.2:1.9])
xlim([300 900])

%firing dots
figure;
plot([353:63:794],sumSpikes/(length(BinningTime)*BinningInterval),'ro')
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylim([0.9:1.9])
xlim([300 900])
set(gca,'xtick',[])
set(gca,'ytick',[])

%axis
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ax1 = gca;
xlim([300 900])
ylabel('Average firing rate (Hz)')
ylim([0.9:1.9])
set(gca,'xtick',[])
set(gca, 'YTick', [0.9:0.2:1.9])
set(gca,'YTickLabel', [0.9:0.2:1.9])
set(gca,'YAxisLocation','right')
ax1.YColor='r';
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');


%z=3
 z =3;
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
schannel=[2    10    12    22    28    28    30    32        34       38    40    41    49    52]; %0404 UD
for i =schannel  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
range=zeros(8,8);
range(1,2:7)=[1:6];
range(8,2:7)=[55:60];
for i=2:7
    range(i,1:8)=[14+8*(i-2)-7:14+8*(i-2)];
end
for kk=1:8
    chcount=0;
    for i=1:8
        if find(schannel==range(kk,i)) ~= 0
            chcount=chcount+1;
        end
    end
    
fg=range(kk,find(range(kk,:)~=0))
ansforslice=0;
for jk=1:length(fg)
        ansforslice=ansforslice+sum(BinningSpike(fg(jk),:));
        fg(jk)
end
sumSpikes(kk)=ansforslice/chcount;
end

%z=3
%plot bar position histogram
figure; hist(bin_pos,8); hold on;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Count')
xlabel('Bar position (\mum)')
xlim([300 900])
ylim([0 6000])
set(gca, 'XTick',[353:63*2:794+63*2])
set(gca,'XTickLabel',[1:202*2:1417+202*2] )
set(gca, 'YTick', [1000:1000:6000])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

%check:
figure;
plot([353:63:794],sumSpikes/(length(BinningTime)*BinningInterval),'ro')

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylim([1.8 3.2])
set(gca, 'YTick', [1.8:0.2:3.2])
set(gca,'YTickLabel',  [1.8:0.2:3.2])
xlim([300 900])

%firing dots
figure;
plot([353:63:794],sumSpikes/(length(BinningTime)*BinningInterval),'ro')
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylim([1.8 3.2])
xlim([300 900])
set(gca,'xtick',[])
set(gca,'ytick',[])

%axis
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ax1 = gca;
xlim([300 900])
ylabel('Average firing rate (Hz)')
ylim([1.8 3.2])
set(gca,'xtick',[])
set(gca, 'YTick', [1.8:0.2:3.2])
set(gca,'YTickLabel',  [1.8:0.2:3.2])
set(gca,'YAxisLocation','right')
ax1.YColor='r';
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

%% Plot autocorr: HMM and OU
% line([-50 58],[0.5 0.5],get(gca, 'ylim'),'Color','b','LineStyle','--','linewidth',2);  
% line([58 58],[-0.2 0.5],get(gca, 'ylim'),'Color','b','LineStyle','--','linewidth',2);  

load('D:\Yiko\Images for thesis_NV\workspace_newx_newXarray')   %HMM
[acf,lags,bounds] = autocorr(newXarray,200);
figure;
plot(lags,acf); hold on;
[acf,lags,bounds] = autocorr(new_x,200);
plot(lags,acf); hold on;

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( 'Lagged Frame');ylabel('Correlation');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
ylim([-0.1 1.1])

lgd = legend('HMM', 'OU');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  

  
%% autoMI HMM and OU
clear all;
figure;
for z =1:2
load('D:\Yiko\Images for thesis_NV\workspace_newx_newXarray')   %HMM

if z==1
TheStimuli=newXarray;
else
  TheStimuli=new_x;  
end

BinningInterval=0.016;
bin=BinningInterval*10^3; %ms


StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 

%MI
backward=ceil(3000/bin); forward=ceil(3000/bin);
Neurons=isi2;

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
        norm = 1; %bits

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
        norm = 1; %bits

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
 
time=[-backward*bin:bin:forward*bin];
plot(time,information,'linewidth',1); hold on

end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('AutoMI (bits)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2000 2000])
ylim([0 5])

lgd = legend('HMM', 'OU');
lgd.FontSize = 15;
 lgd.FontName = 'Times New Roman';
legend boxoff  

%% bar of firing rate with error bar  for P and N cell
%data from excel : video result t test: 3 data" 4/23, 4/25, 4/26
clear all
meanfiringrate=[0.72534   1.104252   1.333844 ]; %  from 4/23,4/25,4/26 data
err=[0.543   0.661782   0.842114 ]; %std
ppos=[1  1.5 2 ];
figure;
b=bar(ppos,meanfiringrate,0.1, 'FaceColor',[0    0.4470    0.7410]); hold on;
errorbar(ppos,meanfiringrate,err,'k.'); hold on;
ylim([0 3.5])
xlim([0.8 2.6])
set(gca,'Position',[0.17 0.2  0.75 0.7])
lgd = legend('P cell','Location',[0.7405 0.8175 0.1554 0.0508]);
lgd.FontSize = 15;
 lgd.FontName = 'Times New Roman';
legend boxoff  
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'XTick', [1.05 1.55 2.05])
set(gca,'XTickLabel', {'1.28 ','0.9 ','0.16'} )
ylabel('Averaged firing rate (Hz)')
xlabel('\tau (sec)')

meanfiringrate=[  1.18171   1.562771  1.547944]; %  from 4/23,4/25,4/26 data
err=[1.141599  1.599732    1.438463]; %std
figure;
d=0.1;
ppos=[ 1+d  1.5+d   2+d];
b=bar(ppos,meanfiringrate,0.1, 'FaceColor',[    0.8500    0.3250    0.0980]); hold on;
errorbar(ppos,meanfiringrate,err,'k.')
ylim([0 3.5])
xlim([0.8 2.6])
set(gca,'Position',[0.17 0.2  0.75 0.7])
lgd = legend('N cell','Location',[0.7405 0.8175-0.05 0.1554 0.0508]);
% lgd = legend('N cell','Location',[0.7405 0.8175 0.1554 0.0508])
lgd.FontSize = 15;
 lgd.FontName = 'Times New Roman';
legend boxoff  
set(gca, 'XTick', [   ])
set(gca, 'YTick', [   ])

%% bar of firing rate with error bar  for direction-selective 
%data from excel : video result t test: 3 data" 4/23, 4/25, 4/26
clear all
meanfiringrate=[1.658 1.54 1.429]; % predictive dir, orthagonal dir, diag
err=[0.756 0.616 0.844]; %std

figure;
b=bar(meanfiringrate); hold on;
errorbar(meanfiringrate,err,'r.'); hold on;

ylim([0 3])
set(gca,'Position',[0.17 0.2  0.75 0.7])
set(gca, 'XTick', [1 2 3 ])
set(gca,'XTickLabel', {'Pred','Orth ','Diag'} )
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
ylabel('Averaged firing rate (Hz)')
xlabel('Moving direction')


%% bar of firing rate with error bar  for spatial distribution
%data from excel : video result: firing rate spatial dist
% 3 data" 4/23, 4/25, 4/26
%G25
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G025')
[f, x] =hist(bin_pos,8); hold on;
figure;
bar(x, fliplr(13*f / sum(f)),'FaceColor', 'w','EdgeColor',[    0.8500    0.3250    0.0980]); hold on

x = [410  473 536 599  661 724  787 850];
meanfiringrate=[1.12 2.14 0.74 0.39 0.64 0.78 0.6 0]; 
err=[0.62 1.83 0.21 0 0.4 0.47 0.46 0]; %std
errorbar(x,meanfiringrate,err,'.')

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlim([350 950])
ylim([0 4.5])
set(gca, 'XTick', x )
set(gca,'XTickLabel', {'1','2 ','3','4','5','6','7','8'} )
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
ylabel('Averaged firing rate (Hz)')
xlabel('Column')

%G43
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G043')
[f, x] =hist(bin_pos,8); hold on;
figure;
bar(x, fliplr(15*f / sum(f)),'FaceColor', 'w','EdgeColor',[    0.8500    0.3250    0.0980]); hold on

x = [410  473 536 599  661 724  787 850];
meanfiringrate=[1.29 2.86 1.29 0.77 0.82 1.14 1.06 0]; 
err=[0.61 2.58 0.54 0 0.4 0.97 0.7 0]; %std
errorbar(x,meanfiringrate,err,'.')

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlim([350 950])
ylim([0 6])
set(gca, 'XTick', x )
set(gca,'XTickLabel', {'1','2 ','3','4','5','6','7','8'} )
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
ylabel('Averaged firing rate (Hz)')
xlabel('Column')

%G20
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G20')
[f, x] =hist(bin_pos,8); hold on;
figure;
bar(x, fliplr(15*f / sum(f)),'FaceColor', 'w','EdgeColor',[    0.8500    0.3250    0.0980]); hold on

x = [410  473 536 599  661 724  787 850];
meanfiringrate=[ 1.37 2.74 0.92 0.31 1.21 1.13 1.76 0]; 
err=[1.06 2.16 0.39 0 0.78 0.82 0.77 0]; %std
errorbar(x,meanfiringrate,err,'.')

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlim([350 950])
ylim([0 6])
set(gca, 'XTick', x )
set(gca,'XTickLabel', {'1','2 ','3','4','5','6','7','8'} )
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
ylabel('Averaged firing rate (Hz)')
xlabel('Column')


