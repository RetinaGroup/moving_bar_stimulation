Screen('Preference', 'SkipSyncTests', 1)%%%%%skipping the screen check
clear global all
global rect w
% screens=Screen('Screens');
% % screenNumber=max(screens);
% % white=WhiteIndex(screenNumber);
% % black=BlackIndex(screenNumber);
% % grey=white/4;
% %   [w, rect] = Screen('OpenWindow',2, [255 255 255]); %white background
  [w, rect] = Screen('OpenWindow',1, [0 0 0]); %[0 0 0] is black background / "1"(first) or "2"(second) is the monitor number

%% Step 1. global these important variables first
global init final stop_flag  rest0 rest1;
global  fps T X Y trials
global   clean_LED   bar_le  bar_wid

%% Step 2. load "BasicParameters_05102017" and "channel_map05042017", since "global" would set those parameters to empty matrix
%% Remind: for each directio: first run that section(especially its global) by hand!!!

%% 
%RL (12.13)
% init=channel_map{68}; %R
% final=channel_map{69}; %L
%LR
% init=channel_map{69}; %L
% final=channel_map{68}; %R

%UD
% init=channel_map{66}; %U
% final=channel_map{67}; %D
%DU
% init=channel_map{67}; %D
% final=channel_map{66}; %U

% % Diagnal 19-46
init=channel_map{19}; %top-left
final=channel_map{46}; %bottom-right
% % Diagnal 43-22
% init=channel_map{43}; %top-right
% final=channel_map{22}; %bottom-left

%%
trials=5;
fps =60;  %freq of the screen flipping 
T=0.7;%second: time for one way
dt=1/fps;
T=0:dt:T;

X0 = init(1); 
Y0 = init(2);
dx = (final(1)-init(1)) /length(T);
dy = (final(2)-init(2))/length(T);

rest0=1; %sec %rest before stimualtion
rest1=1; %sec %rest after stimulation

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

daq.Pace = daqmx_Task('dev1/ao0');
daq.Pace.write(-0.5);

%% Same Y
% set init=R, final=L
if final(2) == init(2)
X = zeros(1,length(T)); 
Y = zeros(1,length(T));
X(1,1)=X0; % since the mean value of damped eq is zero
Y(1,1)=Y0;
n=0;
for i = 1:length(T)+1
            X(1,i) = X0 + (i-1) *dx;
            Y(1,i) = Y0 + (i-1) *dy;
end

%% Show Time
for trialn=1:trials
   trialn
%draw bar
X2=X(1,1); Y2=Y(1,1);
barX=round(X2)-round(leftx_bm);
barY=round(Y2)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);  %draw bar

Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest0
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

daq.Pace.write(0.5);
for runi=1:length(X)
flick_time = 0;
start_time = GetSecs;
thetime = GetSecs - start_time;

X2=X(1, runi);  Y2=Y(1, runi); 
%draw bar
barX=round(X2-round(leftx_bm));
barY=round(Y2-round(lefty_bm));
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);
Screen('Flip', w);

% set next flicker T
flick_time = flick_time + 1/fps; 
    while thetime < flick_time 
        thetime = GetSecs - start_time; % T (sec) since loop started    
        if stop_flag == 1;    break; end
    end
if stop_flag == 1;    break; end   
end

daq.Pace.write(-0.5);
X2 = X(1,end); Y2=Y(1,end);
%draw bar
barX=X2-round(leftx_bm);
barY=Y2-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2 ,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);
Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest1
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

Screen('Flip', w);
start_time = GetSecs; 
while GetSecs - start_time< 2  %2 sec between trials
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

end

end

%% Same X
%set final(2) > init(2)
if final(1) == init(1)
X = zeros(1,length(T)); %multiply 2 because back and forth
Y = zeros(1,length(T));
X(1,1)=X0; % since the mean value of damped eq is zero
Y(1,1)=Y0;
n=0;
for i = 1:length(T)+1
            X(1,i) = X0 + (i-1) *dx;
            Y(1,i) = Y0 + (i-1) *dy;
end

%% Show Time
for trialn=1:trials
trialn
%draw bar
X2=X(1,1); Y2=Y(1,1);
barX=round(X2)-round(leftx_bm);
barY=round(Y2)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);  %draw bar
Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest0
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

daq.Pace.write(0.5);
for runi=1:length(X)
flick_time = 0;
start_time = GetSecs;
thetime = GetSecs - start_time;

X2=X(1, runi);  Y2=Y(1, runi); 
%draw bar
barX=round(X2-round(leftx_bm));
barY=round(Y2-round(lefty_bm));
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);
Screen('Flip', w);

% set next flicker T
flick_time = flick_time + 1/fps; 
    while thetime < flick_time 
        thetime = GetSecs - start_time; % T (sec) since loop started    
        if stop_flag == 1;    break; end
    end
if stop_flag == 1;    break; end   

end

daq.Pace.write(-0.5);
X2 = X(1,end); Y2=Y(1,end);
%draw bar
barX=X2-round(leftx_bm);
barY=Y2-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2 ,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);
Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest1
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

Screen('Flip', w);
start_time = GetSecs; 
while GetSecs - start_time<1.2
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

end

end

%% Diagnal movement
if final(1)~=init(1) && final(2) ~= init(2)

%% 19(init)-46(final) Diagnal
if init(1)<final(1)
X0 = init(1); Y0 = init(2);
dx = (final(1)-init(1))/length(T);
dy =  (final(2)-init(2)) /length(T);
X = zeros(1,length(T)); %multiply 2 because back and forth
Y = zeros(1,length(T));
X(1,1)=X0; 
Y(1,1)=Y0;
n=0;
for i = 1:length(T)+1
            % 19-46
%             X(1,i) = X0 + (i-1) *dx;
%             Y(1,i) = Y0 + (i-1) *dy;

%             %for reverse direction (46-19)
            X(1,i) = X0 + round(length(T)) *dx - n *dx;
            Y(1,i) = Y0 + round(length(T)) *dy - n *dy;
            n=n+1;
            
end
X=round(X);
Y=round(Y);

%% Show Time
for trialn=1:trials
    trialn
%draw bar
X2=X(1,1); Y2=Y(1,1);
center_backg=(length(diagBackg(1,:))-1)/2+1;
accor_len=(init(1)+final(1))/2-center_backg;  
%through comparing center value of screen position and diagBackg matrix,
%get the value that need to add for each diagBackg element to get correct corresponding position on screen
barX=X2-accor_len;
barImage=diagBackg(:, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, 45, [], []);
Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest0
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

daq.Pace.write(0.5);
for runi=1:length(X)
flick_time = 0;
start_time = GetSecs;
thetime = GetSecs - start_time;

X2=X(1, runi);  Y2=Y(1, runi); 
%draw bar
center_backg=(length(diagBackg(1,:))-1)/2+1;
accor_len=(init(1)+final(1))/2-center_backg;  
%through comparing center value of screen position and diagBackg matrix,
%get the value that need to add for each diagBackg element to get correct corresponding position on screen
barX=X2-accor_len;
barImage=diagBackg(:, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, 45, [], []);
Screen('Flip', w);

% set next flicker T
flick_time = flick_time + 1/fps; 
    while thetime < flick_time 
        thetime = GetSecs - start_time; % T (sec) since loop started    
        if stop_flag == 1;    break; end
    end
if stop_flag == 1;    break; end   

end

daq.Pace.write(-0.5);
X2 = X(1,end); Y2=Y(1,end);
%draw bar
center_backg=(length(diagBackg(1,:))-1)/2+1;
accor_len=(init(1)+final(1))/2-center_backg;  
%through comparing center value of screen position and diagBackg matrix,
%get the value that need to add for each diagBackg element to get correct corresponding position on screen
barX=X2-accor_len;
barImage=diagBackg(:, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, 45, [], []);
Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest1
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

Screen('Flip', w);
start_time = GetSecs; 
while GetSecs - start_time< 2
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

end

else
%% 43(init)-22(final) Diagnal
X0 = init(1); Y0 = init(2);
dx = (final(1)-init(1))/length(T);
dy =  (final(2)-init(2)) /length(T);
X = zeros(1,length(T)); %multiply 2 because back and forth
Y = zeros(1,length(T));
X(1,1)=X0; 
Y(1,1)=Y0;
n=0;
for i = 1:length(T)+1

%             X(1,i) = X0 + (i-1) *dx;
%             Y(1,i) = Y0 + (i-1) *dy;

% for reverse direction(22-43)
            X(1,i) = X0 + round(length(T)) *dx - n *dx;
            Y(1,i) = Y0 + round(length(T)) *dy - n *dy;
            n=n+1;
end
X=round(X);
Y=round(Y);

%% Show Time
for trialn=1:trials
%draw bar
X2=X(1,1); Y2=Y(1,1);
center_backg=(length(diagBackg(1,:))-1)/2+1;
accor_len=(init(1)+final(1))/2-center_backg;  
%through comparing center value of screen position and diagBackg matrix,
%get the value that need to add for each diagBackg element to get correct corresponding position on screen
barX=X2-accor_len;
barImage=diagBackg(:, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, -45, [], []);
Screen('Flip', w);


start_time = GetSecs; 
while GetSecs - start_time<rest0
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

daq.Pace.write(0.5);
for runi=1:length(X)
flick_time = 0;
start_time = GetSecs;
thetime = GetSecs - start_time;

X2=X(1, runi);  Y2=Y(1, runi); 
%draw bar
center_backg=(length(diagBackg(1,:))-1)/2+1;
accor_len=(init(1)+final(1))/2-center_backg;  
%through comparing center value of screen position and diagBackg matrix,
%get the value that need to add for each diagBackg element to get correct corresponding position on screen
barX=X2-accor_len;
barImage=diagBackg(:, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, -45, [], []);
Screen('Flip', w);

% set next flicker T
flick_time = flick_time + 1/fps; 
    while thetime < flick_time 
        thetime = GetSecs - start_time; % T (sec) since loop started    
        if stop_flag == 1;    break; end
    end
if stop_flag == 1;    break; end   

end

daq.Pace.write(-0.5);
X2 = X(1,end); Y2=Y(1,end);
%draw bar
center_backg=(length(diagBackg(1,:))-1)/2+1;
accor_len=(init(1)+final(1))/2-center_backg;  
%through comparing center value of screen position and diagBackg matrix,
%get the value that need to add for each diagBackg element to get correct corresponding position on screen
barX=X2-accor_len;
barImage=diagBackg(:, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X2,Y2);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, -45, [], []);
Screen('Flip', w);

start_time = GetSecs; 
while GetSecs - start_time<rest1
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

Screen('Flip', w);
start_time = GetSecs; 
while GetSecs - start_time< 2
%         if KbCheck;Screen('Close',w); break; end % exit loop upon key press
    if stop_flag == 1;    break; end
end

end
    
end
end

%%
Screen('Flip', w);
figure(1) 
 


