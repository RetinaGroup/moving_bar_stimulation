clear all
cd('D:\Yiko\Files for Thesis\03292018\New folder') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;
% figure;
% hist(abs(diff(bin_pos)),10)

% bar position STA
ch_list=[ 15 ]; %choose channel
for channel=ch_list
    STA_frame=[];
    STA_frame_past=[]; 
    STA_frame_future=[]; 
    barpos_record=[];
for z =[ 1  8] % choose file
clearvars -except all_file n_file z  channel ch_list minp maxp barpos_record STA_frame_future   STA_frame_past STA_frame
cd('D:\Yiko\Files for Thesis\03292018\New folder') ;  % the folder of the files
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

%rescale the number of bin_pos
reStim=bin_pos-min(bin_pos)+12; % let the first element be at number 12 in the 503x503 matrix

%make BinningSpike
BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ; %yk_spike already cut off before and after stimulation
    BinningSpike(i,:) = n ;
end

bar_wid=11;
stim_size=503+2*bar_wid; 
before_frames=100; %choose how many frames you want before a spike
future_frames=100; %choose how many frames you want after a spike

barpos_record_past=[];
barpos_record_fut=[];

find_spikes=[]; 
find_spikes=find(BinningSpike(channel,:) > 0 );  %find spikes location

%Past part
for frame=1:before_frames+1  %+1: including zeroth frame
temp=zeros(stim_size,stim_size);
sum_spikes=0;
barpos_record_temp=[];
count=1;
        for h=find_spikes   
                if h-frame <=0
                         h=h;
                else
                    %make bar image for each frame
                    tempFrame=zeros(stim_size,stim_size);
                    barcenter=reStim(h-frame+1); %+1: including zeroth frame
                    % moving bar image (for all directions) just plot all dircetions the same
                    tempFrame(barcenter-11:barcenter+11,:)=1; %for both RL and UD
                    
                    %weighting with firing rate
                    temp=temp+BinningSpike(channel,h)*tempFrame; 
                    
                    barpos_record_temp(1,count)=BinningSpike(channel,h)*barcenter;
                    count=count+1;
                end
         sum_spikes=sum_spikes+BinningSpike(channel,h);
        end
        
barpos_record_past(frame)=sum( barpos_record_temp)/sum_spikes;

%weighting with firing rate
% STA_frame_past{channel,frame}=temp./sum_spikes;
STA_frame_past{z,frame}=temp./sum_spikes;
end

%Future part
for frame=1:future_frames
temp=zeros(stim_size,stim_size);
sum_spikes=0;
barpos_record_temp=[];
count=1;
        for h=find_spikes
                if h+frame >=length(reStim)
                         h=h;
                else
                    %make bar image for each frame
                    tempFrame=zeros(stim_size,stim_size);
                    barcenter=reStim(h+frame);
                     % moving bar image (for all directions) just plot all dircetions the same
                    tempFrame(barcenter-11:barcenter+11,:)=1;
                    
                    %weighting with firing rate
                    temp=temp+BinningSpike(channel,h)*tempFrame; 
                    
                    barpos_record_temp(1,count)=BinningSpike(channel,h)*barcenter;
                    count=count+1;
                end
         sum_spikes=sum_spikes+BinningSpike(channel,h);
        end

barpos_record_fut(frame)=sum( barpos_record_temp)/sum_spikes;
        
%weighting with firing rate
% STA_frame_future{channel,frame}=temp./sum_spikes;
STA_frame_future{z,frame}=temp./sum_spikes;
end


barpos_record(z,:)=[fliplr(barpos_record_fut) barpos_record_past];  %flip left and right is to link past and future series together
STA_frame=[fliplr(STA_frame_future)  STA_frame_past]; %flip left and right is to link past and future series together


%% Plot bar RF
dd=ceil((before_frames+1+future_frames)^(1/2)); %+1: including zeroth frame
hch=channel; %channel number
h2=figure;
set(h2, 'Position', [10 250 630 630]);
num=future_frames;

for i=1:before_frames+future_frames+1 %+1: including zeroth frame
subplot(dd,dd,i); 
shimage=imrotate(STA_frame{z,i},-90); %for UD  / correspond to orientation of MEA 
% shimage=flipud(STA_frame{z,i});  %for RL : flip udside down for corresponding orientation to MEA 
imagesc(shimage);
colormap(gray);
title(num);
num=num-1;
end
annotation('textbox', [0.1 0.95  0.1 0.02],'String',{name});

str =[ 'channel  ',num2str(channel)];
yy=annotation('textbox',[0.45 .8 .2 .2],'String',str,'FitBoxToText','on');


minp(1,z)= min(barpos_record(z,:)) ;
maxp(1,z)= max(barpos_record(z,:)) ;
end

%% Plot max lum position of STA_frame
max_STA_frame=[];
figure; 
for z=1
for jj=1:length(STA_frame)
    %for RL : flip udside down for corresponding orientation to MEA 
%           qoo=flipud(STA_frame{z,jj});  
%           max_STA_frame{z,jj}=mean(find(qoo(:,1)==max(qoo(:,1))));

          %for UD  / correspond to orientation of MEA 
          qoo=imrotate(STA_frame{z,jj},-90);  
          max_STA_frame(jj)=mean(find(qoo(1,:)==max(qoo(1,:))));

end
plot(max_STA_frame);
hold on;
end

%% plot single frame
select_frame=18;
select_z=1;
figure;
shimage=imrotate(STA_frame{select_z,select_frame},-90); %for UD  / correspond to orientation of MEA 
% shimage=flipud(STA_frame{z,i});  %for RL : flip udside down for corresponding orientation to MEA 
imagesc(shimage);
colormap(gray);
title(select_frame);


save(['D:\Yiko\Files for Thesis\03292018\0329 OU STA barpos 100frames 3G_ch ',num2str(channel),'.mat'],'STA_frame','barpos_record','before_frames','future_frames')


end


%% 2D plotting 
%load mat  files
clear all
cd('D:\Yiko\Files for Thesis\04262018\0426 barposSTA\0426 STA new pics') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for z=1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

numOrder=[101-38 ,101-25:3:101-4, 101, 101+4:3:101+25, 101+38];
secOrder=[  608  400   352   304   256   208   160   112    64    0     -64   -112  -160  -208  -256   -304   -352   -400  -608];
GOrder=[025 053 20];


for G=[1 2 3 ]
        h2=figure;
        set(h2, 'Position', [10 250 630 630]);
for i=1:length(numOrder)
subplot(5,5,i); 

% shimage=imrotate(STA_frame{G,numOrder(i)},-90); %for UD  / correspond to orientation of MEA 
shimage=flipud(STA_frame{G,numOrder(i)});  %for RL : flip udside down for corresponding orientation to MEA 

imagesc(shimage);
colormap(gray);
 title([num2str(secOrder(i)),' ms']);
end
str ={'G', GOrder(G)};
yy=annotation('textbox',[0.0968253968253968 0.949206349206349 0.0984126984126984 0.0366666666666665],'String',str,'FitBoxToText','on');
 annotation('textbox', [0.34 0.950793650793651 0.463174603174603 0.0362063492063491],'String',{name});

 
% saveas(gcf,['D:\Yiko\Files for Thesis\04262018\0426 barposSTA\0426 2D STA jumped frames plot\2D jp G',num2str(GOrder(G)),'_',name],'fig');
end

end







