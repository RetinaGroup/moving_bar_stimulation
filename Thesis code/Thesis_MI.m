%% HMM MI - single tau
% close all
clear all
load('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs\03 merge sort1 unit1 _HMM RL G043')
channelnumber=44;
figure;
% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;

% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms
% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;
% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
% for channelnumber=34
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
time=[-backward*bin:bin:forward*bin];
plot(time,information,'linewidth',2); hold on
plot(time,information_shuffle,'linewidth',2); hold on


set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2300 2300])
ylim([0.1 1.3])
set(gca, 'XTick', [-2000 -1000 0 1000 2000])
set(gca, 'YTick', [0.3 0.6 0.9 1.2 1.5])
box on




%% HMM MI peak position - 5 tau moving forward
% close all
clear all
cd('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs just differnet order') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[21]
figure;

for z =[1 2 4 7 8]   %[1,5,8]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;
% if z==4
% TheStimuli=newYarray; 
% else
% TheStimuli=newXarray; 
% end
% TheStimuli=newYarray; 
% TheStimuli=new_y; 
% % figure; autocorr(TheStimuli, 1000); %check!

% TheStimuli=new_y; 
% figure; autocorr(TheStimuli, 1000); %check!

%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
% fps=1/60;
% multiple_value=1;
% % multiple_value=[1,2,3] correspond to binning time=[0.016, 0.008, 0.0056(s)]
% BinningInterval = DataTime/(multiple_value*length(TheStimuli)); %s
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

% BinningTime = [ 0 : BinningInterval : DataTime]; 
% %1 more element is due to the transmission time of daq
% %DataTime(calculated by daq difference ~300.17 s) would > 5*60 s
% d=length(BinningTime) - length(TheStimuli);
% if length(BinningTime) < length(TheStimuli)
%     sprintf('length of BinningTime is smaller!!')  
% end
% if length(BinningTime) > length(TheStimuli)
%     sprintf('length(BinningTime) - length(TheStimuli)= %0.5f',d) 
%     BinningTime=BinningTime(1:length(BinningTime)-d); % drop the last number of BinningTime
% end

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
% for channelnumber=34
n = channelnumber;

Neurons = BinningSpike(n,:);  %for single channel

%Neurons = sum(BinningSpike(1:60,:));  %calculate population MI

% Neurons=isi2;

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 
time=[-backward*bin:bin:forward*bin];


% plot(time,information,'linewidth',2); hold on
% plot(time,information_shuffle,'linewidth',2);
plot(time,information-information_shuffle,'linewidth',2); hold on

%save workspace
ptin=information-information_shuffle;
save(['D:\Yiko\Images for thesis_NV\image workspace for origin\HMM predictive MI ',name,'.mat'],'time','ptin')

end

end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2300 2300])
ylim([0 1.6])
set(gca, 'XTick', [-2000 -1000 0 1000 2000])
set(gca, 'YTick', [0.3 0.6 0.9 1.2 1.5])
box on

lgd = legend('\tau =0.16 sec', '\tau =0.38 sec','\tau =0.55 sec','\tau =1.07 sec','\tau =1.28 sec');
lgd.FontSize = 15;
lgd.FontName = 'Times New Roman';
legend boxoff  








%% HMM MI 4 direction: 1 dir predictive
% close all
clear all
cd('D:\Yiko\Files for Thesis\04042018\0404 4 direction') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[33]
figure;

for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',1.5); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -1500 1500])
ylim([0 1.5])

lgd = legend('Dir 1', 'Dir 2','Dir 3','Dir 4');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  

%% HMM MI 4 direction: 2 dir predictive, 2 lag
% close all
clear all
cd('D:\Yiko\Files for Thesis\04252018\pos2 merge sortD 4 direction') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[19]
figure;

for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(10000/bin); forward=ceil(10000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',1.5); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -10000 10000])
ylim([0 0.6])

lgd = legend('Dir 1', 'Dir 2','Dir 3','Dir 4');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  


%% HMM MI 4 direction: 3 dir predictive, 1 lag
% close all
clear all
cd('D:\Yiko\Files for Thesis\04232018\HMM pos2 4 direction G045') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[47]
figure;

for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',1.5); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2000 2000])
ylim([0 0.7])

lgd = legend('Dir 1', 'Dir 2','Dir 3','Dir 4');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  


%% HMM MI 4 direction: 3 dir predictive, one no curve
% close all
clear all
cd('D:\Yiko\Files for Thesis\04042018\0404 4 direction') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[38]
figure;

for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',1.5); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2000 2000])
ylim([0 1])

lgd = legend('Dir 1', 'Dir 2','Dir 3','Dir 4');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  



%% HMM MI 4 direction: 2 dir predictive, 2 no curve
% close all
clear all
cd('D:\Yiko\Files for Thesis\04252018\pos2 merge sortD 4 direction') ;  % the folder of the files
% cd('D:\Yiko\Files for Thesis\04142018\pos2 sort1 merge 4 direction HMM')
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[ 12]
figure;

for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(4000/bin); forward=ceil(4000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',1.5); hold on
% plot(time,information,'linewidth',1.5); hold on

end


end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2000 2000])
ylim([0 0.35])

lgd = legend('Dir 1', 'Dir 2','Dir 3','Dir 4');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  




%% HMM MI align
% close all
clear all
cd('D:\Yiko\Files for Thesis\04232018\sort HMM RL Tina ch 12 33') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[12]
figure;

for z =[1 2 3 5 6]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',2); hold on

%save workspace
ptin=information-information_shuffle;
save(['D:\Yiko\Images for thesis_NV\image workspace for origin\HMM align MI ',name,'.mat'],'time','ptin')

end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2500 2500])
ylim([0 0.65])

lgd = legend('\tau =0.16 sec', '\tau =0.38 sec','\tau =0.55 sec','\tau =1.07 sec','\tau =1.28 sec');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  




%% HMM MI peak long tau move backward
% close all
clear all
cd('D:\Yiko\Files for Thesis\04042018\sort pos2 HMM RL Tinach28') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[28]
figure;

for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',2); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -3000 3000])
ylim([0 1.25])

lgd = legend('\tau =0.16 sec', '\tau =0.43 sec','\tau =0.63 sec','\tau =0.9 sec','\tau =1.08 sec');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  




%% HMM MI peak long tau lagged 2nd peak
% close all
clear all
cd('D:\Yiko\Files for Thesis\04252018\sort1 merge pos2 HMM') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[11]
figure;

for z =[3]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(6000/bin); forward=ceil(6000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',2); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -6000 6000])
ylim([0 0.6])




%% HMM MI peak short tau 2 peak
% close all
clear all
cd('D:\Yiko\Files for Thesis\04232018\0423 merge sort1') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[28]
figure;

for z =[3]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(6000/bin); forward=ceil(6000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',2); hold on


end
end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2000 2000])
ylim([0 1.1])



%% HMM MI peak short tau 2 peak with 2 corr times
% close all
clear all
cd('D:\Yiko\Files for Thesis\04232018\sort HMM RL short tau 2 peak with 2 corr') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

figure;
for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

if z<3
    channelnumber=[28];
else
    channelnumber=[47];
end

%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(6000/bin); forward=ceil(6000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];

% if z==1
% plot(time,information-information_shuffle,'linewidth',2,'color',[ 0    0.4470    0.7410]); hold on
% elseif z==2
%     plot(time,information-information_shuffle,'--','linewidth',2,'color',[ 0    0.4470    0.7410]); hold on
% elseif z==3
%     plot(time,information-information_shuffle,'linewidth',2,'color',[     0.8500    0.3250    0.0980]); hold on
% elseif z==4
%     plot(time,information-information_shuffle,'--','linewidth',2,'color',[     0.8500    0.3250    0.0980]); hold on
% end

if z<3
plot(time,information-information_shuffle,'linewidth',2); hold on
else
    plot(time,information-information_shuffle,':','linewidth',2); hold on
end

%  get(gca,'colororder'); %get default matlab color order
 
end

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -1600 2000])
ylim([0 2.8])

lgd = legend('cell 1, \tau =0.17 sec', 'cell 1, \tau =0.9 sec','cell 2, \tau =0.17 sec', 'cell 2, \tau =0.9 sec');
lgd.FontSize = 13;
lgd.FontName =  'Times New Roman';
legend boxoff  



%% HMM MI peak long tau lagged 2 peak with 2 corr times
% close all
clear all
cd('D:\Yiko\Files for Thesis\04252018\sort1 merge pos2 HMM 1946 long tau lagged 2 peak 2 tau') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

figure;
for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

if z<3
    channelnumber=[19];
else
    channelnumber=[11];
end

%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;


%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(10000/bin); forward=ceil(10000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];

% if z==1
% plot(time,information-information_shuffle,'linewidth',2,'color',[ 0    0.4470    0.7410]); hold on
% elseif z==2
%     plot(time,information-information_shuffle,'--','linewidth',2,'color',[ 0    0.4470    0.7410]); hold on
% elseif z==3
%     plot(time,information-information_shuffle,'linewidth',2,'color',[     0.8500    0.3250    0.0980]); hold on
% elseif z==4
%     plot(time,information-information_shuffle,'--','linewidth',2,'color',[     0.8500    0.3250    0.0980]); hold on
% end

if z<3
plot(time,information-information_shuffle,'linewidth',2); hold on
else
    plot(time,information-information_shuffle,':','linewidth',2); hold on
end

%  get(gca,'colororder'); %get default matlab color order
 
end

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -6000 6000])
ylim([0 0.6])

lgd = legend('cell 1, \tau =0.17 sec', 'cell 1, \tau =0.9 sec','cell 2, \tau =0.17 sec', 'cell 2, \tau =0.9 sec');
lgd.FontSize = 12;
lgd.FontName =  'Times New Roman';
legend boxoff  



%% HMM MI peak long tau 2 peaks lagged behind
% close all
clear all;

figure;
for z =1

if z==1
load('D:\Yiko\Files for Thesis\04252018\sort1 merge pos2 HMM 1946 long tau lagged 2 peak 2 tau\4 ch11 merge sort1 unit1 _0425pos2 HMM 1946 G043')
channelnumber=[11];

else

% load('D:\Yiko\Files for Thesis\04262018\pos2 merge sort\merge sort1 _0426pos2 HMM 4322 G043')    
% channelnumber=[18];  
end

%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;

%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(8000/bin); forward=ceil(8000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel


dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'linewidth',2); hold on


clear all
end

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -6500 6500])
ylim([0 0.6])

% lgd = legend('cell 1 ', 'cell 2');
% lgd.FontSize = 12;
% lgd.FontName =  'Times New Roman';
% legend boxoff  




%% OU MI peak position - tau
% close all
clear all
cd('D:\Yiko\Files for Thesis\03292018\New folder') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[45]
figure;

for z =[1,2,4,6,8]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;

%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
% for channelnumber=34
n = channelnumber;

Neurons = BinningSpike(n,:);  %for single channel

%Neurons = sum(BinningSpike(1:60,:));  %calculate population MI

% Neurons=isi2;

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 
time=[-backward*bin:bin:forward*bin];


% plot(time,information,'linewidth',2); hold on
% plot(time,information_shuffle,'linewidth',2);
plot(time,information-information_shuffle,'linewidth',1); hold on



end

end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');

xlim([ -2300 2300])
ylim([0 1.5])
set(gca, 'XTick', [-2000 -1000 0 1000 2000])
set(gca, 'YTick', [0.3 0.6 0.9 1.2 1.5])
box on

lgd = legend('\tau =1.82 sec', '\tau =1.35 sec','\tau =0.75 sec','\tau =0.63 sec','\tau =0.3 sec');
lgd.FontSize = 15;
 lgd.FontName = 'Times New Roman';
legend boxoff  

%% HMM MI with sudden no peak case
clear all
cd('D:\Yiko\Files for Thesis\04042018\sort2 pos2 HMM RL MI figure') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[28]
figure;

for z =[3 4 6 7]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;

% Remember to replace the below "newXarray" to your actual stimuli array!!!
% Binning
bin=BinningInterval*10^3; %ms

% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
% for channelnumber=34
n = channelnumber;

Neurons = BinningSpike(n,:);  %for single channel

%Neurons = sum(BinningSpike(1:60,:));  %calculate population MI

% Neurons=isi2;

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 
time=[-backward*bin:bin:forward*bin];


% plot(time,information,'linewidth',2); hold on
% plot(time,information_shuffle,'linewidth',2);
plot(time,information-information_shuffle,'linewidth',1); hold on

end

end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([ -3000 3000])
ylim([0 1.3])

lgd = legend('\tau =0.68 sec', '\tau =0.63 sec','\tau =0.35 sec','\tau =0.17 sec');
lgd.FontSize = 15;
 lgd.FontName = 'Times New Roman';
legend boxoff  


%% HMM MI 2 repeated trial
clear all
cd('D:\Yiko\Files for Thesis\04232018\pos2 HMM RL G3 2 trial') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

for channelnumber=[23 37]
figure;

for z =1:2
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name



% put your stimulus here!!!!!!!!!!
TheStimuli=bin_pos;

% Remember to replace the below "newXarray" to your actual stimuli array!!!
% Binning
bin=BinningInterval*10^3; %ms

% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
% for channelnumber=34
n = channelnumber;

Neurons = BinningSpike(n,:);  %for single channel

%Neurons = sum(BinningSpike(1:60,:));  %calculate population MI

% Neurons=isi2;

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
 
time=[-backward*bin:bin:forward*bin];


% plot(time,information,'linewidth',2); hold on
% plot(time,information_shuffle,'linewidth',2);
plot(time,information-information_shuffle,'linewidth',1); hold on

end

end
set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits/sec)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([ -3000 3000])
% ylim([0 0.5])
ylim([0 0.6])

lgd = legend('Trial 1','Trial 2');
lgd.FontSize = 15;
 lgd.FontName = 'Times New Roman';
legend boxoff  




%% HMM MI peak-latency with error bar/  P cell
x=[1.283  0.9 0.783 0.633 0.466 0.383 0.166]; %corr time
y=[133 -6.6 -30 -69.6 -83 -99.6 -133 ]; %mean latency for these selected channels
err=[84.4 49.6 58.6 12.4 29.5 43.3 10.7];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('\delta t(ms)')
xlabel(' \tau(sec)')
set(gca, 'Fontname', 'Times New Roman');
box on
set(gca,'fontsize',15); 
set(gca, 'YTick', [-200 -100 0 100 200])
set(gca, 'XTick', [0.2 0.4 0.6 0.8 1 1.2 1.4 ])

%% HMM MI peak-latency with error bar/  N cell
x=[1.283  0.9 0.783 0.633 0.466 0.383 0.166]; %corr time
y=[-91 -116 -92 -107 -124 -119 -110]; %mean latency for these selected channels
err=[43.2  33.5 18.5 27.5 33 24.7 8.7];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('\delta t(ms)')
xlabel(' \tau(sec)')
set(gca, 'Fontname', 'Times New Roman');
box on
set(gca,'fontsize',15); 
ylim([-300 100])
set(gca, 'YTick', [-300 -200 -100 0 100])
set(gca, 'XTick', [0.2 0.4 0.6 0.8 1 1.2 1.4 ])



%% HMM MI peak-latency with error bar/  N cell
x=[1.283  0.9 0.783 0.633 0.466 0.383 0.166]; %corr time
y=[-91 -116 -92 -107 -124 -119 -110]; %mean latency for these selected channels
err=[43.2  33.5 18.5 27.5 33 24.7 8.7];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('\delta t(ms)')
xlabel(' \tau(sec)')
set(gca, 'Fontname', 'Times New Roman');
box on
set(gca,'fontsize',15); 
ylim([-300 100])
set(gca, 'YTick', [-300 -200 -100 0 100])
set(gca, 'XTick', [0.2 0.4 0.6 0.8 1 1.2 1.4 ])


%% HMM MI peak-width with error bar / type 3
x=[1.98 1.08 0.63 0.58 0.5 0.43 0.16]; %corr time
y=[-170.2 -85 87 -29.2 106 8.25 -89.2]; %mean width for these selected channels
err=[142.4 262.5 28.1 209.7 52.9 43.5 47.7];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('\delta t(ms)')
xlabel(' \tau(sec)')
set(gca, 'Fontname', 'Times New Roman');
box on
set(gca,'fontsize',15); 
ylim([-400 250])
xlim([0 2.2])
set(gca, 'YTick', [-400 -300 -200 -100 0 100 200])
set(gca, 'XTick', [0.2  0.6  1 1.4  1.8 2.2])


%% OU MI peak-latency with error bar

x=[1.8167    1.3500    0.9667    0.7500    0.6500    0.6333    0.4333    0.3000]; %corr time
y=[-180.8 -185.1 -185.1 -174.7 -170.5 -151.5 -176.7 -178.8]; %mean latency for these selected channels
err=[27.2 10.8 18.6 29.4 26.4 25.9 15.3 17.3];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('\delta t(ms)')
xlabel(' \tau_{corr}(sec)')
set(gca, 'Fontname', 'Times New Roman');
box on
set(gca,'fontsize',15); 
ylim([-250 -100])
set(gca, 'YTick', [-230 -200  -170 -140 -110])
set(gca, 'XTick', [0.2  0.6  1  1.4  1.8])


%% OU MI peak-width with error bar
x=[1.8167    1.3500    0.9667    0.7500    0.6500    0.6333    0.4333    0.3000]; %corr time
y=[44 18.5 20.9 16.3 13.6 13.9 10.1 9.3]; %mean width for these selected channels
err=[24.9 6.39 4.58 2.43 2.92 3.31 1.55 1.38];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Width(ms)')
xlabel(' \tau_{corr}(sec)')
set(gca, 'Fontname', 'Times New Roman');
box on
xlim([0.2 1.9])
set(gca,'fontsize',15); 
set(gca, 'YTick', [10 30 50 70])



%% Test firing rate HMM MI 4 direction: 1 dir predictive 
% close all
clear all
cd('D:\Yiko\Files for Thesis\04242018\sortD merge HMM 4 direction G043') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

%1 predictive
channelnumber=[1]
for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
% z
name

bin=BinningInterval*10^3; %ms
BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
[n,~] = hist(yk_spikes{channelnumber},BinningTime) ;
BinningSpike(channelnumber,:) = n ;
sum(BinningSpike(channelnumber,:))

figure;
plot([1:length(BinningSpike(channelnumber,:))],BinningSpike(channelnumber,:)/BinningInterval) 
title(name)

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate (Hz)')
xlabel('Time (sec)')
xlim([0 length(BinningSpike(channelnumber,:))])
% ylim([0.5 6])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'XTick',[2000 6000 10000 14000 18000])
set(gca,'XTickLabel',[33 100 166 233 300])

end

%% plot firing rate for 4 direction: 3 predictive
%3 predictive
clear all
cd('D:\Yiko\Files for Thesis\04042018\0404 4 direction') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;

channelnumber=[38]
% figure;
for z =[3 4 1 2 ]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


bin=BinningInterval*10^3; %ms
BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
[n,~] = hist(yk_spikes{channelnumber},BinningTime) ;
BinningSpike(channelnumber,:) = n ;

figure;
plot([1:length(BinningSpike(channelnumber,:))],BinningSpike(channelnumber,:)/BinningInterval) 


set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate (Hz)')
xlabel('Time (sec)')
xlim([0 length(BinningSpike(channelnumber,:))])
% ylim([0.5 6])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'XTick',[2000 6000 10000 14000 18000])
set(gca,'XTickLabel',[33 100 166 233 300])

end




