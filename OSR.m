%% OSR
cd('D:\Yiko\ExperimentalData\09052017_HMM_OU_direction\mat\OSR') ; % the folder of the files
all_file = subdir('*.mat') ; %Michael's "analyze_MEA_data" must use "subdir"!!!
n_file = length(all_file) ; 
for z=1:n_file
 file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

cut_spikes = seperate_trials(Spikes,TimeStamps); 
DataTime=TimeStamps(2)-TimeStamps(1);
% Binning
BinningInterval = 1/40;  %s
BinningTime = [ 0 : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(cut_spikes{i},BinningTime) ;
%     [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 
figure;  imagesc(BinningTime,[1:60],BinningSpike);   
title(name, 'Fontsize',16)
end

