%% Plot: single RF
load('D:\Yiko\Files for Thesis\12132017\retina2 pos1\all_merge_1213Re2pos1_onlysortsoma')

max_frame=cell(1,60); %for plotting 60 RF
min_frame=cell(1,60);

for channel=[19]
array_1ch=[];  %1 channel's STA checkerboard frame images
array_1ch=STA_frame(channel,:); 

%determine absolute max value from the averaged checkerboard images
%for the normalization used below
MAX=[];
if max(max(cell2mat(array_1ch)))>-min(min(cell2mat(array_1ch)))
    MAX=max(max(cell2mat(array_1ch)));
else
    MAX=-min(min(cell2mat(array_1ch)));
end

% change back to nthu mseq data form to draw using their codes
STA_frame2=[];
for g=1:length(array_1ch)
        STA_frame2(g,1:checker_n^2)=reshape(array_1ch{g},[1,checker_n^2]);
end
STA_frame2=STA_frame2./MAX;    %divided by the absolute max of all time and of all pixel

normtSTA=[];
for i=1:checker_n^2
        % get the 20 frames before each spike
        normtsta=flipud(STA_frame2(1:fps,i))';   %flipud: flip up to down   %to show temporal sta (x=36: 1st frame, x=1: 36th frame)
        normtSTA=[normtSTA;normtsta]; %take the next normtsta to next row, so each row represent each pixel through 36, 35,...,1 frames
end

%for unselected/broken channels
if MAX ==0
    normtSTA=ones(checker_n^2,fps);
end

%% Find max/min lum pixel
MAXsite=[];  MINsite=[];
[rows,cols]=find(normtSTA==max(max(normtSTA)));   %find the max timing and the pixel
MAXsite=[rows,cols];

[rows2,cols2]=find(normtSTA==min(min(normtSTA))); % rows is the pixel number has min normtSTA. cols is the frame has min normtSTA.
MINsite=[rows2,cols2];


%% plot the frame you want
frame_chosed=3;
Peak1sSTA=reshape(STA_frame2(frame_chosed,:),checker_n,checker_n,[]); %find the 256 pixels
% imagesc(Peak1sSTA);
P=flipud(rot90(Peak1sSTA,3)); %make it to fit Tina's coordination
figure;
imagesc(P);
pbaspect([1 1 1]) ;%make it square
 colormap(gray);
% axis square;
axis off;
% colorbar

% plot mea electrode position
hold on;
Numb=find(cell2mat(mea_num)==channel);
ror=cell2mat(mea_pos(Numb));
p1=ror(2);
p2=ror(1);
plot(p2,p1,'.','Color','r','MarkerSize',13)

%extract the final image
F = getframe(gcf);
[X,Map] = frame2im(F);
figure; imagesc(X)
XNi=X(32:374,119:462,:);
figure; imagesc(XNi)
pbaspect([1 1 1])
axis off;

cbr=colorbar;
set(gca,'fontsize',15);

%% Gaussion filter
Iblur=imgaussfilt(Peak1sSTA,1);
P=flipud(rot90(Iblur,3)); %make it to fit Tina's coordination
figure;
imagesc(P);
pbaspect([1 1 1]) ;%make it square
 colormap(gray);
% axis square;
axis off;
cbr=colorbar;
set(gca,'fontsize',15);


figure; imagesc(Iblur)
 colormap(gray);
 pbaspect([1 1 1]) ;%make it square
end


%% Plot lots of RF
load('D:\Yiko\Files for Thesis\12132017\retina2 pos1\all_merge_1213Re2pos1_onlysortsoma')
ch_list=[6 9 15 18 19 23 26 33 53 55 59];
frame_list=[4 3 4 4 3 3 4 4 5 4 4];
cfframe=1;
for channel=ch_list
array_1ch=[];  %1 channel's STA checkerboard frame images
array_1ch=STA_frame(channel,:); 

%determine absolute max value from the averaged checkerboard images
%for the normalization used below
MAX=[];
if max(max(cell2mat(array_1ch)))>-min(min(cell2mat(array_1ch)))
    MAX=max(max(cell2mat(array_1ch)));
else
    MAX=-min(min(cell2mat(array_1ch)));
end

% change back to nthu mseq data form to draw using their codes
STA_frame2=[];
for g=1:length(array_1ch)
        STA_frame2(g,1:checker_n^2)=reshape(array_1ch{g},[1,checker_n^2]);
end
STA_frame2=STA_frame2./MAX;    %divided by the absolute max of all time and of all pixel

normtSTA=[];
for i=1:checker_n^2
        % get the 20 frames before each spike
        normtsta=flipud(STA_frame2(1:fps,i))';   %flipud: flip up to down   %to show temporal sta (x=36: 1st frame, x=1: 36th frame)
        normtSTA=[normtSTA;normtsta]; %take the next normtsta to next row, so each row represent each pixel through 36, 35,...,1 frames
end

%for unselected/broken channels
if MAX ==0
    normtSTA=ones(checker_n^2,fps);
end

%% Find max/min lum pixel
MAXsite=[];  MINsite=[];
[rows,cols]=find(normtSTA==max(max(normtSTA)));   %find the max timing and the pixel
MAXsite=[rows,cols];

[rows2,cols2]=find(normtSTA==min(min(normtSTA))); % rows is the pixel number has min normtSTA. cols is the frame has min normtSTA.
MINsite=[rows2,cols2];

%% plot RF of 16 frames
% figure;
% set(gca, 'Position', [10 250 630 630]);
% for i=1:checker_n
%    sSTA=reshape(STA_frame2(i,:),checker_n,checker_n); 
%    subplot(4,4,i);
% 
%    imagesc(sSTA);
%    set(gca,'xtick',[]);
%    set(gca,'ytick',[]);
% % Iblur=imgaussfilt(sSTA,1);
% %  imagesc(Iblur)
%    
%    axis square;
%    colormap(gray);
%    title(num2str(channel))
% end

%% plot the frame you want
frame_chosed=frame_list(cfframe);
cfframe=cfframe+1;
Peak1sSTA=reshape(STA_frame2(frame_chosed,:),checker_n,checker_n,[]); %find the 256 pixels

Iblur=imgaussfilt(Peak1sSTA,1);
P=flipud(rot90(Iblur,3)); %make it to fit Tina's coordination
figure;
imagesc(P);
pbaspect([1 1 1]) ;%make it square
 colormap(gray);
axis off;
end


%% Plot tSTA
load('D:\Yiko\Files for Thesis\11232017\RF\all_merge_11282017_ch50')
for channel=[50]
array_1ch=[];  %1 channel's STA checkerboard frame images
array_1ch=STA_frame(channel,:); 

%determine absolute max value from the averaged checkerboard images
%for the normalization used below
MAX=[];
if max(max(cell2mat(array_1ch)))>-min(min(cell2mat(array_1ch)))
    MAX=max(max(cell2mat(array_1ch)));
else
    MAX=-min(min(cell2mat(array_1ch)));
end

% change back to nthu mseq data form to draw using their codes
STA_frame2=[];
for g=1:length(array_1ch)
        STA_frame2(g,1:checker_n^2)=reshape(array_1ch{g},[1,checker_n^2]);
end
STA_frame2=STA_frame2./MAX;    %divided by the absolute max of all time and of all pixel

normtSTA=[];
for i=1:checker_n^2
        % get the 20 frames before each spike
        normtsta=flipud(STA_frame2(1:fps,i))';   %flipud: flip up to down   %to show temporal sta (x=36: 1st frame, x=1: 36th frame)
        normtSTA=[normtSTA;normtsta]; %take the next normtsta to next row, so each row represent each pixel through 36, 35,...,1 frames
end

%for unselected/broken channels
if MAX ==0
    normtSTA=ones(checker_n^2,fps);
end

%% Find max/min lum pixel
MAXsite=[];  MINsite=[];
[rows,cols]=find(normtSTA==max(max(normtSTA)));   %find the max timing and the pixel
MAXsite=[rows,cols];

[rows2,cols2]=find(normtSTA==min(min(normtSTA))); % rows is the pixel number has min normtSTA. cols is the frame has min normtSTA.
MINsite=[rows2,cols2];

%% Temparol STA for one pixel of one channel
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
rows=rows2(1); %for several max case, just choose the first one
% plot(smooth(normtSTA(rows,:))); %smoothed
plot(normtSTA(rows,:),'linewidth',2);
xlim([1 fps]);
ylim([-0.8 0.8]);
set(gca,'fontsize',15); 
xlabel('Time(sec)')
ylabel('STA')
set(gca,'xtick',[3,6,9,12,15,18]);
set(gca,'ytick',[-1 0 1]);
set(gca,'XTickLabel', {'-0.85','-0.7','-0.55','-0.4','-0.25','-0.1'} )
% set(gca, 'Xdir', 'reverse')
end








%% Making merge workspace: Single RF
% Calculate start timing
clear all
load('D:\Yiko\Experiment\12132017_diff_areas\retina2\pos1\pos1_ch12_01')

% figure;plot(a_data(3,:))

diode_timing=225800;
DataTime=5*60;

stop_light_t=diode_timing/20000;  %sec
checker_start_t=stop_light_t+10;
checker_end_t=checker_start_t+DataTime;

%%useend diode time to calculate the timing
% diode_timing_end=6489000;
% error=(checker_end_t*20000-diode_timing_end)/20000; %sec
% error
% checker_end_t=diode_timing_end/20000; 
% DataTime=5*60;
% checker_start_t=checker_end_t-DataTime;



% read in sorted excel file and make Spikes matrix
% A = xlsread('D:\Yiko\Files for Thesis\11232017\1123_200filter_SD5_ch12_02_onlysynchannels_axon.xls');
% A = xlsread('D:\Yiko\Files for Thesis\12312017\RF\1231_200filter_SD5_ch12_01_nsort.xls');

A = xlsread('D:\Yiko\Files for Thesis\12132017\retina2 pos1\RF\1213_200filter_SD5_ch12_01_onlysoma.xls');
Spikes=cell(1,60);
uint=1; %pick unit x
for h=1:60
label=find(A(:,1)==h & A(:,2)==uint ); %pick channel  & pick unit 1
Spikes(h)=mat2cell(A(label,3)',1);  %Timestamp for that channel
end

sorted_Spikes=Spikes;
Spikes=cell(1,60);
                    
% for offline sorter ch_77 61 34
rNumber=[59,42,46,7,10,52,...
                        55,38,21,25,28,31,14,56,...
                        13,34,17,3,49,35,18,39,...
                        9,48,30,51,60,22,4,43,...
                        6,27,45,24,15,53,11,32,...
                        2,41,58,12,26,40,57,36,...
                        20,37,54,50,47,44,1,19,...
                        16,33,29,8,5,23];
                    
for h=1:60
    Spikes{h}=sorted_Spikes{rNumber(h)};
end

% Cut Spikes (values in Spikes are the timing of spikes)
%have set the starting time = 0 
yk_spikes=[];
 for j = 1:length(Spikes)    %running through each channel
        ss = Spikes{j};
                ss(ss<checker_start_t) = [];  %delete the spikes before TimeStamps(1)
                ss(ss>checker_end_t)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-checker_start_t;
        end
        yk_spikes{j} = ss;
 end
 
% Binning
fps=20; %same as movie fps => so I use weighting of firing rate to each movie frame

BinningInterval = 1/fps;  %s
BinningTime = [ 0 : BinningInterval : DataTime];
% BinningTime = [ checker_start_t : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 

% MEA electrode coordination
start_x=1.73;
distanc=1.69;
posl = [start_x:distanc:start_x+7*distanc];
%electrode's coordination
mea_pos=cell(8,8);
for ui=1:8
p2=posl(ui); %y coordinate
        for io=1:8
            mea_pos{ui,io}(1)=[posl(io)]; % x coordinate
            mea_pos{ui,io}(2)=p2;
        end
end
% mea number
mea_num=cell(8,8);
kkbob=[0 ,1, 2, 3, 4, 5 ,6, 0,...
                  7 ,8 ,9, 10 ,11 ,12, 13 ,14,...
                  15, 16, 17 ,18, 19 ,20, 21, 22,...
                  23, 24 ,25, 26, 27, 28, 29, 30,...
                  31,32, 33 ,34 ,35 ,36, 37 ,38,...
                  39, 40, 41, 42 ,43 ,44, 45 ,46,...
                  47 ,48 ,49, 50, 51, 52 ,53, 54,...
                  0, 55, 56 ,57, 58, 59 ,60, 0];
for kkb=1:64
    mea_num{kkb}=kkbob(kkb);
end

%test mea electrode number position
for channel=1:8
figure;
Numb=find(cell2mat(mea_num)==channel);
ror=cell2mat(mea_pos(Numb));
p1=ror(2);
p2=ror(1);
plot(p2,p1,'.','Color','r','MarkerSize',13)
title(num2str(channel))
end

% Parameters for  STA
load('D:\Yiko\Map RF\Oct_mapping\randomSeq_15min_fps20_ch12_fixLum_addi1'); %load stimulus mat file
% load('\\192.168.0.100\Experiment\Retina\YiKo\Map RF\Oct_mapping\randomSeq_5min_fps20_ch12_fixLum_addi1'); %load stimulus mat file
fps=20;
T=5*60;

start_pt=1;
% start_pt=6001; %2nd part: 2nd 5 minutes
% start_pt=12001; %3rd part

FrameS= T*fps; 
before_frames=1/(1/fps);  %find checkers 1 sec before a spike
CheckerBoard=thisSeq;  %put the checkers matrix here!!!
CheckerBoard(find(CheckerBoard==0))=-1;  %let 0 in CheckerBoard be -1 => increase difference
%corresponding subpart of checker frames
LCD_check=CheckerBoard(start_pt : start_pt+FrameS-1, :, :);  %extract the corresponding stimulus
STA_frame=cell(60,before_frames);  %each row is for one channel


% NTHU Method Plot : singel channel plot
check_n=12;
addi=1;  %additional checker on one side
ad_check_n=check_n+addi*2; 
checker_n=ad_check_n;

STA_frame=[]; 
for channel=1:60
find_spikes=[]; 
find_spikes=find(BinningSpike(channel,:) > 0 );  %find spikes location

for frame=1:before_frames
temp=zeros(size(LCD_check,2),size(LCD_check,2));
sum_spikes=0;
        for h=find_spikes     %(1):find_spikes(end)
                if h-frame <=0
                         h=h;
                else
                    %weighting with firing rate
                    temp=temp+BinningSpike(channel,h)*squeeze(LCD_check(h-frame,:,:));   %use squeeze to make "1 x m x n" into "m x n" array
                    
                    %No weighting
%                temp=temp+squeeze(LCD_check(h-frame,:,:));  
                end
         sum_spikes=sum_spikes+BinningSpike(channel,h);
        end

%weighting with firing rate
STA_frame{channel,frame}=temp./sum_spikes;

%No weighting
% STA_frame{channel,frame}=temp;

end
end

%save workspace and file name:all_merge_1213Re2pos1_onlysortsoma



%% Plot corresponding electrode positions with 14x14 checkerboard
load('D:\Yiko\Images for thesis_NV\Peak1sSTA for plot electrode position with checkerboard region')

P=flipud(rot90(Peak1sSTA,3)); %make it to fit Tina's coordination
P(8,5:6)=0.0684;
P(9,5:6)=0.1302;

figure;
imagesc(P);
pbaspect([1 1 1]) ;%make it square
 colormap(gray);
caxis([-1.0000    0.3024]) %rescale color range
axis off;

start_x=1.73;
distanc=1.69;
posl = [start_x:distanc:start_x+7*distanc];
%electrode's coordination
mea_pos=cell(8,8);
for ui=1:8
p2=posl(ui); %y coordinate
        for io=1:8
            mea_pos{ui,io}(1)=[posl(io)]; % x coordinate
            mea_pos{ui,io}(2)=p2;
        end
end
% mea number
mea_num=cell(8,8);
kkbob=[0 ,1, 2, 3, 4, 5 ,6, 0,...
                  7 ,8 ,9, 10 ,11 ,12, 13 ,14,...
                  15, 16, 17 ,18, 19 ,20, 21, 22,...
                  23, 24 ,25, 26, 27, 28, 29, 30,...
                  31,32, 33 ,34 ,35 ,36, 37 ,38,...
                  39, 40, 41, 42 ,43 ,44, 45 ,46,...
                  47 ,48 ,49, 50, 51, 52 ,53, 54,...
                  0, 55, 56 ,57, 58, 59 ,60, 0];
for kkb=1:64
    mea_num{kkb}=kkbob(kkb);
end

hold on;
for channel=1:60
Numb=find(cell2mat(mea_num)==channel);
ror=cell2mat(mea_pos(Numb));
p1=ror(2);
p2=ror(1);
plot(p2,p1,'.','Color','r','MarkerSize',13);
end









%% Making merge workspace: Single unit t-STA
% Calculate start timing
clear all
load('D:\Yiko\Experiment\11282017_RF\ch12_01')

% figure;plot(a_data(3,:))

diode_timing=306546;
DataTime=5*60;

stop_light_t=diode_timing/20000;  %sec
checker_start_t=stop_light_t+10;
checker_end_t=checker_start_t+DataTime;

%%useend diode time to calculate the timing
% diode_timing_end=6489000;
% error=(checker_end_t*20000-diode_timing_end)/20000; %sec
% error
% checker_end_t=diode_timing_end/20000; 
% DataTime=5*60;
% checker_start_t=checker_end_t-DataTime;



% read in sorted excel file and make Spikes matrix
A = xlsread('D:\Yiko\Files for Thesis\11282017\1128_200filter_SD5_ch12_01_channel50.xls');
Spikes=cell(1,60);
uint=1; %pick unit x
for h=1:60
label=find(A(:,1)==h & A(:,2)==uint ); %pick channel  & pick unit 1
Spikes(h)=mat2cell(A(label,3)',1);  %Timestamp for that channel
end

sorted_Spikes=Spikes;
Spikes=cell(1,60);
                    
% for offline sorter ch_77 61 34
rNumber=[59,42,46,7,10,52,...
                        55,38,21,25,28,31,14,56,...
                        13,34,17,3,49,35,18,39,...
                        9,48,30,51,60,22,4,43,...
                        6,27,45,24,15,53,11,32,...
                        2,41,58,12,26,40,57,36,...
                        20,37,54,50,47,44,1,19,...
                        16,33,29,8,5,23];
                    
for h=1:60
    Spikes{h}=sorted_Spikes{rNumber(h)};
end

% Cut Spikes (values in Spikes are the timing of spikes)
%have set the starting time = 0 
yk_spikes=[];
 for j = 1:length(Spikes)    %running through each channel
        ss = Spikes{j};
                ss(ss<checker_start_t) = [];  %delete the spikes before TimeStamps(1)
                ss(ss>checker_end_t)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-checker_start_t;
        end
        yk_spikes{j} = ss;
 end
 
% Binning
fps=20; %same as movie fps => so I use weighting of firing rate to each movie frame

BinningInterval = 1/fps;  %s
BinningTime = [ 0 : BinningInterval : DataTime];
% BinningTime = [ checker_start_t : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 

% MEA electrode coordination
start_x=1.73;
distanc=1.69;
posl = [start_x:distanc:start_x+7*distanc];
%electrode's coordination
mea_pos=cell(8,8);
for ui=1:8
p2=posl(ui); %y coordinate
        for io=1:8
            mea_pos{ui,io}(1)=[posl(io)]; % x coordinate
            mea_pos{ui,io}(2)=p2;
        end
end
% mea number
mea_num=cell(8,8);
kkbob=[0 ,1, 2, 3, 4, 5 ,6, 0,...
                  7 ,8 ,9, 10 ,11 ,12, 13 ,14,...
                  15, 16, 17 ,18, 19 ,20, 21, 22,...
                  23, 24 ,25, 26, 27, 28, 29, 30,...
                  31,32, 33 ,34 ,35 ,36, 37 ,38,...
                  39, 40, 41, 42 ,43 ,44, 45 ,46,...
                  47 ,48 ,49, 50, 51, 52 ,53, 54,...
                  0, 55, 56 ,57, 58, 59 ,60, 0];
for kkb=1:64
    mea_num{kkb}=kkbob(kkb);
end


% Parameters for  STA
load('D:\Yiko\Map RF\Oct_mapping\randomSeq_15min_fps20_ch12_fixLum_addi1'); %load stimulus mat file
% load('\\192.168.0.100\Experiment\Retina\YiKo\Map RF\Oct_mapping\randomSeq_5min_fps20_ch12_fixLum_addi1'); %load stimulus mat file
fps=20;
T=5*60;

start_pt=1;
% start_pt=6001; %2nd part: 2nd 5 minutes
% start_pt=12001; %3rd part

FrameS= T*fps; 
before_frames=1/(1/fps);  %find checkers 1 sec before a spike
CheckerBoard=thisSeq;  %put the checkers matrix here!!!
CheckerBoard(find(CheckerBoard==0))=-1;  %let 0 in CheckerBoard be -1 => increase difference
%corresponding subpart of checker frames
LCD_check=CheckerBoard(start_pt : start_pt+FrameS-1, :, :);  %extract the corresponding stimulus
STA_frame=cell(60,before_frames);  %each row is for one channel


% NTHU Method Plot : singel channel plot
check_n=12;
addi=1;  %additional checker on one side
ad_check_n=check_n+addi*2; 
checker_n=ad_check_n;

STA_frame=[]; 
for channel=1:60
find_spikes=[]; 
find_spikes=find(BinningSpike(channel,:) > 0 );  %find spikes location

for frame=1:before_frames
temp=zeros(size(LCD_check,2),size(LCD_check,2));
sum_spikes=0;
        for h=find_spikes     %(1):find_spikes(end)
                if h-frame <=0
                         h=h;
                else
                    %weighting with firing rate
                    temp=temp+BinningSpike(channel,h)*squeeze(LCD_check(h-frame,:,:));   %use squeeze to make "1 x m x n" into "m x n" array
                    
                    %No weighting
%                temp=temp+squeeze(LCD_check(h-frame,:,:));  
                end
         sum_spikes=sum_spikes+BinningSpike(channel,h);
        end

%weighting with firing rate
STA_frame{channel,frame}=temp./sum_spikes;

%No weighting
% STA_frame{channel,frame}=temp;

end
end

%save workspace and file name:all_merge_11282017_ch50





 %% LCD screen orientation
 rr=[63,62,61,60,59,58,...
          56,55,54,53,52,51,50,49,...
          48,47,46,45,44,43,42,41,...
          40,39,38,37,36,35,34,33,...
          32,31,30,29,28,27,26,25,...
          24,23,22,21,20,19,18,17,...
          16,15,14,13,12,11,10,9,...
          7,6,5,4,3,2];
      
figure;
for channelnumber=1:60
h=subplot(8,8,rr(channelnumber)); hold on;
% tempfig=max_frame{1,channelnumber};
tempfig=min_frame{1,channelnumber};
set(gca,'YDir','reverse') %using subplot would reverse the image upside down, so turn it back

imagesc(tempfig);
colormap(gray);
axis square;
axis off
title(channelnumber)
end

%check oreintation!
figure;
channelnumber=17;
tempfig=min_frame{1,channelnumber};
imagesc(tempfig);
colormap(gray);
axis square;
axis off
title(channelnumber)



%% Gaussian filter
Iblur=imgaussfilt(Peak1sSTA,1);
figure; imagesc(Iblur)
colormap(jet)
title('Gaussian filtered image, \sigma = 1')


%% Ref code for manipulating

for channel=[19 ]
array_1ch=[];  %1 channel's STA checkerboard frame images
array_1ch=STA_frame(channel,:); 

%determine absolute max value from the averaged checkerboard images
%for the normalization used below
MAX=[];
if max(max(cell2mat(array_1ch)))>-min(min(cell2mat(array_1ch)))
    MAX=max(max(cell2mat(array_1ch)));
else
    MAX=-min(min(cell2mat(array_1ch)));
end

% change back to nthu mseq data form to draw using their codes
STA_frame2=[];
for g=1:length(array_1ch)
        STA_frame2(g,1:checker_n^2)=reshape(array_1ch{g},[1,checker_n^2]);
end
STA_frame2=STA_frame2./MAX;    %divided by the absolute max of all time and of all pixel

normtSTA=[];
for i=1:checker_n^2
        % get the 20 frames before each spike
        normtsta=flipud(STA_frame2(1:fps,i))';   %flipud: flip up to down   %to show temporal sta (x=36: 1st frame, x=1: 36th frame)
        normtSTA=[normtSTA;normtsta]; %take the next normtsta to next row, so each row represent each pixel through 36, 35,...,1 frames
end

%for unselected/broken channels
if MAX ==0
    normtSTA=ones(checker_n^2,fps);
end

%% Find max/min lum pixel
MAXsite=[];  MINsite=[];
[rows,cols]=find(normtSTA==max(max(normtSTA)));   %find the max timing and the pixel
MAXsite=[rows,cols];

[rows2,cols2]=find(normtSTA==min(min(normtSTA))); % rows is the pixel number has min normtSTA. cols is the frame has min normtSTA.
MINsite=[rows2,cols2];

%% Temparol STA for one pixel of one channel
% figure;
% set(gca,'Position',[0.17 0.2  0.75 0.7])
% rows=rows(1); %for several max case, just choose the first one
% % plot(smooth(normtSTA(rows,:))); %smoothed
% plot(normtSTA(rows,:),'linewidth',2);
% xlim([1 fps]);
% ylim([-1 1]);
% set(gca,'fontsize',20); 
% xlabel('Time lag(ms)')
% ylabel('STA')
% set(gca,'xtick',[3,11,19]);
% set(gca,'ytick',[-1 0 1]);
% set(gca,'XTickLabel', {'1000','500','50'} )
% set(gca, 'Xdir', 'reverse')

%% plot the max luminance RF
% cols=cols(1);  %for several max case, just choose the first one
% Peak1sSTA=reshape(STA_frame2((fps-cols+1),:),checker_n,checker_n,[]); %find the 256 pixels
% figure;
% imagesc(Peak1sSTA);
% set(gca,'Position',[0.17 0.2  0.75 0.7])
% axis square;
% colormap(gray);
% colorbar;
% title(channel);
% max_frame{1,channel}=Peak1sSTA;

%% plot the min luminance RF
% cols=cols2(1);  %for several max case, just choose the first one
% Peak1sSTA=reshape(STA_frame2((fps-cols+1),:),checker_n,checker_n,[]); %find the 256 pixels
% figure;
% set(gca,'Position',[0.17 0.2  0.75 0.7])
% imagesc(Peak1sSTA);
% axis square;
% colormap(gray);
% cbr=colorbar;
% cbr.Ticks = [-0.6 -0.2 0.2];
% set(cbr, 'XTickLabel',{-0.6, -0.2,0.2});
% set(gca,'fontsize',15); 
% set(gca,'xtick',[]);
% set(gca,'ytick',[]);
% 
% min_frame{1,channel}=Peak1sSTA;
%gaussian filter
% Iblur=imgaussfilt(Peak1sSTA,1);
% figure; imagesc(Iblur)
% set(gca,'Position',[0.17 0.2  0.75 0.7])
% axis square;
% colormap(gray);
% cbr=colorbar;
% cbr.Ticks = [-0.3 -0.1 0.1];
% set(cbr, 'XTickLabel',{-0.3 -0.1 0.1});
% set(gca,'fontsize',15); 
% set(gca,'xtick',[]);
% set(gca,'ytick',[]);

%% plot RF of 16 frames
% figure;
% set(h2, 'Position', [10 250 630 630]);
% for i=1:checker_n
%    sSTA=reshape(STA_frame2(i,:),checker_n,checker_n); 
%    subplot(4,4,i);
% 
%    imagesc(sSTA);
%    set(gca,'xtick',[]);
%    set(gca,'ytick',[]);
% % Iblur=imgaussfilt(sSTA,1);
% %  imagesc(Iblur)
%    
%    axis square;
%    colormap(gray);
%    title(channel)
% end


%% plot the frame you want
frame_chosed=3;
Peak1sSTA=reshape(STA_frame2(frame_chosed,:),checker_n,checker_n,[]); %find the 256 pixels
% imagesc(Peak1sSTA);
P=flipud(rot90(Peak1sSTA,3)); %make it to fit Tina's coordination
figure;
imagesc(P);
pbaspect([1 1 1]) ;%make it square
% axis square;
axis off;
colorbar
% 
% % plot mea electrode position
hold on;
Numb=find(cell2mat(mea_num)==channel);
ror=cell2mat(mea_pos(Numb));
p1=ror(2);
p2=ror(1);
plot(p2,p1,'.','Color','r','MarkerSize',13)
% 
% %extract the final image
% F = getframe(gcf);
% [X,Map] = frame2im(F);
% figure; imagesc(X)
% XNi=X(32:374,119:462,:);
% figure; imagesc(XNi)
% pbaspect([1 1 1])
% axis off;

% colormap(gray);
% colormap(jet);
% cbr=colorbar;
% cbr.Ticks = [-0.3 -0.1 0.1];

%% Gaussion filter
% Iblur=imgaussfilt(Peak1sSTA,1);
% figure; imagesc(Iblur)
% colormap(jet)

end

