%PTB timing image plotting 
load('D:\Yiko\Files for Thesis\PTB timing problem data\PTB_bar2state\workspace_all_bar2state')
figure;
plot(image_sumlum(1.068*10^4:1.077*10^4))
xlim([0 90]);
ylim([-2*10^3 ,2*10^5])

% [20:20:90]/120  %ccd fps=120Hz

set(gca, 'XTick', [20:20:90])
set(gca,'XTickLabel', {'0.17','0.33','0.5','0.67'} )
set(gca, 'YTick', [0 10 20]*10^4)
set(gca,'YTickLabel', {'0','0.5','1'} )
ylabel('Rescaled luminance')
xlabel('Time (sec)')

set(gca,'Position',[0.11 0.2  0.85 0.7])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');