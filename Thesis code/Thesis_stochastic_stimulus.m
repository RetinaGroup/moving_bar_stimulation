%% generate HMM stimulus for 2 G
clear all
cd('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs')
fname=['D:\Yiko\Files for Thesis\04102018\New folder']; %for saving
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 
latency_list=[];

T1=100;
T2=2000;

%G=4.3
z =3;
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

figure;
time=[T1*BinningInterval:BinningInterval:T2*BinningInterval];
plot(time,bin_pos(T1:T2))

set(gca,'Position',[0.17 0.2  0.75 0.35])
xlabel( 'Time(sec)');ylabel('Position');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([T1*BinningInterval T2*BinningInterval])

%G=9
z =7;
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

figure;
time=[T1*BinningInterval:BinningInterval:T2*BinningInterval];
plot(time,bin_pos(T1:T2),'Color',[1 .5 0])

set(gca,'Position',[0.17 0.2  0.75 0.35])
xlabel( 'Time(sec)');ylabel('Position');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([T1*BinningInterval T2*BinningInterval])




%% auto MI for 2 G
%load G4.3 and G9
clear all
cd('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs')
fname=['D:\Yiko\Files for Thesis\04102018\New folder']; %for saving
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 
latency_list=[];
figure;
for z =[3 7]

file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name


%% put your stimulus here!!!!!!!!!!
TheStimuli=[];
TheStimuli2=[];
% TheStimuli=bin_pos(20:end);
% TheStimuli2=bin_pos(10:end-10);
% 
% TheStimuli=bin_pos(10:end-10);
% TheStimuli2=bin_pos(20:end);

TheStimuli=bin_pos;
TheStimuli2=bin_pos;

% figure;
% plot(TheStimuli);
% hold on;
% plot(TheStimuli2)



%% Remember to replace the below "newXarray" to your actual stimuli array!!!
%% Binning

bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
% BinningTime =diode_BT;


%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
% StimuSN2=8; %number of stimulus states
% nX2=sort(TheStimuli2);
% abin=length(nX2)/StimuSN2;
% intervals2=[nX2(1:abin:end) inf]; % inf: the last term: for all rested values
% temp=0; isi3=[];
% for jj=1:length(TheStimuli2)
%     temp=temp+1;
%     isi3(temp) = find(TheStimuli2(jj)<=intervals2,1);
% end 

%% Predictive information

backward=ceil(2500/bin); forward=ceil(2500/bin);
% for channelnumber=34
% n = channelnumber;
% Neurons = BinningSpike(n,:);  %for single channel
%Neurons = sum(BinningSpike(1:60,:));  %calculate population MI
% Neurons=isi2;
Neurons =TheStimuli2;
% Neurons =isi3;

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
%         norm = BinningInterval; %bits/second
norm=1;

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
%         norm = BinningInterval; %bits/second
norm=1;

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
% corr=[corrp corrf];
% corr=corr/max(corr);
% mean(information);
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
%         norm = BinningInterval;
norm=1;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
%         norm = BinningInterval;
norm=1;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
    


time=[-backward*bin:bin:forward*bin];

plot(time,information-information_shuffle,'Linewidth',3); hold on; 

end

set(gca,'Position',[0.17 0.2  0.75 0.7])
xlabel( '\delta t (ms)');ylabel('MI (bits)');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([-2000 2000])

lgd = legend('\tau =0.9 sec', '\tau =0.38 sec');
lgd.FontSize = 15;
lgd.FontName =  'Times New Roman';
legend boxoff  



%% plot HMM and OU stimulus with same corr time
clear all
T1=100;
T2=2000;
BinningInterval=0.0167;
%HMM G4
load('D:\Yiko\HMM video frame\workspace_HMM 1946 G4 5min Q85')
figure;
time=[T1*BinningInterval:BinningInterval:T2*BinningInterval];
plot(time,newXarray(T1:T2))

set(gca,'Position',[0.17 0.2  0.75 0.35])
xlabel( 'Time(sec)');ylabel('Position');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([T1*BinningInterval T2*BinningInterval])


%OU G4
load('D:\Yiko\OU video frame\workspace_OU 1946 G4 10min Q85')
figure;
time=[T1*BinningInterval:BinningInterval:T2*BinningInterval];
plot(time,new_x(T1:T2))

set(gca,'Position',[0.17 0.2  0.75 0.35])
xlabel( 'Time(sec)');ylabel('Position');
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([T1*BinningInterval T2*BinningInterval])



