%% Bar Trajectpry with raster plot 01
%load the sorted merge file first
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G043')

T1=200; %sec %choose time range for plotting
T2=250;

% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',3); hold on;
set(gca,'Ydir','reverse')
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
%test the bar width plotting
% plot(bin_pos(1,frame1:frame2)+11,'b--','linewidth',0.5); hold on;
% plot(bin_pos(1,frame1:frame2)-11,'b--','linewidth',0.5);
% plot(new_y(1,frame1:frame2)+11,'b--','linewidth',0.5); hold on;
% plot(new_y(1,frame1:frame2)-11,'b--','linewidth',0.5);

% plot(newYarray)
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])


%Raster plot of selected channels
yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end


%for RL direction stimulation: 1st row is for channel number:7,15,23,31,39,47/
%2nd row is for number: 1,8,16,24,32,40,48,55 etc
rasterch=cell(1,36); %36 is to plot 36 rows
for ch=1:length(rasterch)
    rasterch{ch}=zeros(1,length(rasterch));
end
% selectch=[2 0 19 0 0 43 50 55];%choose specific channels
selectch=[23 40 56 0 51 5 53 0];%choose specific channels % remember to enter 0 if that row has no selected channel
cou=1;
for ch=[1 6 11 16 21 26 31 36] %the numbers are the location, NOT the selected channel number / dont need to change/ correspond to 36 rows setting
    if selectch(cou)>0
         rasterch{ch}=yk_spikes2{selectch(cou)};
    end
    cou=cou+1;
end

figure; %plot raster plot for selected channels
set(gca,'Position',[0.17 0.2  0.75 0.7])
plotSpikeRaster(rasterch,'PlotType','vertline');
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

% create axis plot 
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Position (mm)')
xlabel('Time (sec)')
xlim([0,50])
% set(gca,'ytick',[])
set(gca, 'YTick', [0 0.5 1])
set(gca,'YTickLabel', {'0','0.7','1.4'} )
set(gca, 'XTick', [0 25 50])
set(gca,'XTickLabel', {'200','225','250'} )
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');


%% Bar Trajectpry with raster plot 02
%load the sorted merge file first
clear all
load('D:\Yiko\Files for Thesis\04042018\sort2 pos2 HMM RL MI figure\Sort2 unit1 merge_HMM RL G05 5min Q85')

T1=50; %sec %choose time range for plotting
T2=150;

% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',3); hold on;
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
%test the bar width plotting
% plot(bin_pos(1,frame1:frame2)+11,'b--','linewidth',0.5); hold on;
% plot(bin_pos(1,frame1:frame2)-11,'b--','linewidth',0.5);
% plot(new_y(1,frame1:frame2)+11,'b--','linewidth',0.5); hold on;
% plot(new_y(1,frame1:frame2)-11,'b--','linewidth',0.5);

% plot(newYarray)
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])


%Raster plot of selected channels
yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end


%for RL direction stimulation: 1st row is for channel number:7,15,23,31,39,47/
%2nd row is for number: 1,8,16,24,32,40,48,55 etc
rasterch=cell(1,36); %36 is to plot 36 rows
for ch=1:length(rasterch)
    rasterch{ch}=zeros(1,length(rasterch));
end
% selectch=[2 0 19 0 0 43 50 55];%choose specific channels
selectch=[0 0 0 34 11 28 6 22];%choose specific channels % remember to enter 0 if that row has no selected channel
cou=1;
for ch=[1 6 11 16 21 26 31 36] %the numbers are the location, NOT the selected channel number / dont need to change/ correspond to 36 rows setting
    if selectch(cou)>0
         rasterch{ch}=yk_spikes2{selectch(cou)};
    end
    cou=cou+1;
end

figure; %plot raster plot for selected channels
set(gca,'Position',[0.17 0.2  0.75 0.7])
plotSpikeRaster(rasterch,'PlotType','vertline');
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

% create axis plot 
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Position')
xlabel('Time (sec)')
xlim([0,100])
set(gca,'ytick',[])
set(gca, 'XTick', [0 50 100])
set(gca,'XTickLabel', {'50','100','150'} )
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');



%% PSTH with trajectory : 3G
%G=043
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G043')

T1=200; %sec %choose time range for plotting
T2=300;

% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',1,'Color',[  1.0000    0.6667         0]); hold on;
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end

%use the trajectory plotted above
%Binning
        % BinningInterval = 1/60;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
        % BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningTime =diode_BT(frame1:frame2);
BinningTime=BinningTime-T1;
BinningSpike = zeros(60,length(BinningTime));
s=zeros(1,length(BinningSpike));
for i = [21 22 23 29 31 37 47 13]  %select neurons
    [n,~] = hist(yk_spikes2{i},BinningTime) ;
    BinningSpike(i,:) = n ;
    s=s+BinningSpike(i,:);
end 

 max(s)
 
figure;
plot(BinningTime,s/8/BinningInterval,'black');
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate (Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
% xlim([0,100])
% set(gca, 'XTick', [0 50 100])
% set(gca,'XTickLabel', {'160','210','260'} )
% set(gca, 'YTick', [ 10 30 50 70])
% uistack(gca,'top');


%G=025
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G025')

T1=200; %sec %choose time range for plotting
T2=300;

% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',1,'Color',[  1.0000    0.6667         0]); hold on;
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end

%use the trajectory plotted above
%Binning
        % BinningInterval = 1/60;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
        % BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningTime =diode_BT(frame1:frame2);
BinningTime=BinningTime-T1;
BinningSpike = zeros(60,length(BinningTime));
s=zeros(1,length(BinningSpike));
for i = [21 22 23 29 31 37 47 13]  %select neurons
    [n,~] = hist(yk_spikes2{i},BinningTime) ;
    BinningSpike(i,:) = n ;
    s=s+BinningSpike(i,:);
end 

 max(s)
 
figure;
plot(BinningTime,s/8/BinningInterval,'black');
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate (Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
% xlim([0,100])
% set(gca, 'XTick', [0 50 100])
% set(gca,'XTickLabel', {'160','210','260'} )
% set(gca, 'YTick', [ 10 30 50 70])
% uistack(gca,'top');



%G=20
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G20')

T1=200; %sec %choose time range for plotting
T2=300;

% bar trajectory
figure;
set(gca,'Position',[0.17 0.2  0.75 0.7])

frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
% plot(newYarray(1,frame1:frame2),'linewidth',3); hold on;
plot(bin_pos(1,frame1:frame2),'linewidth',1,'Color',[  1.0000    0.6667         0]); hold on;
xlim([0,frame2-frame1])
% ylim([min(newYarray), max(newYarray)])
ylim([min(bin_pos), max(bin_pos)])
set(gca,'xtick',[]) %clear axis number
set(gca,'ytick',[])
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');

yk_spikes2=[];
for j = 1:length(Spikes)    %running through each channel  % choose corresponding spikes in T range
ss = Spikes{j};
        ss(ss<T1) = []; 
        ss(ss>T2)=[];
        for i = 1:length(ss)
            ss(i) = ss(i)-T1;
        end
yk_spikes2{j} = ss;
end

%use the trajectory plotted above
%Binning
        % BinningInterval = 1/60;  %s %since bar fps=60Hz, I think divide it into 3 parts is enough
        % BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningTime =diode_BT(frame1:frame2);
BinningTime=BinningTime-T1;
BinningSpike = zeros(60,length(BinningTime));
s=zeros(1,length(BinningSpike));
for i = [21 22 23 29 31 37 47 13]  %select neurons
    [n,~] = hist(yk_spikes2{i},BinningTime) ;
    BinningSpike(i,:) = n ;
    s=s+BinningSpike(i,:);
end 

 max(s)
 
figure;
plot(BinningTime,s/8/BinningInterval,'black');
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate (Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
% xlim([0,100])
% set(gca, 'XTick', [0 50 100])
% set(gca,'XTickLabel', {'160','210','260'} )
% set(gca, 'YTick', [ 10 30 50 70])
% uistack(gca,'top');





%%
clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G20')
figure; hist(bin_pos,8)

BinningTime =diode_BT;
% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
channel=[5 11 19 29 33 43 48 58];
eachSpikes=[];
for i=1:length(channel)
eachSpikes(i)=sum(BinningSpike(channel(i),:));
end
figure; plot(eachSpikes,'o')
 








