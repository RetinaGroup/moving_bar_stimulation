%% OU RL
% namelist=cell(1,7);
% namelist(1)={'OU RL G1p55 5min'};
% namelist(2)={'OU RL G2p45 5min'};
% namelist(3)={'OU RL G3p2 5min'};
% namelist(4)={'OU RL G4 5min'};
% namelist(5)={'OU RL G5p7 5min'};
% namelist(6)={'OU RL G7p6 5min'};
% namelist(7)={'OU RL G10p5 5min'};
% namelist(8)={'OU RL G5p03 5min'};

G_list=[1.55 2.45 3.2 4 5.7 7.6 10.5 5.03];

for Gvalue=G_list
clearvars -except G_list Gvalue
    load('D:\Yiko\OU video frame\workspace_for_video');
%     name=char(namelist(hh));
    
%% OU RL
fps =60;  %freq of the screen flipping 
T=5*60; %second
dt=1/fps;
T=dt:dt:T;

G_OU = G_list(hh); % damping / only G will influence correlation time
D_OU = 2700000; %dynamical range
omega =G_OU/2.12;   % omega = G/(2w)=1.06; follow Bielak's overdamped dynamics/ 2015PNAS
name
G_OU
%R-L
init=channel_map{68}; %L
final=channel_map{69}; %R

x = zeros(1,length(T));
x(1,1)=0; % since the mean value of damped eq is zero
for uu = 1:length(T)-1
      x(uu+1) = (1-dt*G_OU/(2.12)^2)*x(uu)+sqrt(dt*D_OU)*randn;
end
%%% Normalize to proper moving range
nrx=(final(1)-init(1))/(max(x)-min(x));
x2=x*nrx;
mdist=abs(min(x2)-init(1)); %rearrange the boundary values
    if min(x2)>init(1)
     mdist=-mdist;
    end
x3=x2+mdist;
new_x=round(x3); 
Y =round((init(2)+final(2))/2); 
% figure; autocorr(new_x,500)

clean_LED=clean_LED./255; %make it to 0-1 range for double (due to imwrite format)
clean_LED(clean_LED>1)=1;

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

%rearrange dotPositionMatrix format
dotPosition_x=[];   dotPosition_y=[];
dotPosition_x=dotPositionMatrix(1,1:8:end);
dotPosition_y=dotPositionMatrix(2,1:8);

number_repeat=repmat([1:12000],1,3);

%number file
cd('D:\Yiko\Timing stuff\numberFlip')
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['OU RL G',num2str(G_OU) ,' 5min Q85'];
name
% ori_dir =['D:\Yiko\OU video frame\',name];   

%video setting
Time=T; %sec
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\HMM video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);
%start part: dark adaptation
for mm=1:60
img=0*ones(1024,1280);   
writeVideo(writerObj,  img);
end


for kk =1:length(T)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%OU bar trajectory
X=new_x(kk);
if X-bar_wid <= dotPosition_x(1)
    ypart=(Y+bar_le-(Y-bar_le))/14;
    portion01=round(ypart*5);
    portion02=round(ypart*11);
    portion03=round(ypart*13);
%part1
    py1=Y-bar_le;
    py2=Y-bar_le+portion01;
    px1=X-bar_wid-1;
    px2= X+bar_wid;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_le+portion01+1;
    py4=Y-bar_le+portion02;
    px3=X-bar_wid;
    px4= X+bar_wid;
    a(py3:py4 ,  px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_le+portion02+1;
    py6=Y-bar_le+portion03;
    px5=X-bar_wid-1 ;
    px6= X+bar_wid;
    a(py5:py6 ,  px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_le+portion03+1;
    py8=Y+bar_le;
    px7=X-bar_wid-1;
    px8= X+bar_wid;
    ptmiddle=round((dotPosition_x(2)+dotPosition_x(1))/2);
    a(py7:py8 , px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm));
        if px7 < dotPosition_x(1)
                 a(py8:py8+2 ,  px7:dotPosition_x(1))=clean_LED(py8-round(lefty_bm):py8+2-round(lefty_bm),  px7-round(leftx_bm): dotPosition_x(1)-round(leftx_bm));
        end
        
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
a(Y-bar_le:Y+bar_le,X-bar_wid:X+bar_wid)=barImage;
end

%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;

%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '\', num2str(kk,'%06i'),'.jpeg']);
%imwrite conversion would change data value and class/ Indexed images are converted to RGB before writing out JPEG files

img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part video
for mm=1:10
            img=0*ones(1024,1280);
            img(500-35:500+35,1230:1280)=0.2; %gray 
            writeVideo(writerObj,  img);
end
close(writerObj);


ori_dir02 =['D:\Yiko\OU video frame\workspace_',name,'.mat'];   
save(ori_dir02)

end




%% OU UD

% namelist=cell(1,7);
% namelist(1)={'OU UD G1p55 5min'};
% namelist(2)={'OU UD G2p45 5min'};
% namelist(3)={'OU UD G3p2 5min'};
% namelist(4)={'OU UD G4 5min'};
% namelist(5)={'OU UD G5p7 5min'};
% namelist(6)={'OU UD G7p6 5min'};
% namelist(7)={'OU UD G10p5 5min'};

G_list=[1.55 2.45 3.2 4 5.7 7.6 10.5];

for Gvalue=G_list
clearvars -except G_list Gvalue
    load('D:\Yiko\OU video frame\workspace_for_video');
%     name=char(namelist(hh));

    
fps =60;  %freq of the screen flipping 
T=5*60; %second
dt=1/fps;
T=dt:dt:T;

G_OU = G_list(hh); % damping / only G will influence correlation time
D_OU = 2700000; %dynamical range
omega =G_OU/2.12;   % omega = G/(2w)=1.06; follow Bielak's overdamped dynamics/ 2015PNAS
name
G_OU
%U-D
init=channel_map{66}; %U
final=channel_map{67}; %D

y = zeros(1,length(T));
y(1,1)=0; % since the mean value of damped eq is zero
for uu = 1:length(T)-1
      y(uu+1) = (1-dt*G_OU/(2.12)^2)*y(uu)+sqrt(dt*D_OU)*randn;
end
%%% Normalize to proper moving range
nry=(final(2)-init(2))/(max(y)-min(y));
y2=y*nry;
mdist=abs(min(y2)-init(2));
    if min(y2)>init(2)
     mdist=-mdist;
    end
y3=y2+mdist;
new_y=round(y3); 
X =round((init(1)+final(1))/2); 

clean_LED=clean_LED./255;%make it to 0-1 range for double (due to imwrite format)
clean_LED(clean_LED>1)=1;

mea_size=503;
mea_size_bm=541; %bigger mea size , from luminance calibrated region
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

bar_le=(mea_size-1)/2; %half of bar length / pixel number on LCD /total length = mea_size = 1919 um
bar_wid=11; %half of bar width / total length = 23 pixels = 65 um

%rearrange dotPositionMatrix format
dotPosition_x=[];   dotPosition_y=[];
dotPosition_x=dotPositionMatrix(1,1:8:end);
dotPosition_y=dotPositionMatrix(2,1:8);

number_repeat=repmat([1:12000],1,3);

%number file
cd('D:\Yiko\Timing stuff\numberFlip')
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['OU UD G',num2str(G_OU) ,' 5min Q85'];
name
% ori_dir =['D:\Yiko\OU video frame\',name];   

%video setting
Time=T; %sec
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\HMM video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);
%start part: dark adaptation
for kk=1:60
img=0*ones(1024,1280);   
writeVideo(writerObj,  img);
end


for kk =1:length(T)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size
%OU UD bar trajectory
Y=new_y(kk);

if Y+bar_wid >= dotPosition_y(end)
    xpart=(X+bar_le-(X-bar_le))/14;
    portion01=round(xpart);
    portion02=round(xpart*7);
    portion03=round(xpart*9);
    portion04=round(xpart*11);
    portion05=round(xpart*13);
%part1
    py1=Y-bar_wid;
    py2=Y+bar_wid+2;
    px1=X-bar_le;
    px2=X-bar_le+portion01;
    a(py1:py2, px1:px2)=clean_LED(py1-round(lefty_bm):py2-round(lefty_bm),  px1-round(leftx_bm):px2-round(leftx_bm)); 
%part2
    py3=Y-bar_wid;
    py4=Y+bar_wid+1;
    px3=X-bar_le+portion01+1;
    px4=X-bar_le+portion02;
    a(py3:py4, px3:px4 )=clean_LED(py3-round(lefty_bm):py4-round(lefty_bm),  px3-round(leftx_bm):px4-round(leftx_bm)); 
%part3
    py5=Y-bar_wid;
    py6=Y+bar_wid;
    px5=X-bar_le+portion02+1;
    px6=X-bar_le+portion03;
    a(py5:py6, px5:px6 )=clean_LED(py5-round(lefty_bm):py6-round(lefty_bm),  px5-round(leftx_bm):px6-round(leftx_bm)); 
%part4
    py7=Y-bar_wid;
    py8=Y+bar_wid+1;
    px7=X-bar_le+portion03+1;
    px8=X-bar_le+portion04;
    a(py7:py8, px7:px8 )=clean_LED(py7-round(lefty_bm):py8-round(lefty_bm),  px7-round(leftx_bm):px8-round(leftx_bm)); 
%part5
    py9=Y-bar_wid;
    py10=Y+bar_wid;
    px9=X-bar_le+portion04+1;
    px10=X-bar_le+portion05;
    a(py9:py10, px9:px10 )=clean_LED(py9-round(lefty_bm):py10-round(lefty_bm),  px9-round(leftx_bm):px10-round(leftx_bm)); 
%part6
    py11=Y-bar_wid;
    py12=Y+bar_wid+1;
    px11=X-bar_le+portion05+1;
    px12=X+bar_le;
    a(py11:py12, px11:px12 )=clean_LED(py11-round(lefty_bm):py12-round(lefty_bm), px11-round(leftx_bm):px12-round(leftx_bm)); 
   
else
barX=X-round(leftx_bm);
barY=round(Y)-round(lefty_bm);
barImage=clean_LED(barY-bar_wid: barY+bar_wid, barX-bar_le: barX+bar_le); 
a(Y-bar_wid:Y+bar_wid,X-bar_le:X+bar_le)=barImage;
end


%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;

%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end


% imwrite(a, [ori_dir, '\', num2str(kk,'%06i'),'.jpeg']);
%imwrite conversion would change data value and class/ Indexed images are converted to RGB before writing out JPEG files
img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part video
for kk=1:10
            img=0*ones(1024,1280);
            img(500-35:500+35,1230:1280)=0.2; %gray 
            writeVideo(writerObj,  img);
end
close(writerObj);

ori_dir02 =['D:\Yiko\OU video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end

%% OU diagonal 19-46 (top-left/bottom right)
clear all
G_list=[1.55 2.45 3.2 4 5.7 7.6 10.5];
for Gvalue=G_list(1)
clearvars -except G_list Gvalue
load('D:\Yiko\HMM video frame\workspace_for_video')

fps =60;  %freq of the screen flipping 
T=5*60; %second
dt=1/fps;
T=dt:dt:T;

G_OU = Gvalue; % damping / only G will influence correlation time
D_OU = 2700000; %dynamical range
omega =G_OU/2.12;   % omega = G/(2w)=1.06; follow Bielak's overdamped dynamics/ 2015PNAS

%%Diagnal 19-46 > same distance as RL and UD
init=channel_map{10}+2;
final=channel_map{55}-2;

%make clean_LEDdiag : enlarge the matrix 
clean_LED=clean_LED./255; %make it to 0-1 range for double (due to imwrite format)
clean_LED(clean_LED>1)=1;

clean_LEDdiag=zeros(941,941);
clean_LEDdiag(1:200,1:200)=clean_LED(1,1);
clean_LEDdiag(201:201+length(clean_LED)-1 , 201:201+length(clean_LED)-1)=clean_LED;
for u=1:length(clean_LED)
    clean_LEDdiag(200+u,1:200)=clean_LED(u,1);%left
    clean_LEDdiag(1:200,200+u)=clean_LED(1,u); %top
    clean_LEDdiag(200+length(clean_LED)+1:400+length(clean_LED),200+u)=clean_LED(length(clean_LED),u); %bottom
    clean_LEDdiag(200+u, 200+length(clean_LED)+1:400+length(clean_LED))=clean_LED(u,length(clean_LED)); %right
end
clean_LEDdiag(201+length(clean_LED):end,1:200)=clean_LED(end,1);
clean_LEDdiag(1:200,201+length(clean_LED):end)=clean_LED(1,end);
clean_LEDdiag(201+length(clean_LED):end,201+length(clean_LED):end)=clean_LED(end,end);

%other parameters
mea_size=503;
mea_size_bm=length(clean_LEDdiag); %for clean_LEDdiag matrix
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

%generate location series
y = zeros(1,length(T));   
x = zeros(1,length(T));
y(1,1)=0;  x(1,1)=0;
for uu = 1:length(T)-1 
      x(uu+1) = (1-dt*G_OU/(2.12)^2)*x(uu)+sqrt(dt*D_OU)*randn;
      y(uu+1) =x(uu+1); 
end
%%% Normalize to proper moving range
% d_1=same_distance;
d_1=((final(1)-init(1))^2+(final(2)-init(2))^2)^(1/2);
d_2=((max(y)-min(y))^2+(max(x)-min(x))^2)^(1/2);
nr=d_1/d_2;
x2=x*nr;  
y2=y*nr;
mdist_x=abs(min(x2)-init(1));
    if min(x2)>init(1)
     mdist_x=-mdist_x;
    end
x3=x2+mdist_x;
new_x=round(x3);
mdist_y=abs(min(y2)-init(2));
    if min(y2)>init(2)
     mdist_y=-mdist_y;
    end
y3=y2+mdist_y;
new_y=round(y3);


%number file
number_repeat=repmat([1:12000],1,3);
cd('D:\Yiko\Timing stuff\numberFlip')
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['OU 1946 G',num2str(G_OU) ,' 5min Q85'];
name
% ori_dir =['D:\Yiko\OU video frame\',name];   

%video setting
Time=T; %sec
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\OU video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);
%start part: dark adaptation
for kk=1:60
img=0*ones(1024,1280);   
writeVideo(writerObj,  img);
end


for kk =1:length(T)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%OU 1946 bar trajectory
X=new_x(kk);
Y=new_y(kk);

rightopY=Y-177+8;
edge01=[];
edge01(1)=X-177-8;
edge01(2)=Y+177-8;
qoo=Y+177-8-(rightopY-1);
ptemp01=[];
for g=1:qoo
ptemp01(1)=edge01(1)+(g-1);
ptemp01(2)=edge01(2)-(g-1);
a(ptemp01(2), ptemp01(1):ptemp01(1)+32)=clean_LEDdiag(ptemp01(2)-round(lefty_bm), ptemp01(1)-round(leftx_bm):ptemp01(1)-round(leftx_bm)+32);
end

for k=1:23-1
    a(edge01(2)+k, edge01(1)+k:edge01(1)+32-k)=clean_LEDdiag(edge01(2)+k-round(lefty_bm), edge01(1)+k-round(leftx_bm):edge01(1)+32-k-round(leftx_bm)); %bottom left corner
    a(ptemp01(2)-k, ptemp01(1)+k:ptemp01(1)+32-k)=clean_LEDdiag(ptemp01(2)-k-round(lefty_bm),ptemp01(1)+k-round(leftx_bm):ptemp01(1)+32-k-round(leftx_bm)); %top right corner
end

%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;
%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '\', num2str(kk,'%06i'),'.png']);

img=[];
img=a;
writeVideo(writerObj,  img);
end

%end part video
for kk=1:10
            img=0*ones(1024,1280);
            img(500-35:500+35,1230:1280)=0.2; %gray 
            writeVideo(writerObj,  img);
end
close(writerObj);

ori_dir02 =['D:\Yiko\OU video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end



%% OU 4322 diagonal
clear all
G_list=[1.55 2.45 3.2 4 5.7 7.6 10.5];
for Gvalue=G_list(1)
clearvars -except G_list Gvalue
load('D:\Yiko\OU video frame\workspace_for_video')

fps =60;  %freq of the screen flipping 
T=5*60; %second
dt=1/fps;
T=dt:dt:T;

G_OU =Gvalue; %Gvalue; % damping / only G will influence correlation time
D_OU = 2700000; %dynamical range
omega =G_OU/2.12;   % omega = G/(2w)=1.06; follow Bielak's overdamped dynamics/ 2015PNAS

%%Diagnal 4322 > same distance as RL and UD
init(1)=channel_map{50}(1)-2;
init(2)=channel_map{50}(2)+2;
final(1)=channel_map{15}(1)+2;
final(2)=channel_map{15}(2)-2;

%make clean_LEDdiag : enlarge the matrix 
clean_LED=clean_LED./255; %make it to 0-1 range for double (due to imwrite format)
clean_LED(clean_LED>1)=1;

clean_LEDdiag=zeros(941,941);
clean_LEDdiag(1:200,1:200)=clean_LED(1,1);
clean_LEDdiag(201:201+length(clean_LED)-1 , 201:201+length(clean_LED)-1)=clean_LED;
for u=1:length(clean_LED)
    clean_LEDdiag(200+u,1:200)=clean_LED(u,1);%left
    clean_LEDdiag(1:200,200+u)=clean_LED(1,u); %top
    clean_LEDdiag(200+length(clean_LED)+1:400+length(clean_LED),200+u)=clean_LED(length(clean_LED),u); %bottom
    clean_LEDdiag(200+u, 200+length(clean_LED)+1:400+length(clean_LED))=clean_LED(u,length(clean_LED)); %right
end
clean_LEDdiag(201+length(clean_LED):end,1:200)=clean_LED(end,1);
clean_LEDdiag(1:200,201+length(clean_LED):end)=clean_LED(1,end);
clean_LEDdiag(201+length(clean_LED):end,201+length(clean_LED):end)=clean_LED(end,end);

%other parameters
mea_size=503;
mea_size_bm=length(clean_LEDdiag); %for clean_LEDdiag matrix
meaCenter_x=630; 
meaCenter_y=573; 
leftx_bm=meaCenter_x-(mea_size_bm-1)/2; %the first x position of the bigger mea region(luminance calibrated region) on LED screen
lefty_bm=meaCenter_y-(mea_size_bm-1)/2;

%generate location series
y = zeros(1,length(T));   
x = zeros(1,length(T));
y(1,1)=0;  x(1,1)=0;
for uu = 1:length(T)-1 
      x(uu+1) = (1-dt*G_OU/(2.12)^2)*x(uu)+sqrt(dt*D_OU)*randn;
      y(uu+1) =-x(uu+1); 
end
% Normalize to proper moving range
d_1=((final(1)-init(1))^2+(final(2)-init(2))^2)^(1/2);
d_2=((max(y)-min(y))^2+(max(x)-min(x))^2)^(1/2);
nr=d_1/d_2;
x2=x*nr;  y2=y*nr;
mdist_x=abs(min(x2)-final(1));
    if min(x2)>final(1)
     mdist_x=-mdist_x;
    end
x3=x2+mdist_x;
new_x=round(x3);
mdist_y=abs(min(y2)-init(2));
    if min(y2)>init(2)
     mdist_y=-mdist_y;
    end
y3=y2+mdist_y;
new_y=round(y3);


%number file
number_repeat=repmat([1:12000],1,3);
cd('D:\Yiko\Timing stuff\numberFlip')
all_fileNumber= dir('*.jpeg') ;
%video frame file
name=['OU 4322 G',num2str(G_OU) ,' 5min Q85'];
name
% ori_dir =['D:\Yiko\OU video frame\',name];   

%video setting
Time=T; %sec
video_fps=fps;
writerObj = VideoWriter(['D:\Yiko\OU video frame\', name,'.avi']);  %change video name here!
writerObj.FrameRate = video_fps;
writerObj.Quality = 85;
open(writerObj);
%start part: dark adaptation
for kk=1:60
img=0*ones(1024,1280);   
writeVideo(writerObj,  img);
end


for kk =1: length(T)
a=zeros(1024,1280);%full screen pixel matrix %it's the LED screen size

%OU 4322 bar trajectory
X=new_x(kk);
Y=new_y(kk);

rightbY=Y+177-8;
edge01=[];
edge01(1)=X-177-8;
edge01(2)=Y-177+8;
qoo=rightbY-(edge01(2)-1);
ptemp01=[];
for g=1:qoo
ptemp01(1)=edge01(1)+(g-1);
ptemp01(2)=edge01(2)+(g-1);
a(ptemp01(2), ptemp01(1):ptemp01(1)+32)=clean_LEDdiag(ptemp01(2)-round(lefty_bm), ptemp01(1)-round(leftx_bm):ptemp01(1)-round(leftx_bm)+32);
end

for k=1:23-1
    a(edge01(2)-k, edge01(1)+k:edge01(1)+32-k)=clean_LEDdiag(edge01(2)-k-round(lefty_bm), edge01(1)+k-round(leftx_bm):edge01(1)+32-k-round(leftx_bm)); %bottom left corner
    a(ptemp01(2)+k, ptemp01(1)+k:ptemp01(1)+32-k)=clean_LEDdiag(ptemp01(2)+k-round(lefty_bm),ptemp01(1)+k-round(leftx_bm):ptemp01(1)+32-k-round(leftx_bm)); %top right corner
end

%number representing bar position
imgNum=imread(all_fileNumber(number_repeat(kk)).name); %read in image of number
imgNum2=rgb2gray(imgNum);
% figure;imshow(imgNum2)
imgNum2(imgNum2<35)=0;
imgNum2(imgNum2>=35)=255;
imgNum2(imgNum2==255)=1;
temp1=420;
a(temp1+1:temp1+43, 1232: 1280)=imgNum2;

%square_flicker
if mod(kk,3)==1 %odd number
a(500-35:500+35,1230:1280)=1; % white square
elseif mod(kk,3)==2
a(500-35:500+35,1230:1280)=0.2; %gray %test02
else
a(500-35:500+35,1230:1280)=0; % dark
end

% imwrite(a, [ori_dir, '\', num2str(kk,'%06i'),'.png']);
img=[];
img=a;
writeVideo(writerObj,  img);

end

%end part video
for kk=1:10
            img=0*ones(1024,1280);
            img(500-35:500+35,1230:1280)=0.2; %gray 
            writeVideo(writerObj,  img);
end
close(writerObj);

ori_dir02 =['D:\Yiko\OU video frame\workspace_',name,'.mat'];   
save(ori_dir02)
end






%%
% %% Generate video
% 
% %RL
% namelist=cell(1,7);
% namelist(1)={'OU RL G1p55 5min'};
% namelist(2)={'OU RL G2p45 5min'};
% namelist(3)={'OU RL G3p2 5min'};
% namelist(4)={'OU RL G4 5min'};
% namelist(5)={'OU RL G5p7 5min'};
% namelist(6)={'OU RL G7p6 5min'};
% namelist(7)={'OU RL G10p5 5min'};
% %UD
% namelist=cell(1,7);
% namelist(1)={'OU UD G1p55 5min'};
% namelist(2)={'OU UD G2p45 5min'};
% namelist(3)={'OU UD G3p2 5min'};
% namelist(4)={'OU UD G4 5min'};
% namelist(5)={'OU UD G5p7 5min'};
% namelist(6)={'OU UD G7p6 5min'};
% namelist(7)={'OU UD G10p5 5min'};
% 
% 
% 
% for hh=8 %1:length(namelist)
% clearvars -except namelist  hh
% name=char(namelist(hh));
% Time=5*60; %sec
% video_fps=60;
% hh
% name
% 
% writerObj = VideoWriter(['D:\Yiko\OU video frame\',name],'Motion JPEG AVI');  %change video name here!
% writerObj.FrameRate = video_fps;
% open(writerObj);
% 
% %start part: dark adaptation
% for kk=1:60
% img=0*ones(1024,1280);   
% writeVideo(writerObj,  img);
% end
% 
% %stimulation frame
% cd(['D:\Yiko\OU video frame\',name]) %load in the image files
% all_file = dir('*.jpeg') ;
% for i=1:Time*video_fps
%             img=imread(all_file(i).name);
%             writeVideo(writerObj,  img);
% end
% 
% %end part
% for kk=1:5
%             img=0.2*ones(1024,1280); 
%             writeVideo(writerObj,  img);
% end
% close(writerObj);
% 
% end


