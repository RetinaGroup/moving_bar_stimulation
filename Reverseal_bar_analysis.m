%load sort merge file

%%  RL heatmap
%load 0410 nsort reversal LR T09
clear all
load('D:\Yiko\Files for Thesis\04102018\workspace_nsort 0410 reversal_LR_T09')

z=1;
list=[];
list=[0.9];
% change rows for RL(LCD) direction stimulation
%LCD(RL) <=> MEA(UD)
mn(1,:)=[0 7 15 23 31 39 47 0];   mn(8,:)=[0 14 22 30 38 46 54 0];
for i=1:6
    if i==1
            mn(i+1,:)=[1 8 16 24 32 40 48 55];
    else        mn(i+1,:)=[1 8 16 24 32 40 48 55]+i-1;   end
end
BinningSpike2=[]; %the rearranged matrix
%test orientation:
Newori=[];
count=7; %start from #7 
for j=2:7
        for kk=1:8
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
                 count=55;
end
%
count=7; %start from #7 
select_ch=54;
for j=2:7
        for kk=1:8
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                          if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                    if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
                 count=55;
end

% to strengthen the color during moving
BinningSpike2(:,(2+list(z))/BinningInterval+1:end)=[]; %set firing =[] after bar disappear
BinningSpike2(:,1:1/BinningInterval-1)=[]; %set firing =[] before bar shows
BinningSpike2=BinningSpike2./(12-1); %due to 12 trials nad doesnt take last trial

BinningTime(:,(2+list(z))/BinningInterval+1:end)=[]; 
BinningTime(:,1:1/BinningInterval-1)=[]; 
BinningTime=BinningTime-BinningTime(1);

figure;
imagesc(BinningTime,[1:60],BinningSpike2);   hold on

x4=0.9; line([x4 x4], get(gca, 'ylim'),'Color','w','LineStyle','--');  %bar start moving

% set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
ylabel('Channel Number')
xlabel('Time (sec)')
set(gca,'LineWidth',1.3);
set(gca, 'XTick', [0 0.8 1.6 ])
set(gca, 'YTick', [10 30 50])
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[1  2 3]) %set colorbar tick




%% PSTH 
clear all
cd('D:\Yiko\Files for Thesis\04102018\New folder')
fname=['D:\Yiko\Files for Thesis\04102018\New folder']; %for saving
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 

for z =4
list=[];
list=[ 0.9 0.9 0.9 0.9];
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

T1=1; %sec %choose time range for plotting
T2=3;

%bin=10ms
BinningInterval=0.01;
frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
BinningTime2=[];
BinningTime2=BinningTime(frame1:frame2)-T1;

BinningSpike2 =zeros(60,frame2-frame1+1);
for i=[1 5 7 8 9 17 25 26 ]  %select neurons
    BinningSpike2(i,:)=BinningSpike(i,frame1:frame2) ;
end

%sum all selected channels' firing 
s=0;
for channel=[ 1 5 7 8 9 17 25 26] %only select reversal channels
    s= s + BinningSpike2(channel,:);
end
figure; plot(BinningTime2,s/(BinningInterval)/8/(12-1)); %17 is the total number of selected channels % s-1 %12 trails and dont take the last one


x1=list(z); line([x1 x1], get(gca, 'ylim'),'Color','r','LineStyle','--');  %bar reverse

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate(Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'YTick', [10  60 110])
% saveas(gca, fullfile(fname, name), 'fig');
end


