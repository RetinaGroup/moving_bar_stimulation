%generate channel map according to  calibration at October 2017 (orignial MEA with 8020cube microscope)
%parameters from code "Space_calibration_Oct2017"
%% according to new calibration mea region:
mea_size=503; 
meaCenter_x=630; 
meaCenter_y=573;  

channel0 =[meaCenter_x-((mea_size-1))/2, meaCenter_y-((mea_size-1))/2 ]; %top left point
channel64=[meaCenter_x+((mea_size-1))/2, meaCenter_y+((mea_size-1))/2 ];  % bottom right position
channel_map=[];
%generate matrix "channel_map"
 if ~isempty(channel0) && ~isempty(channel64)
    vdx = ([(channel0(1)+channel0(2)+channel64(1)-channel64(2))/2,(-channel0(1)+channel0(2)+channel64(1)+channel64(2))/2] - channel0)/7;
        %           (a-b+c+d)/2,(a+b-c+d)/2
    vdy = ([(channel0(1)-channel0(2)+channel64(1)+channel64(2))/2,(channel0(1)+channel0(2)-channel64(1)+channel64(2))/2] - channel0)/7;    
    for i = 1:64
        tempx = floor((i-1)/8);
        tempy = mod(i-1,8);
        channel_map{i} = channel0+tempy*vdx+tempx*vdy;
    end
    channel_map{65} = (channel0+channel64)/2;    %center
    channel_map{66} = (channel_map{25}+channel_map{33})/2;   %up
    channel_map{67} = (channel_map{32}+channel_map{40})/2;   %down
    channel_map{68} = (channel_map{4}+channel_map{5})/2;   %left
    channel_map{69} = (channel_map{60}+channel_map{61})/2;   %right
 end

 clearvars -except channel_map
 save('D:\Yiko\Code\channel_map_11132017','channel_map');
 
 %% check
 init= channel_map{68} ;
 final= channel_map{69} ;
 
X = init(1); 
X=final(1);
Y =(init(2)+final(2))/2; 
barX=X-round(leftx);
barY=round(Y)-round(lefty);
barImage=LED_color_1st(barY-bar_le: barY+bar_le, barX-bar_wid: barX+bar_wid); 
[s1, s2] = size(barImage);
barRect = [0 0 s2 s1];
bardstRects = nan(4, 1);
bardstRects(:, 1) = CenterRectOnPointd(barRect, X,Y);
barTexture = Screen('MakeTexture', w, barImage);
Screen('DrawTextures', w, barTexture, [], bardstRects, [], [], []);  %draw bar
Screen('Flip', w);

%%
 Screen('Preference', 'SkipSyncTests', 1)
global rect w
[w, rect] = Screen('OpenWindow',2, [0 0 0]); %black background
%plot mea region(not bigger mea region
 mea_size=473; %pixel %just try and error %use odd number!!!
 baseRect = [0 0 mea_size mea_size]; 
meaCenter_x=575;
meaCenter_y=465;
centeredRect = CenterRectOnPointd(baseRect, meaCenter_x, meaCenter_y);
Screen('FillRect', w, 170,centeredRect); 
 %plot channel0 and channel64 position
dotSize=10;
dotColors=255;
dotPositionMatrix(1,:)=[channel0(1) channel64(1) ]; % x coordinate / same as using GetMouse's x coordinate
dotPositionMatrix(2,:)=[channel0(2) channel64(2)];  % y coordinate / same as using GetMouse's y coordinate
Screen('DrawDots', w, dotPositionMatrix,dotSize, dotColors,[],2); %2 is to specify dot type
Screen('Flip', w);

 
 
 
 
