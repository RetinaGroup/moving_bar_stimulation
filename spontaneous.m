%% spontaneous
 temp=[];  
 temp(1)=0;  
 temp(2)=3*60;  %change yourself
cut_spikes = seperate_trials(Spikes,temp); 
DataTime=3*60; %change yourself
% Binning
BinningInterval = 1/100;  %s
BinningTime = [BinningInterval : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 
figure;  imagesc(BinningTime,[1:60],BinningSpike);
title('before Spontaneous   /  BinningInterval=10ms')
colorbar
set(gca,'fontsize',12)
xlabel('time(s)');  
ylabel('channel ID');
saveas(gcf, ['Before Spontanrous  BinningInterval=10ms'],'fig');