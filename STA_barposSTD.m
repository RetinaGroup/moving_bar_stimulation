clear all
% load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G025')

cd('D:\Yiko\Experiment\04142018\0414OU merge') ;  % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;
% figure;
% hist(abs(diff(bin_pos)),10)

ch_list=[25 19]; %choose channel
for channel=ch_list
    figure;
for z =2:4
clearvars -except all_file n_file z  channel ch_list minp maxp
cd('D:\Yiko\Experiment\04142018\0414OU merge') ;  % the folder of the files
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name

%rescale the number of bin_pos
reStim=bin_pos-min(bin_pos)+12; % let the first element be at number 12 in the 503x503 matrix

%make BinningSpike
BinningTime =diode_BT;
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ; %yk_spike already cut off before and after stimulation
    BinningSpike(i,:) = n ;
end

bar_wid=11;
stim_size=503+2*bar_wid; 
before_frames=60; %180
future_frames=30;

STA_frame_past=[]; 
STA_frame_future=[]; 

find_spikes=[]; 
find_spikes=find(BinningSpike(channel,:) > 0 );  %find spikes location

for frame=1:before_frames+1  %+1: including zeroth frame
temp=zeros(stim_size,stim_size);
sum_spikes=0;
        for h=find_spikes   
                if h-frame <=0
                         h=h;
                else
                    %make bar image for each frame
                    tempFrame=zeros(stim_size,stim_size);
                    barcenter=reStim(h-frame+1); %+1: including zeroth frame
                    % moving bar image (for all directions) just plot all dircetions the same
                    tempFrame(barcenter-11:barcenter+11,:)=1;
                    
                    %weighting with firing rate
                    temp=temp+BinningSpike(channel,h)*tempFrame; 
                    
                end
         sum_spikes=sum_spikes+BinningSpike(channel,h);
        end

%weighting with firing rate
STA_frame_past{channel,frame}=temp./sum_spikes;
end

%Future part
for frame=1:future_frames
temp=zeros(stim_size,stim_size);
sum_spikes=0;

        for h=find_spikes
                if h+frame >=length(reStim)
                         h=h;

                else
                    %make bar image for each frame
                    tempFrame=zeros(stim_size,stim_size);
                    barcenter=reStim(h+frame);
                     % moving bar image (for all directions) just plot all dircetions the same
                    tempFrame(barcenter-11:barcenter+11,:)=1;
                    
                    %weighting with firing rate
                    temp=temp+BinningSpike(channel,h)*tempFrame; 
                    
                end
         sum_spikes=sum_spikes+BinningSpike(channel,h);
        end

%weighting with firing rate
STA_frame_future{channel,frame}=temp./sum_spikes;
end

STA_frame=[fliplr(STA_frame_future)  STA_frame_past];


%calculate std of bar position
 ch=channel;
        for frame=1:before_frames+future_frames+1  %+1: including zeroth frame
        shimage=STA_frame{ch,frame};
        sliceimage=shimage(:,1); %all direction
        sliceimage2=sliceimage./max(sliceimage(:)); %weights=prob dist

        %%%information: https://www.mathworks.com/matlabcentral/answers/142383-calculate-standard-deviation-from-pdf
        X = [1:525];     % Data
        W =  sliceimage2';    % Weights
        N = length(X);
        Wmean = X*W'/sum(W);    % Weighted mean
        Wstd = sqrt((sum(W.*(X - Wmean).^2)/N) / ((N-1)*sum(W)/N)); % Weighted std

        pos_std(frame)=Wstd;
        end

% figure;
plot(fliplr(pos_std)); hold on; %flip left and right is to let future part in the large value of x
title(num2str(ch))
% cd('C:\Users\hydrolab_Retina\Desktop\0423barstd')
% saveas(gcf, ['pos_std ',name,' ch',num2str(ch)],'fig');
minp(1,z)= min(pos_std) ;
maxp(1,z)= max(pos_std) ;

end
line([before_frames before_frames],[ min(minp)  max(maxp)]);
legend('-DynamicLegend');
legend('show')
lgd = legend('G04','G057','G19');

saveas(gcf,['D:\Yiko\Experiment\04142018\0414 OU barstd STA\0414 barStdSTA 3G_ch ',num2str(channel)],'fig');
minp=[];
maxp=[];
end