clear all
%close all
cd('D:\Yiko\Experiment\03292018\merge OU RL') ;  % the folder of the files
% name='0404 HMM RL 4G';
all_file = subdir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ;  
cc=hsv(9); %for color chosen

% name='0425 pos2 HMM G043 4direction';

%Tina orientation
rr =[9,17,25,33,41,49,...
          2,10,18,26,34,42,50,58,...
          3,11,19,27,35,43,51,59,...
          4,12,20,28,36,44,52,60,...
          5,13,21,29,37,45,53,61,...
          6,14,22,30,38,46,54,62,...
          7,15,23,31,39,47,55,63,...
            16,24,32,40,48,56];
      
count=1;
figure;
for z =[1 4 6] %choose file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


figure;
for channelnumber=1:60
%% put your stimulus here!!
TheStimuli=bin_pos;

%% velocity
% HMM
% for i=1:length(newXarray)-1
%     v(i)=(newXarray(i+1)-newXarray(i))/17; %17ms
% end
% TheStimuli= v;

%OU
% for i=1:length(new_y)-1
%     v(i)=(new_y(i+1)-new_y(i))/17; %17ms
% end
% TheStimuli= v; 


%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
% BinningTime = [ 0 : BinningInterval : DataTime];
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    %[n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
n = channelnumber;
Neurons = BinningSpike(n,:);  %for single channel
%Neurons = sum(BinningSpike(1:60,:));  %calculate population MI

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%    norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
t=[-backward*bin:bin:forward*bin];

h=subplot(8,8,rr(n)); hold on;
% plot(t, information ,'color',cc(count,:),'LineWidth',0.8);
plot(t, information-information_shuffle,'LineWidth',0.8);
xlim([ -3000 3000])
ylim([0 inf+0.1])
title(channelnumber)


end
%test box
% str =['unsort  ',name];
% yy=annotation('textbox',[0.45 .8 .2 .2],'String',str,'FitBoxToText','on');

count=count+1;
% saveas(gcf,['unsort  60MI_minus shuffled ',name],'fig');

end
    