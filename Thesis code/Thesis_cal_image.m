%% plot luminance histogram images
%before calibration
A2= imread('D:\Yiko\Calibration\Oct2017_calibration\Lum_cal\Lum_1st_gain0_expT800000.tif');
selected_img_1st=A2(y1:y2,x1:x2);
figure; histogram(selected_img_1st)
xlim([50 120])
set(gca,'xtick',[  60 80 100 120]);
set(gca,'Position',[0.17 0.2  0.75 0.7])
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
%calculate luminance according to monitor Bri=25
meanlum=0.5169; %mW/m^2
multvalue=meanlum/mean(selected_img_2nd(:,round(mpos(1))));
[60 80 100 120]*multvalue
set(gca,'XTickLabel', {'0.31','0.41','0.51','0.62'} )
set(gca,'ytick',[ 0 0.5 1 1.5 2]*10^4);
set(gca,'YTickLabel', {'0','0.5','1','1.5','2'} )
ylabel('Number of pixles(10^4)')
xlabel('Luminance (mW/m^{2})')


%after calibration
A2=[];
A2= imread('D:\Yiko\Calibration\Oct2017_calibration\Lum_cal\Lum_2nd_gain0_expT800000.tif');
selected_img_2nd=A2(y1:y2,x1:x2);
figure; histogram(selected_img_2nd)
xlim([50 120])
set(gca,'xtick',[  60 80 100 120]);
set(gca,'Position',[0.17 0.2  0.75 0.7])
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
%calculate luminance according to monitor Bri=25
meanlum=0.5169; %mW/m^2
multvalue=meanlum/mean(selected_img_2nd(:,round(mpos(1))));
[60 80 100 120]*multvalue
set(gca,'XTickLabel', {'0.31','0.41','0.51','0.62'} )
set(gca,'ytick',[ 0 2 4 6 8 10]*10^4);
set(gca,'YTickLabel', {'0','2','4','6','8','10'} )
ylabel('Number of pixles(10^4)')
xlabel('Luminance (mW/m^{2})')


%% Plot  luminance distortion of a slice before and after valibration / columns and rows
clear all
x1=717; %top left x
x2=1425; %bottom right x
y1=669;  %top left y
y2=1377;  %bottom right y

%before calibration
A2= imread('D:\Yiko\Calibration\Oct2017_calibration\Lum_cal\Lum_1st_gain0_expT800000.tif');
% figure; imshow(A2(y1:y2,x1:x2));
selected_img_1st=A2(y1:y2,x1:x2);
% figure; histogram(selected_img_1st)

%after calibration
A2=[];
A2= imread('D:\Yiko\Calibration\Oct2017_calibration\Lum_cal\Lum_2nd_gain0_expT800000.tif');
% figure; imshow(A2(y1:y2,x1:x2));
%test whether it's correct on figure
selected_img_2nd=A2(y1:y2,x1:x2);
%check distortion level
% figure; histogram(selected_img_2nd)

mpos=[];
mpos(1,:)=[length(selected_img_2nd)/2]; %x coordinate
mpos(2,:)=[length(selected_img_2nd)/2]; %y coordinate

%plot column
figure;
plot(selected_img_1st(:,round(mpos(1))));  %column
hold on;
plot(selected_img_2nd(:,round(mpos(1))));  %column
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Luminance (mW/m^{2})')
xlabel('Position (pixel)')
box on
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([10 680])

lgd = legend('Before', 'After');
lgd.FontSize = 15;
legend boxoff  
%calculate luminance according to monitor Bri=25
meanlum=0.5169; %mW/m^2
multvalue=meanlum/mean(selected_img_2nd(:,round(mpos(1))));
set(gca, 'YTick', [85 95 105 115])
[85 95 105 115]*multvalue
set(gca,'YTickLabel', {'0.44','0.49','0.54','0.59'} )



%plot row
figure;
plot(selected_img_1st(round(mpos(2)),:));  %row
hold on;
plot(selected_img_2nd(round(mpos(2)),:));  %row
set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Luminance (mW/m^{2})')
xlabel('Position(pixel)')
box on
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
xlim([10 680])
% set(gca, 'YTick', [10  60 110])
% set(gca,'XTickLabel', {'0','100','200','300','400'} )
lgd = legend('Before', 'After');
lgd.FontSize = 15;
legend boxoff  
%calculate luminance according to monitor Bri=25
meanlum=0.5169; %mW/m^2
multvalue=meanlum/mean(selected_img_2nd(:,round(mpos(1))));
set(gca, 'YTick', [85 95 105 115])
[85 95 105 115]*multvalue
set(gca,'YTickLabel', {'0.44','0.49','0.54','0.59'} )







