%use PTB, so first need to open the PTB interface

t=zeros(1,2);
n = 1; % the squares on the y axis
pulse_dur = 2; % 2s of stimuli flash

checks =zeros(size(checkerboard(round(rect(4)/n),n))); %black
t(1,2) = Screen('MakeTexture', w, checks);
checks =255*ones(size(checkerboard(round(rect(4)/n),n))); %white
t(1,1) = Screen('MakeTexture', w, checks);

daq.Pace = daqmx_Task('dev1/ao0');
daq.Pace.write(-0.5);

Screen('DrawTexture', w, t(1,2)); %black
Screen('Flip', w); 
start_time = GetSecs;    
thetime = GetSecs - start_time; % time (sec) since loop started  
    while thetime < 5 %5 sec rest first
    thetime = GetSecs - start_time; % time (sec) since loop started 
        if stop_flag == 1;    break; end
    end

%% start on-off stimulation
for trial=1:7  %7 trials
trial
mm=1;

for mm = 1:3  %for each trial
mm
daq.Pace.write(0.5);

    Screen('DrawTexture', w, t(1,1)); %white
    Screen('Flip', w); 
    
    start_time = GetSecs;    
    thetime = GetSecs - start_time; % time (sec) since loop started  
    flick_time = pulse_dur;
    while thetime < flick_time 
    thetime = GetSecs - start_time; % time (sec) since loop started 
        if stop_flag == 1;    break; end
    end
    
daq.Pace.write(-0.5);  
    Screen('DrawTexture', w, t(1,2)); %black
    Screen('Flip', w); 
    
    start_time = GetSecs;    
    thetime = GetSecs - start_time; % time (sec) since loop started  
    flick_time = pulse_dur;
    while thetime < flick_time 
    thetime = GetSecs - start_time; % time (sec) since loop started 
        if stop_flag == 1;    break; end
    end


    if stop_flag == 1;    break; end
    mm=mm+1;  
end

    start_time = GetSecs;    
    thetime = GetSecs - start_time; % time (sec) since loop started  
    while thetime < 5 % time to reverse contrast
    thetime = GetSecs - start_time; % time (sec) since loop started 
        if stop_flag == 1;    break; end
    end

end

figure(1) 


