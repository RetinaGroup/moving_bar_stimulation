%% On Off heatmap
%load file first and check
figure;plot(a_data(3,:)); hold on
plot(TimeStamps*20000,36000,'*')

%on off  timestamps
temp=[];
temp(1)=TimeStamps(1);
temp(2)=TimeStamps(4);
temp(3)=TimeStamps(7);
temp(4)=TimeStamps(10);
temp(5)=TimeStamps(13);
temp(6)=TimeStamps(16);
temp(7)=TimeStamps(19);
temp(8)=TimeStamps(19)+17.03;

temp=temp./20000.

figure;plot(a_data(3,:));  hold on
plot(temp*20000,35000,'*')

cut_spikes = seperate_trials(Spikes,temp); 
DataTime=temp(2)-temp(1);
% Binning
BinningInterval = 1/100;  %s
BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
%     [n,~] = hist(cut_spikes{i},BinningTime) ;
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%%  Heat Map / all channels
figure;  
imagesc(BinningTime,[1:60],BinningSpike);
colorbar
title('before OnOff   /  BinningInterval=10ms')
xlabel('time(s)');   ylabel('channel ID');
saveas(gcf, ['Before Onoff BinningInterval=10ms'],'fig');

%Heatmap /  specific channels only
sp_BinningSpike=[];
ff=1;
chlist=[54 53 52  18 17 16   22 21 12]; %choose channels
for ch=chlist
    sp_BinningSpike(ff,:)=BinningSpike(ch,:);
    ff=ff+1;
end
figure;  
imagesc(BinningTime,[1,length(chlist)],sp_BinningSpike);


%% PSTH
% subplot all channels' OnOff   PSTH
s=0;
for channelnumber=1:60
s= s+ BinningSpike(channelnumber,:);
title(channelnumber)
end
figure;
plot(BinningTime,s);
% saveas(gcf, ['Before Onoff PSTH BinningInterval=10ms  LCD screen orientation'],'fig');
      
%plot single channel PSTH
channelnumber=18;
figure;
s=0;
s=  BinningSpike(channelnumber,:);
plot(BinningTime,s);
xlim([0 18])
title(channelnumber)
