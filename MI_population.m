
cd('D:\Yiko\Files for Thesis\04232018\sortp HMM pos2 RL Gs just differnet order') ; 
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 
cc=hsv(11); %for color chosen
ch_inf=[];ch_inf2=[];


count=1;
Pchannel=[47 37 31 23 21]; %choose channel
figure;
for trial=1:5

 numberofch=trial; %select how many number of cells you want as a group
for z =8  %[1 2 4 7 8]
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
z
name


%% put your stimulus here!
TheStimuli=bin_pos;

%% Binning
bin=BinningInterval*10^3; %ms

%% make the array length of sti and response the same: drop the last number
BinningTime =diode_BT;

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
%figure; hist(isi2,StimuSN);
% title(name);

%% BinningSpike
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(yk_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end

%% pick cells as a group
pick_ch= datasample(Pchannel, numberofch); %random pick x channels
%sum the firing rate of these cells
sum_BinningSpike=zeros(1,length(BinningSpike));
for cha=pick_ch
sum_BinningSpike=sum_BinningSpike+BinningSpike(cha,:);
end


%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
Neurons = sum_BinningSpike;  

dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons(forward+1-i:length(Neurons)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
        norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
%% shuffle MI
sNeurons=[];
r=randperm(length(Neurons));
for j=1:length(r)            
    sNeurons(j)=Neurons(r(j));
end
Neurons_shuffle=sNeurons;

dat=[];information_shuffle_p=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        x=Neurons_shuffle((i-1)+forward+1:length(Neurons_shuffle)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;
 
        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp-1;
        information_shuffle_p(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);
    end  

    dat=[];information_shuffle_f=[];temp=0;sdat=[];
    for i=1:forward
        x = Neurons_shuffle(forward+1-i:length(Neurons_shuffle)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize
        norm = BinningInterval;

        [N,C]=hist3(dat{i},[max(Neurons_shuffle)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        information_shuffle_f(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information_shuffle=[information_shuffle_p, information_shuffle_f];
    

time=[-backward*bin:bin:forward*bin];
plot(time,information-information_shuffle,'LineWidth',1.5); hold on; %,'color',cc(z,:));hold on
hold on;
xlabel('delta t (ms)');ylabel('MI (bits/second)( minus shuffled)');
set(gca,'fontsize',12); hold on


legend('-DynamicLegend');
legend('show')
% lgd = legend('\tau =0.16 sec', '\tau =0.38 sec','\tau =0.55 sec','\tau =1.07 sec','\tau =1.28 sec');
lgd = legend('1','2','3','4','5');

xlim([ -backward*bin backward*bin])
ylim([0 inf])
title([name]) 
end
count=count+1;

end
