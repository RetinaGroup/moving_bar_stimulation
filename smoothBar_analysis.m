%% Oneway UD
clear all
load('D:\Yiko\Files for Thesis\12132017\retina2 pos2\oneway\sorted_merge_1213oneway_UD_re2_pos2')

%select only soma channels(Tina #!)
 Spikes2=cell(1,60);
for i=[5     7     9    11    14    15    19    22    24    27    30    35    38    43    44    50    51    58    59    60]
 Spikes2{i}= Spikes{i};
end
Spikes=cell(1,60);
Spikes=Spikes2;

% Binning : 10ms
cut_spikes=cell(1,60);
cut_spikes = seperate_trials(Spikes,TimeStamps); 
DataTime=TimeStamps(2)-TimeStamps(1);

BinningInterval = 1/100;  %s
BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(cut_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 

%Plot Heatmap UD
T=0.8;
Tbin=T/BinningInterval;
figure;
imagesc(BinningTime(1,1:Tbin),[1:60],BinningSpike(:,1:Tbin)./(10-1));  %due to 10 trials 
set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',15); 
ylabel('Channel Number')
xlabel('Time (sec)')
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'XTick', [0 0.35 0.7 ])
set(gca, 'YTick', [10 30 50])
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[0.5 1 1.5]) %set colorbar tick
set(gca,'LineWidth',1.3);

%% Plot square dashed box
lineT=[0:0.1:0.8];
lineP=[60 55 47 39 31 23 15 7 1];
for k=1:8 %8 boxes
      line([lineT(k) lineT(k)],[lineP(k) lineP(k+1)] ,get(gca, 'ylim'),'Color','w','LineStyle','--');  
      line([lineT(k+1) lineT(k+1)],[lineP(k) lineP(k+1)] ,get(gca, 'ylim'),'Color','w','LineStyle','--');  
      line([lineT(k) lineT(k+1)] ,[lineP(k) lineP(k)],'Color','w','LineStyle','--');  
      line([lineT(k) lineT(k+1)],[lineP(k+1) lineP(k+1)] ,'Color','w','LineStyle','--');  
end


%% Oneway RL
clear all
load('D:\Yiko\Files for Thesis\12132017\retina2 pos2\oneway\sorted_merge_1213oneway_RL_re2_pos2')

%select only soma channels(Tina #!)
 Spikes2=cell(1,60);
for i=[5     7     9    11    14    15    19     24    27    30    35    38    43    44    50    51    58    59    60]
 Spikes2{i}= Spikes{i};
end
Spikes=cell(1,60);
Spikes=Spikes2;

% Binning : 10ms
cut_spikes=cell(1,60);
cut_spikes = seperate_trials(Spikes,TimeStamps); 
DataTime=TimeStamps(2)-TimeStamps(1);

BinningInterval = 1/100;  %s
BinningTime = [ BinningInterval : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60  % i is the channel number
    [n,~] = hist(cut_spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end 

% change rows for RL(LCD) direction stimulation: make BinningSpike2
%LCD(RL) <=> MEA(UD)
mn(1,:)=[0 7 15 23 31 39 47 0];   mn(8,:)=[0 14 22 30 38 46 54 0];
for i=1:6
    if i==1
            mn(i+1,:)=[1 8 16 24 32 40 48 55];
    else        mn(i+1,:)=[1 8 16 24 32 40 48 55]+i-1;   end
end
BinningSpike2=[]; %the rearranged matrix
%test orientation:
Newori=[];
count=7; %start from #7 
for j=2:7
        for kk=1:8
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
                 count=55;
end
%
count=7; %start from #7 
select_ch=54;
for j=2:7
        for kk=1:8
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                          if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                    if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
                 count=55;
end


%Plot Heatmap RL
T=0.8;
Tbin=T/BinningInterval;
figure;
imagesc(BinningTime(1,1:Tbin),[1:60],BinningSpike2(:,1:Tbin)./(10-1));  %due to 10 trials 
set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',15); 
ylabel('Channel Number')
xlabel('Time (sec)')
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'XTick', [0 0.35 0.7 ])
set(gca, 'YTick', [10 30 50])
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[0.5 1 1.5]) %set colorbar tick
set(gca,'LineWidth',1.3);
% set(gca,'YColor', 'g')

