clear all
load('D:\Yiko\Files for Thesis\04232018\0423 merge sort1\merge sort1 unit1 _HMM RL G043')

%auto mutual information for stimulus
T=7*60; %second
%auto MI
TheStimuli =  idealStimuli;
Neurons =  idealStimuli; 

%stimuli in future
% dd=length(idealStimuli);
% TheStimuli =  idealStimuli(1:dd-50);
% Neurons =  idealStimuli(51:end); 

% stimuli in past
dd=length(idealStimuli);
TheStimuli = idealStimuli(51:end);
Neurons =  idealStimuli(1:dd-50) ; 

%% Binning
fps=1/60;
multiple_value=1;
DataTime=T;
BinningInterval = DataTime/length(TheStimuli); %s
bin=BinningInterval*10^3; %ms

%% cut Stimulus State _ equal probability of each state (different interval range)
StimuSN=30; %number of stimulus states
nX=sort(TheStimuli);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; isi2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    isi2(temp) = find(TheStimuli(jj)<=intervals,1);
end 
% figure; hist(isi2,StimuSN);
% title(name);

 %% cut Neurons state
 nX=[];
 StimuSN=30; %number of stimulus states
nX=sort(Neurons);
abin=length(nX)/StimuSN;
intervals=[nX(1:abin:end) inf]; % inf: the last term: for all rested values
temp=0; Neurons2=[];
for jj=1:length(TheStimuli)
    temp=temp+1;
    Neurons2(temp) = find(Neurons(jj)<=intervals,1);
end 


%% Predictive information
backward=ceil(3000/bin); forward=ceil(3000/bin);
dat=[];informationp=[];temp=backward+2;
%         norm = BinningInterval; %bits/second
norm =1;

    for i=1:backward+1 %past(t<0)
        x=Neurons2((i-1)+forward+1:length(Neurons2)-backward+(i-1))';
        y=isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike

        [N,C]=hist3(dat{i},[max(Neurons2)+1,max(isi2)]); %20:dividing firing rate  6:# of stim

        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];

        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
 
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
        corrp(temp)=sum(x.*y);

    end  

    dat=[];informationf=[];temp=0;dat=[];
    for i=1:forward
        x = Neurons2(forward+1-i:length(Neurons2)-backward-i)';
        y = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[x,y];
%       norm = sum(x)/ length(x); %normalize: bits/spike
%         norm = BinningInterval; %bits/second

        [N,C]=hist3(dat{i},[max(Neurons2)+1,max(isi2)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/norm;
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
        corrf(temp)=sum(x.*y);
    end
    
information=[informationp informationf];
   
t=[-backward*bin:bin:forward*bin];

figure;
plot(t,information,'LineWidth',1.5); hold on
xlabel( '\delta t (ms)')
ylabel('MI (bits)');
set(gca, 'Fontname', 'Times New Roman');
set(gca,'fontsize',18); hold on
xlim([-2800 2800])


