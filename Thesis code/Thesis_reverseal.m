%load sort merge file

%%  RL heatmap
%load 0410 nsort reversal LR T09
clear all
load('D:\Yiko\Files for Thesis\04102018\workspace_nsort 0410 reversal_LR_T09')

z=1;
list=[];
list=[0.9];
% change rows for RL(LCD) direction stimulation
%LCD(RL) <=> MEA(UD)
mn(1,:)=[0 7 15 23 31 39 47 0];   mn(8,:)=[0 14 22 30 38 46 54 0];
for i=1:6
    if i==1
            mn(i+1,:)=[1 8 16 24 32 40 48 55];
    else        mn(i+1,:)=[1 8 16 24 32 40 48 55]+i-1;   end
end
BinningSpike2=[]; %the rearranged matrix
%test orientation:
Newori=[];
count=7; %start from #7 
for j=2:7
        for kk=1:8
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
                 count=55;
end
%
count=7; %start from #7 
select_ch=54;
for j=2:7
        for kk=1:8
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                          if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                    if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
                 count=55;
end

% to strengthen the color during moving
BinningSpike2(:,(2+list(z))/BinningInterval+1:end)=[]; %set firing =[] after bar disappear
BinningSpike2(:,1:1/BinningInterval-1)=[]; %set firing =[] before bar shows
BinningSpike2=BinningSpike2./(12-1); %due to 12 trials nad doesnt take last trial

BinningTime(:,(2+list(z))/BinningInterval+1:end)=[]; 
BinningTime(:,1:1/BinningInterval-1)=[]; 
BinningTime=BinningTime-BinningTime(1);

figure;
imagesc(BinningTime,[1:60],BinningSpike2);   hold on

x4=0.9; line([x4 x4], get(gca, 'ylim'),'Color','w','LineStyle','--');  %bar start moving

% set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
ylabel('Channel Number')
xlabel('Time (sec)')
set(gca,'LineWidth',1.3);
set(gca, 'XTick', [0 0.8 1.6 ])
set(gca, 'YTick', [10 30 50])
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[1  2 3]) %set colorbar tick



clear all
load('D:\Yiko\Files for Thesis\04102018\workspace_nsort 0410 reversal_LR_T09')

z=1;
list=[];
list=[0.9];
% change rows for RL(LCD) direction stimulation
%LCD(RL) <=> MEA(UD)
mn(1,:)=[0 7 15 23 31 39 47 0];   mn(8,:)=[0 14 22 30 38 46 54 0];
for i=1:6
    if i==1
            mn(i+1,:)=[1 8 16 24 32 40 48 55];
    else        mn(i+1,:)=[1 8 16 24 32 40 48 55]+i-1;   end
end
BinningSpike2=[]; %the rearranged matrix
%test orientation:
Newori=[];
count=7; %start from #7 
for j=2:7
        for kk=1:8
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
                 count=55;
end
%
count=7; %start from #7 
select_ch=54;
for j=2:7
        for kk=1:8
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                          if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    BinningSpike2(count,:)=BinningSpike(mn(j,kk),:);
                    if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
                 count=55;
end

% to strengthen the color during moving
BinningSpike2(:,(2+list(z))/BinningInterval+1:end)=[]; %set firing =[] after bar disappear
BinningSpike2(:,1:1/BinningInterval-1)=[]; %set firing =[] before bar shows
BinningSpike2=BinningSpike2./(12-1); %due to 12 trials nad doesnt take last trial

BinningTime(:,(2+list(z))/BinningInterval+1:end)=[]; 
BinningTime(:,1:1/BinningInterval-1)=[]; 
BinningTime=BinningTime-BinningTime(1);

figure;
imagesc(BinningTime,[1:60],BinningSpike2);   hold on

x4=0.9; line([x4 x4], get(gca, 'ylim'),'Color','w','LineStyle','--');  %bar start moving

% set(gca,'Position',[0.17 0.2  0.75 0.7])
box on
set(gca,'fontsize',15);
set(gca, 'Fontname', 'Times New Roman');
ylabel('Channel Number')
xlabel('Time (sec)')
set(gca,'LineWidth',1.3);
set(gca, 'XTick', [0 0.8 1.6 ])
set(gca, 'YTick', [10 30 50])
cbr = colorbar ; %Create Colorbar
set(cbr,'YTick',[1  2 3]) %set colorbar tick



%% PSTH 
clear all
cd('D:\Yiko\Files for Thesis\04102018\New folder')
fname=['D:\Yiko\Files for Thesis\04102018\New folder']; %for saving
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 

for z =4
list=[];
list=[ 0.9 0.9 0.9 0.9];
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

T1=1; %sec %choose time range for plotting
T2=3;

%bin=10ms
BinningInterval=0.01;
frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
BinningTime2=[];
BinningTime2=BinningTime(frame1:frame2)-T1;

BinningSpike2 =zeros(60,frame2-frame1+1);
for i=[1 5 7 8 9 17 25 26 ]  %select neurons
    BinningSpike2(i,:)=BinningSpike(i,frame1:frame2) ;
end

%sum all selected channels' firing 
s=0;
for channel=[ 1 5 7 8 9 17 25 26] %only select reversal channels
    s= s + BinningSpike2(channel,:);
end
%1 2 3 4 5 6 7 8 9 11 12 17 20 21 25 26 27
figure; plot(BinningTime2,s/(BinningInterval)/8/(12-1)); %17 is the total number of selected channels % s-1 %12 trails and dont take the last one


x1=list(z); line([x1 x1], get(gca, 'ylim'),'Color','r','LineStyle','--');  %bar reverse

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate(Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
set(gca, 'YTick', [10  60 110])
% saveas(gca, fullfile(fname, name), 'fig');
end


%% reversal latency %to find values
clear all
cd('D:\Yiko\Files for Thesis\04102018\New folder')
fname=['D:\Yiko\Files for Thesis\04102018\New folder']; %for saving
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 
latency_list=[];

for z =1:n_file
clearvars -except all_file n_file latency_list
list=[];
list=[ 0.2 0.5 0.7 0.9 1.2 2];
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

for k=[1 5  7 8 9 17 25 26 ]
figure; plot(BinningTime,BinningSpike(k,:))
title(k)
end

%bin=10ms
BinningInterval=0.01;
afterT=1;
afterframe=afterT/BinningInterval;
count=1;
for channel=[1 5  7 8 9 17 25 26 ]
latency_list(count,1)=channel;
latency_list(count,z+1)=BinningTime(find(BinningSpike(channel,:)==max(BinningSpike(channel,afterframe:end)),1,'last'))-1-list(z);
count=count+1;
end

end


%% Plot reversal latency with error bar
%selected channel:1 5 7 8 9 17 25 26
v=[ 0.5 0.7 0.9 1.2 2 ]; %reversal time
mea_range=1.416;
x=mea_range./v; %velocity
y=[0.1957 0.1875 0.2362 0.203 0.22]; %mean latency for these selected channels
err=[0.00494 0.005 0.00488 0.00744 0.00577];
figure;
e=errorbar(x,y,err);
e.Marker = 'square';
e.Color = 'black';
e.MarkerFaceColor='black';

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Latency(sec)')
xlabel('Speed (mm/s)')
set(gca, 'Fontname', 'Times New Roman');
box on
set(gca,'fontsize',15); 
ylim([0.1 0.3])
set(gca, 'YTick', [0.1 0.2 0.3])


%% Plot PSTH for 3 velocity for all soma channels
clear all
cd('D:\Yiko\Files for Thesis\04102018\New folder\rev fpr thesis plot')
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 

for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

list=[];
list=[0.5 0.9 2];

T1=1; %sec %choose time range for plotting
T2=1+list(z)*2+1;

%bin=10ms
BinningInterval=0.01;
frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
BinningTime2=[];
BinningTime2=BinningTime(frame1:frame2)-T1;

BinningSpike2 =zeros(60,frame2-frame1+1);
%select only soma
for i=[1     2     3     4     5     6     7    10    11    12    13    14    17    21    23    25    28    29   31    32    35    36    38    39    41    42    43    44    45    46    47    48    49    50    56    57   58    59    60]
    BinningSpike2(i,:)=BinningSpike(i,frame1:frame2) ;
end

%sum all selected channels' firing 
s=0;
for channel=[1     2     3     4     5     6     7    10    11    12    13    14    17    21    23    25    28    29   31    32    35    36    38    39    41    42    43    44    45    46    47    48    49    50    56    57   58    59    60]
    s= s + BinningSpike2(channel,:);
end
figure; plot(BinningTime2,s/(BinningInterval)/39/(12-1)); %39 is the total number of selected channels % s-1 %12 trails and dont take the last one

x1=list(z); line([x1 x1], get(gca, 'ylim'),'Color','r','LineStyle','--');  %bar reverse
x2=2*list(z); line([x2 x2], get(gca, 'ylim'),'Color','b','LineStyle','--');  %bar stops moving


set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate(Hz)')
xlabel('Time (sec)')
box on
set(gca,'fontsize',15); 
set(gca, 'Fontname', 'Times New Roman');
% set(gca, 'YTick', [10  60 110])
% saveas(gca, fullfile(fname, name), 'fig');
end


%% Plot PSTH for 3 velocity for only reversal channels
clear all
cd('D:\Yiko\Files for Thesis\04102018\New folder\rev fpr thesis plot')
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 

for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

list=[];
list=[0.5 0.9 2];

reversal_ch=[1 2 3 4 5 6 7 8 9 11 12 17 20 21 25 26 27]; %select manually from Heatmap %for all T09 05 2

T1=1; %sec %choose time range for plotting
T2=1+list(z)*2+1;

%bin=10ms
BinningInterval=0.01;
frame1=T1/BinningInterval;
frame2=T2/BinningInterval;
BinningTime2=[];
BinningTime2=BinningTime(frame1:frame2)-T1;

BinningSpike2 =zeros(60,frame2-frame1+1);
%select only soma
for i=[1     2     3     4     5     6     7    10    11    12    13    14    17    21    23    25    28    29   31    32    35    36    38    39    41    42    43    44    45    46    47    48    49    50    56    57   58    59    60]
    BinningSpike2(i,:)=BinningSpike(i,frame1:frame2) ;
end

%sum only reversal channels
s=0;
for channel= reversal_ch
    s= s + BinningSpike2(channel,:);
end
figure; plot(BinningTime2,s/(BinningInterval)/17/(12-1)); %39 is the total number of selected channels % s-1 %12 trails and dont take the last one

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate(Hz)')
xlabel('Time (sec)')
box on
set(gca, 'Fontname', 'Times New Roman');
set(gca,'fontsize',15); 

x1=list(z); line([x1 x1], get(gca, 'ylim'),'Color','r','LineStyle','--');  %bar reverse
x2=2*list(z); line([x2 x2], get(gca, 'ylim'),'Color','b','LineStyle','--');  %bar stops moving
end


%% Plot Heatmap for 3 velocity for all soma channels
clear all
cd('D:\Yiko\Files for Thesis\04102018\New folder\rev fpr thesis plot')
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir. 
n_file = length(all_file) ; 

for z =1:n_file
file = all_file(z).name ;
[pathstr, name, ext] = fileparts(file);
directory = [pathstr,'\'];
filename = [name,ext];
load([filename]);
name=[name];
name

list=[];
list=[0.5 0.9 2];
laterT=[0.5 0.9 1];

T1=1; %sec %choose time range for plotting
T2=1+list(z)*2+laterT(z);

%bin=10ms
BinningInterval=0.01;
frame1=T1/BinningInterval;
frame2=round(T2/BinningInterval);
BinningTime2=[];
BinningTime2=BinningTime(frame1:frame2)-T1;

BinningSpike2 =zeros(60,frame2-frame1+1);
%select only soma
for i=[1     2     3     4     5     6     7    10    11    12    13    14    17    21    23    25    28    29   31    32    35    36    38    39    41    42    43    44    45    46    47    48    49    50    56    57   58    59    60]
    BinningSpike2(i,:)=BinningSpike(i,frame1:frame2) ;
end

% change rows for RL(LCD) direction stimulation
%LCD(RL) <=> MEA(UD)
mn(1,:)=[0 7 15 23 31 39 47 0];   mn(8,:)=[0 14 22 30 38 46 54 0];
for i=1:6
    if i==1
            mn(i+1,:)=[1 8 16 24 32 40 48 55];
    else        mn(i+1,:)=[1 8 16 24 32 40 48 55]+i-1;   end
end
BinningSpike3=[]; %the rearranged matrix
%test orientation:
Newori=[];
count=7; %start from #7 
for j=2:7
        for kk=1:8
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    Newori(count,:)=mn(j,kk);
                    count=count+1;
        end
                 count=55;
end
%
count=7; %start from #7 
select_ch=54;
for j=2:7
        for kk=1:8
                    BinningSpike3(count,:)=BinningSpike2(mn(j,kk),:);
                          if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
end
count=1;    %fill in the first and last 6 numbers
for j=[1 8]
        for kk=2:7
                    BinningSpike3(count,:)=BinningSpike2(mn(j,kk),:);
                    if mn(j,kk)==select_ch
                         count
                    end
                    count=count+1;
        end
                 count=55;
end

figure;
imagesc(BinningTime2,[1:60],BinningSpike3);

x1=list(z); line([x1 x1], get(gca, 'ylim'),'Color','w','LineStyle','--');  %bar reverse
x2=2*list(z); line([x2 x2], get(gca, 'ylim'),'Color','r','LineStyle','--');  %bar stops moving

set(gca,'Position',[0.17 0.2  0.75 0.7])
ylabel('Firing rate(Hz)')
xlabel('Time (sec)')
box on
set(gca, 'Fontname', 'Times New Roman');
set(gca,'fontsize',15); 
set(gca,'LineWidth',1.3);
cbr = colorbar ; %Create Colorbar

end

